import 'dart:async';

import 'package:app/comps/app_event_bus.dart';
import 'package:app/comps/app_pick_field.dart';
import 'package:app/comps/app_select_field.dart';
import 'package:event_bus/event_bus.dart';
import 'package:flutter/material.dart';

class AppNormalForm extends StatefulWidget {
  Map<String, dynamic> dict;
  List items;
  String itemTitle;
  String delBtnText;
  EventBus bus;
  Function(String)? onDelete;
  Function(dynamic)? onSave;
  AppNormalForm({
    Key? key,
    required this.itemTitle,
    required this.delBtnText,
    required this.dict,
    required this.items,
    required this.bus,
    this.onDelete,
    this.onSave,
  }) : super(key: key);

  @override
  _AppNormalFormState createState() => _AppNormalFormState();
}

class _AppNormalFormState extends State<AppNormalForm> {
  String name = "";
  String sdate = "";
  String edate = "";
  int idx = -1;
  GlobalKey gkey = GlobalKey();

  late StreamSubscription ev;

  @override
  void initState() {
    super.initState();

    name = widget.dict["name"] ?? "";
    sdate = widget.dict["sdate"] ?? "";
    edate = widget.dict["edate"] ?? "";
    idx = widget.items.indexOf(name);

    ev = widget.bus.on<AppEventBus>().listen((event) {
      print(event.key);
      if (widget.onSave != null) {
        widget.onSave!({
          "name": name,
          "sdate": sdate,
          "edate": edate,
          "idx": idx,
        });
      }
    });
  }

  @override
  void didUpdateWidget(AppNormalForm oldWidget) {
    super.didUpdateWidget(oldWidget);
    setState(() {});
  }

  @override
  void dispose() {
    ev.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 12),
      key: gkey,
      child: Row(
        children: [
          Container(
            width: 400,
            child: AppSelectField(
              items: widget.items,
              value: name,
              onChanged: (index) {
                idx = index;
                var s = widget.items[idx];
                name = s;
                setState(() {});
              },
              titleStr: widget.itemTitle,
              must: true,
              placeholder: "SELECT DEVICE",
            ),
          ),
          SizedBox(
            width: 20,
          ),
          Container(
            width: 150,
            child: AppPickField(
              onTap: () {
                showCalenderHandle(context, (e) {
                  print(e);
                  sdate = e;
                  setState(() {});
                });
              },
              titleStr: "Start Date",
              must: true,
              placeholder: "01/07/2021",
              value: sdate,
            ),
          ),
          SizedBox(
            width: 20,
          ),
          Container(
            width: 150,
            child: AppPickField(
              onTap: () {
                showCalenderHandle(context, (e) {
                  edate = e;
                  setState(() {});
                });
              },
              titleStr: "End Date",
              placeholder: "Select End Date",
              value: edate,
            ),
          ),
          SizedBox(
            width: 20,
          ),
          Container(
            width: 230,
            padding: EdgeInsets.only(top: 20),
            child: InkWell(
              onTap: () {
                widget.onDelete!(widget.key.toString());
              },
              child: Row(
                children: [
                  Icon(
                    Icons.delete_outlined,
                    size: 20,
                    color: Colors.red,
                  ),
                  Text(
                    widget.delBtnText,
                    style: TextStyle(color: Colors.red, fontSize: 15),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
