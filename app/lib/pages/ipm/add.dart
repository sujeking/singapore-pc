import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_pick_field.dart';
import 'package:app/comps/app_text_field.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/ipm.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:toast/toast.dart';

class IPAddPage extends StatefulWidget {
  IPAddPage({Key? key}) : super(key: key);

  @override
  _IPAddPageState createState() => _IPAddPageState();
}

class _IPAddPageState extends State<IPAddPage> {
  var body = {
    "name": "",
    "ipAddress": "",
    "startDate": "",
    "endDate": "",
  };
  @override
  void initState() {
    super.initState();
  }

  netAddIp() async {
    var res = await ipAdd(body);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show(
        "Successful Operation",
        context,
        gravity: Toast.CENTER,
        duration: Toast.LENGTH_LONG,
      );
      Future.delayed(
        Duration(seconds: 2),
        () {
          Navigator.of(context).pop();
        },
      );
    } else {
      Toast.show(
        res["message"],
        context,
        gravity: Toast.CENTER,
        duration: Toast.LENGTH_LONG,
      );
    }
  }

  rowOne() {
    return Container(
      height: 100,
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Row(
        children: [
          Expanded(
            child: AppTextField(
              value: body["name"],
              titleStr: "IP Name",
              placeholder: "Enter IP Name",
              must: true,
              onChanged: (v) {
                body["name"] = v;
              },
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: AppTextField(
              value: body["ipAddress"],
              titleStr: "IP Address",
              placeholder: "Enter IP Address",
              must: true,
              onChanged: (e) {
                body["ipAddress"] = e;
              },
            ),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    print(body);
    return Container(
      height: 100,
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Row(
        children: [
          Expanded(
            child: AppPickField(
              value: body["startDate"],
              onTap: () {
                showCalenderHandle(context, (e) {
                  setState(() {
                    body["startDate"] = e;
                  });
                });
              },
              titleStr: "IP Start Date",
              must: true,
              placeholder: "Select IP Start Date",
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: AppPickField(
              value: body["endDate"],
              onTap: () {
                showCalenderHandle(context, (e) {
                  body["endDate"] = e;
                  setState(() {});
                });
              },
              titleStr: "IP End Date",
              placeholder: "Select IP End Date",
            ),
          ),
        ],
      ),
    );
  }

  ActionView() {
    return Container(
      child: Row(
        children: [
          AppBottomBtn(
            label: 'Submit For Approval',
            color: Colors.blue,
            onTap: () {
              netAddIp();
            },
          ),
          SizedBox(width: 12),
          AppBottomBtn(
            label: 'Cancel',
            color: Color(0xffff0000),
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.fromLTRB(40, 12, 40, 80),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TopBar(
              backTitle: "Add New IP",
              popAction: () {
                Navigator.pop(context);
              },
            ),
            PageTitleView(title: "Add New IP"),
            Expanded(
              child: Column(
                children: [
                  rowOne(),
                  rowTwo(),
                ],
              ),
            ),
            ActionView()
          ],
        ),
      ),
    );
  }
}
