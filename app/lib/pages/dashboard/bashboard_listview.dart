import 'package:app/comps/app_table.dart';
import 'package:app/comps/app_table_cell.dart';
import 'package:app/comps/base_datatable.dart';
import 'package:app/net/logm.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class BashBoardListView extends StatefulWidget {
  BashBoardListView({Key? key}) : super(key: key);

  @override
  BashBoardListViewState createState() => BashBoardListViewState();
}

class BashBoardListViewState extends State<BashBoardListView> {
  List<String> cols = [
    'USER ACCOUNT NAME',
    'MODULE',
    'ACTIVE DATE & TIME',
    'ACTIVTY TYPE',
    'IP ADDRESS',
    'DEVICE ID',
    'BRANCH NAME',
    'EXCEPTION',
  ];
  List<double> widths = [180, 120, 180, 180, 180, 200, 190, 200, 100];

  List<Widget> rows = [];

  int _page = 1;
  int _pageSize = 30;
  var datasource = [];

  bool isloading = true;
  @override
  void initState() {
    super.initState();
    netLogList(null);
  }

  netLogList(RefreshController? c) async {
    var fi = {"_page": _page, "_pageSize": _pageSize};
    var res = await logList(fi);
    var list = res["list"];
    datasource.addAll(list);
    updateUI();
    isloading = false;
    setState(() {});
    c!.refreshCompleted();
    c.loadComplete();
  }

  updateUI() {
    for (var i = 0; i < datasource.length; i++) {
      var dict = datasource[i];
      var row = Row(children: [
        AppTableCell(width: widths[0], text: dict["userName"] ?? ""),
        AppTableCell(width: widths[1], text: dict["module"] ?? ""),
        AppTableCell(width: widths[2], text: dict["activityTime"] ?? ""),
        AppTableCell(width: widths[3], text: dict["activityType"] ?? ""),
        AppTableCell(width: widths[4], text: dict["ipAddress"] ?? ""),
        AppTableCell(width: widths[5], text: dict["deviceId"] ?? ""),
        AppTableCell(width: widths[6], text: dict["branch"] ?? ""),
        AppTableCell(width: widths[7], text: dict["exception"] ?? ""),
      ]);
      rows.add(row);
    }
  }

  _cleanData() {
    _page = 1;
    rows.clear();
    datasource.clear();
  }

  _header() {
    List<AppTable3Header> items = [];
    for (var i = 0; i < cols.length; i++) {
      AppTable3Header ii = AppTable3Header(
        sortkey: "",
        text: cols[i],
        isup: false,
        hidenIcon: true,
        width: widths[i],
      );
      items.add(ii);
    }
    return Row(children: items);
  }

  @override
  Widget build(BuildContext context) {
    return AppTable3(
      isLoading: isloading,
      onRefresh: (RefreshController c) {
        isloading = true;
        setState(() {});
        _cleanData();
        netLogList(c);
      },
      onLoadMore: (RefreshController c) {
        isloading = true;
        setState(() {});
        _page += 1;
        netLogList(c);
      },
      children: [_header(), ...rows],
    );
  }
}
