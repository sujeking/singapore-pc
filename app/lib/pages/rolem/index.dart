import 'package:app/base/base_page_router.dart';
import 'package:app/pages/rolem/add.dart';
import 'package:app/pages/rolem/approval_add.dart';
import 'package:app/pages/rolem/approval_edit.dart';
import 'package:app/pages/rolem/edit.dart';
import 'package:app/pages/rolem/list.dart';
import 'package:app/pages/rolem/read.dart';
import 'package:flutter/material.dart';

class RoleIndex extends StatefulWidget {
  RoleIndex({Key? key}) : super(key: key);

  @override
  _RoleIndexState createState() => _RoleIndexState();
}

class _RoleIndexState extends State<RoleIndex> {
  Map<String, Widget> dict = {
    '/list': RoleListIndexPage(),
    '/read': RoleReadPage(),
    "/add": RoleAddPage(),
    "/edit":RoleEditPage(),
    "/approval": Container(),
    "/appadd": RoleApprovalAddPage(),
    "/appedit": RoleApprovalEditPage(),
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BasePageRouter(
      initPath: "/list",
      dict: dict,
    ));
  }
}
