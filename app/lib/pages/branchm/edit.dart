import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_pick_field.dart';
import 'package:app/comps/app_text_field.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/branchm.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:date_format/date_format.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class BranchEditPage extends StatefulWidget {
  BranchEditPage({Key? key}) : super(key: key);

  @override
  _BranchEditPageState createState() => _BranchEditPageState();
}

class _BranchEditPageState extends State<BranchEditPage> {
  Map<String, dynamic> body = {
    "name": "",
    "subsidiary": "",
    "code": "",
    "location": "",
    "startDate": "",
    "endDate": ""
  };
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      body = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
      setState(() {});
    });
  }

  netUpdateBranch() async {
    var res = await branchUpdate(body);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show(
        "Successful Operation",
        context,
        gravity: Toast.CENTER,
        duration: Toast.LENGTH_LONG,
      );
      Future.delayed(
        Duration(seconds: 2),
        () {
          Navigator.of(context).pop();
        },
      );
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  rowOne() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Row(
        children: [
          Expanded(
            child: Container(
              child: AppTextField(
                value: body["name"],
                onChanged: (e) {
                  body["name"] = e;
                },
                titleStr: "Branch Name",
                must: true,
                placeholder: "Enter Branch Name",
              ),
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: Container(
              child: AppTextField(
                value: body["subsidiary"],
                onChanged: (e) {
                  body["subsidiary"] = e;
                },
                must: true,
                titleStr: "Subsidiary",
                placeholder: "Enter Subsidiary",
              ),
            ),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
      height: 100,
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Row(
        children: [
          Expanded(
            child: Container(
              child: AppTextField(
                value: body["code"],
                onChanged: (e) {
                  body["code"] = e;
                },
                titleStr: "Branch Code (optional)",
                placeholder: "Enter Branch Code",
              ),
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: Container(
              child: AppTextField(
                value: body["location"],
                onChanged: (e) {
                  body["location"] = e;
                },
                titleStr: "Branch Location",
                must: true,
                placeholder: "Enter Branch Location",
              ),
            ),
          ),
        ],
      ),
    );
  }

  rowThree() {
    return Container(
      height: 100,
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Row(
        children: [
          Expanded(
            child: Container(
              child: AppPickField(
                value: body["startDate"],
                titleStr: "Branch Start Date",
                placeholder: "Enter Branch Start Date",
                onTap: () {
                  showCalenderHandle(context, (e) {
                    body["startDate"] = e;
                    setState(() {});
                  });
                },
              ),
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: Container(
              child: AppPickField(
                value: body["endDate"],
                onTap: () {
                  showCalenderHandle(context, (e) {
                    body["endDate"] = e;
                    setState(() {});
                  });
                },
                titleStr: "Branch End Date (optional)",
                must: true,
                placeholder: "Enter Branch End Date",
              ),
            ),
          ),
        ],
      ),
    );
  }

  ActionView() {
    return Container(
      child: Row(
        children: [
          AppBottomBtn(
            label: 'Submit For Approval',
            color: Colors.blue,
            onTap: () {
              netUpdateBranch();
            },
          ),
          SizedBox(width: 12),
          AppBottomBtn(
            label: 'Cancel',
            color: Color(0xffff0000),
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.fromLTRB(40, 12, 40, 80),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TopBar(
              backTitle: "Edit Branch",
              popAction: () {
                Navigator.pop(context);
              },
            ),
            PageTitleView(title: "Edit Branch"),
            SizedBox(
              height: 40,
            ),
            Expanded(
              child: ListView(
                children: [
                  rowOne(),
                  rowTwo(),
                  rowThree(),
                ],
              ),
            ),
            ActionView(),
          ],
        ),
      ),
    );
  }
}
