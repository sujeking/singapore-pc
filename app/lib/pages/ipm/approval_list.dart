import 'package:app/comps/app_table.dart';
import 'package:app/comps/app_table_cell.dart';
import 'package:app/comps/base_datatable.dart';
import 'package:app/net/ipm.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class IPApprovalListPage extends StatefulWidget {
  String? kwd;
  IPApprovalListPage({Key? key, this.kwd}) : super(key: key);

  @override
  _TenantApprovalPageState createState() => _TenantApprovalPageState();
}

class _TenantApprovalPageState extends State<IPApprovalListPage> {
  int _page = 1;
  int _pageSize = 30;
  var datasource = [];
  String kwd = "";
  String sortKey = "";
  bool resort = false;
  List<String> cols = [
    "IP NAME",
    "IP ADDRESS",
    "REQUESTED BY",
    "REQUESTED DATE",
    "ACTION",
  ];
  List<String> keys = [
    "name",
    "ipAddress",
    "creatorName",
    "updatedAt",
    "operationType",
  ];
  List<double> widths = [
    150,
    230,
    150,
    160,
    300,
  ];

  List<Widget> rows = [];
  bool isloading = true;
  @override
  void initState() {
    super.initState();
    sortKey = keys.first;
    netIpAppList(null);
  }

  @override
  void didUpdateWidget(IPApprovalListPage oldWidget) {
    super.didUpdateWidget(oldWidget);
    kwd = widget.kwd ?? "";
    _cleanData();
    netIpAppList(null);
  }

  netIpAppList(RefreshController? c) async {
    var fi = {
      "_page": _page,
      "_pageSize": _pageSize,
      "_keyword": kwd,
      "_orderby": resort ? "${sortKey} desc" : "${sortKey} asc",
    };
    var res = await ipAppList(fi);
    var list = res["list"];
    print(res);
    datasource.addAll(list);
    updateUI();
    isloading = false;
    setState(() {});
    c!.refreshCompleted();
    c.loadComplete();
  }

  rowSelectHandle(dict) {
    var type = dict["operationType"];
    var newData = dict["newData"];
    if (type == "create") {
      Navigator.pushNamed(context, "/appadd", arguments: newData);
    } else {
      Navigator.pushNamed(context, "/appedit", arguments: dict);
    }
  }

  updateUI() {
    rows.clear();
    for (var i = 0; i < datasource.length; i++) {
      var item = datasource[i];
      var dict = item["approval"];
      var newData = dict["newData"];
      var row = InkWell(
        onTap: () {
          rowSelectHandle(dict);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(children: [
              AppTableCell(width: widths[0], text: newData["name"]),
              AppTableCell(width: widths[1], text: newData["ipAddress"]),
              AppTableCell(width: widths[2], text: newData["creatorName"]),
              AppTableCell(width: widths[3], text: newData["updatedAt"]),
              AppTableCell(width: widths[4], text: dict["operationType"]),
            ]),
            Container(
              height: 0.5,
              width: widths.reduce((a, b) => a + b),
              color: Color(0xffe0e0e0),
            )
          ],
        ),
      );
      rows.add(row);
    }
  }

  _cleanData() {
    _page = 1;
    rows.clear();
    datasource.clear();
  }

  _header() {
    List<AppTable3Header> items = [];
    for (var i = 0; i < cols.length; i++) {
      AppTable3Header ii = AppTable3Header(
        sortkey: keys[i],
        text: cols[i],
        width: widths[i],
        isup: !resort,
        onTap: (key) {
          isloading = true;
          datasource.clear();
          setState(() {});
          sortKey = key;
          _page = 1;
          resort = !resort;
          netIpAppList(null);
        },
      );
      items.add(ii);
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: items),
        Container(
          height: 0.5,
          width: widths.reduce((a, b) => a + b),
          color: Color(0xffe0e0e0),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppTable3(
      isLoading: isloading,
      onRefresh: (RefreshController c) {
        isloading = true;
        setState(() {});
        _cleanData();
        netIpAppList(c);
      },
      onLoadMore: (RefreshController c) {
        isloading = true;
        setState(() {});
        _page += 1;
        netIpAppList(c);
      },
      children: [_header(), ...rows],
    );
  }
}
