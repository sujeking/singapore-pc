import 'package:app/comps/base_datatable.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class AppTable extends StatefulWidget {
  final List<String> heads;
  final List<double> colWidths;
  final List<BaseDataRow> rows;
  double? rowHeight = 30.0;
  AppTable(
      {Key? key,
      required this.heads,
      required this.rows,
      this.rowHeight,
      required this.colWidths})
      : super(key: key);

  @override
  _AppTable createState() => _AppTable();
}

class _AppTable extends State<AppTable> {
  late List cols;
  late List<double> widths;
  late List<BaseDataRow> rows;
  late double rowHeight;
  late Function rowTap;
  double mainHeight = 100.0;
  @override
  void initState() {
    super.initState();
    cols = widget.heads;
    widths = widget.colWidths;
    rows = widget.rows;
    rowHeight = widget.rowHeight as double;
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      controller: ScrollController(),
      scrollDirection: Axis.horizontal,
      children: [
        Container(
          child: SingleChildScrollView(
            physics: NeverScrollableScrollPhysics(),
            child: BaseDataTable(
              showCheckboxColumn: false,
              showBottomBorder: true,
              columnSpacing: 12,
              horizontalMargin: 12,
              headingRowHeight: 44,
              BasedataRowHeight: rowHeight,
              columns: cols.map((e) {
                int idx = cols.indexOf(e);
                return BaseDataColumn(
                    label: Container(
                  constraints: BoxConstraints(minWidth: widths[idx]),
                  child: Stack(
                    children: [
                      Container(
                        padding: EdgeInsets.only(top: 12, bottom: 6),
                        child: Text(
                          e,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 13,
                              fontFamily: "she",
                              color: Color(0xff080808),
                              overflow: TextOverflow.visible),
                        ),
                      ),
                      Positioned(
                          bottom: 0,
                          left: 0,
                          right: 0,
                          child:
                              Container(height: 0.5, color: Color(0xff191818))),
                    ],
                  ),
                ));
              }).toList(),
              rows: rows,
            ),
          ),
        )
      ],
    );
  }
}

///
///
///
///
///
class AppTable2 extends StatefulWidget {
  Widget header;
  List<Widget> rows;
  Function()? onSelect;
  AppTable2({Key? key, required this.header, required this.rows, this.onSelect})
      : super(key: key);
  @override
  _AppTable2State createState() => _AppTable2State();
}

class _AppTable2State extends State<AppTable2> {
  late Widget header;
  late double bodyWidth;
  late List<Widget> rows;
  GlobalKey headerKey = GlobalKey();
  late Function()? onSelect;

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
    rows = widget.rows;
    header = widget.header;
    onSelect = widget.onSelect ?? () {};
    _calcuWidth();
  }

  _calcuWidth() {
    List ws = [];
    List arr = (header as Row).children;
    for (var i = 0; i < arr.length; i++) {
      Container c = arr[i];
      BoxConstraints bc = c.constraints as BoxConstraints;
      print(bc.minWidth);
      ws.add(bc.minWidth);
    }
    bodyWidth = ws.reduce((a, b) => a + b);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Container(
        width: bodyWidth,
        child: SmartRefresher(
          controller: _refreshController,
          onRefresh: () {
            _refreshController.refreshCompleted();
          },
          header: ClassicHeader(),
          child: ListView.builder(
            itemCount: rows.length + 1,
            itemBuilder: (BuildContext context, int index) {
              if (index == 0) {
                return Container(
                  key: headerKey,
                  child: header,
                );
              }
              index -= 1;
              return rows[index];
            },
          ),
        ),
      ),
    );
  }
}

class AppTable3 extends StatefulWidget {
  List<Widget>? children;
  bool? isLoading;
  Function(RefreshController _refreshController)? onRefresh;
  Function(RefreshController _refreshController)? onLoadMore;
  AppTable3(
      {Key? key,
      this.isLoading,
      this.children,
      this.onRefresh,
      this.onLoadMore})
      : super(key: key);

  @override
  _AppTable3State createState() => _AppTable3State();
}

class _AppTable3State extends State<AppTable3> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  Function(RefreshController _refreshController)? onRefresh;
  Function(RefreshController _refreshController)? onLoadMore;

  late List<Widget> children;
  @override
  void initState() {
    super.initState();
    children = widget.children ?? [];
    onRefresh = widget.onRefresh;
    onLoadMore = widget.onLoadMore;
  }

  @override
  void didUpdateWidget(covariant AppTable3 oldWidget) {
    super.didUpdateWidget(oldWidget);
    children = widget.children ?? [];
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SmartRefresher(
          controller: _refreshController,
          onRefresh: () {
            onRefresh!(_refreshController);
          },
          header: ClassicHeader(),
          onLoading: () {
            onLoadMore!(_refreshController);
          },
          enablePullUp: true,
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: children,
                ),
              ),
            ],
          ),
        ),
        Offstage(
          offstage: (widget.isLoading ?? false) != true,
          child: Container(
            color: Colors.black.withOpacity(0.01),
            child: Center(child: CircularProgressIndicator()),
          ),
        ),
      ],
    );
  }
}

class AppTable3Header extends StatefulWidget {
  String sortkey;
  String text;
  double width;
  bool isup;
  bool? hidenIcon;
  Function(String keystr)? onTap;
  AppTable3Header({
    Key? key,
    required this.text,
    this.onTap,
    this.hidenIcon,
    required this.isup,
    required this.sortkey,
    required this.width,
  }) : super(key: key);

  @override
  _AppTable3HeaderState createState() => _AppTable3HeaderState();
}

class _AppTable3HeaderState extends State<AppTable3Header> {
  late String text;
  late double width;
  @override
  void initState() {
    super.initState();
    text = widget.text;
    width = widget.width;
  }

  @override
  void didUpdateWidget(covariant AppTable3Header oldWidget) {
    super.didUpdateWidget(oldWidget);
    text = widget.text;
    width = widget.width;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.onTap!(widget.sortkey);
      },
      child: Container(
        width: width + 12,
        padding: EdgeInsets.only(right: 12),
        height: 35,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(top: 12, bottom: 6),
              child: Row(
                children: [
                  Text(
                    text,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontSize: 11,
                        fontFamily: "sre",
                        color: Color(0xff080808),
                        fontWeight: FontWeight.w900,
                        overflow: TextOverflow.visible),
                  ),
                  Offstage(
                    offstage: widget.hidenIcon == true,
                    child: Icon(
                      widget.isup ? Icons.arrow_drop_down : Icons.arrow_drop_up,
                      size: 15,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
