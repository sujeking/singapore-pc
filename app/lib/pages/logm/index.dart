import 'package:app/base/base_page_router.dart';
import 'package:app/pages/logm/list.dart';
import 'package:flutter/material.dart';

class LogIndex extends StatefulWidget {
  LogIndex({Key? key}) : super(key: key);

  @override
  _LogIndexState createState() => _LogIndexState();
}

class _LogIndexState extends State<LogIndex> {
  Map<String, Widget> dict = {'/list': LogListPage(), "/approval": Container()};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BasePageRouter(
      initPath: '/list',
      dict: dict,
    ));
  }
}
