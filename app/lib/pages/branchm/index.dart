import 'package:app/base/base_page_router.dart';
import 'package:app/pages/branchm/add.dart';
import 'package:app/pages/branchm/approval_add.dart';
import 'package:app/pages/branchm/approval_edit.dart';
import 'package:app/pages/branchm/edit.dart';
import 'package:app/pages/branchm/list.dart';
import 'package:app/pages/branchm/read.dart';
import 'package:flutter/material.dart';

class GroupIndex extends StatefulWidget {
  GroupIndex({Key? key}) : super(key: key);

  @override
  _GroupIndexState createState() => _GroupIndexState();
}

class _GroupIndexState extends State<GroupIndex> {
  Map<String, Widget> dict = {
    '/list': BranchListIndexPage(),
    '/read': BranchReadPage(),
    "/add": BranchAddPage(),
    "/edit": BranchEditPage(),
    "/approval": BranchAddPage(),
    "/appadd": BranchApprovalAddPage(),
    "/appedit": BranchApprovalEditPage(),
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BasePageRouter(
      initPath: "/list",
      dict: dict,
    ));
  }
}
