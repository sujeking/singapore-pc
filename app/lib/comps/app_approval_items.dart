import 'package:flutter/material.dart';

class AppApprovalItems extends StatefulWidget {
  final String title;
  String? oldValue;
  String? newValue;
  AppApprovalItems(
      {Key? key, required this.title, this.oldValue, this.newValue})
      : super(key: key);

  @override
  _AppApprovalItemsState createState() => _AppApprovalItemsState();
}

class _AppApprovalItemsState extends State<AppApprovalItems> {
  String title = "";
  String oldValue = "";
  String newValue = "";

  @override
  void initState() {
    super.initState();
    title = widget.title;
    oldValue = widget.oldValue ?? "";
    newValue = widget.newValue ?? "";
  }

  @override
  void didUpdateWidget(AppApprovalItems oldWidget) {
    super.didUpdateWidget(oldWidget);
    oldValue = widget.oldValue ?? "";
    newValue = widget.newValue ?? "";
    setState(() {});
  }

  _itemView(int tag, String txt) {
    return Row(
      children: [
        Container(
          height: 42,
          constraints: BoxConstraints(minWidth: 130),
          padding: EdgeInsets.symmetric(horizontal: 40),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(21),
            color: tag == 0 ? Color(0xffe0e0e0) : Colors.white,
            border: Border.all(
              width: 1,
              color: tag == 0
                  ? Color(0xff0d0c0c).withOpacity(0.17)
                  : Color(0xff007aff),
            ),
          ),
          child: Center(
            child: Text(
              tag == 0 ? "NEW" : "EXISTING",
              style: TextStyle(
                  fontSize: 11,
                  fontFamily: "sli",
                  color: tag == 0 ? Color(0xff646464) : Color(0xff007aff)),
            ),
          ),
        ),
        SizedBox(width: 16),
        Expanded(
            child: Container(
          height: 42,
          padding: EdgeInsets.symmetric(horizontal: 23),
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(21),
            color: Color(0xffe0e0e0),
            border: Border.all(
              width: 1,
              color: tag == 0
                  ? Color(0xff0d0c0c).withOpacity(0.17)
                  : Color(0xff007aff),
            ),
          ),
          child: Text(
            txt,
            style: TextStyle(
                fontSize: 11,
                fontFamily: "sli",
                color: tag == 0 ? Color(0xff646464) : Color(0xff007aff)),
          ),
        )),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(title,
            style: TextStyle(
              fontSize: 17,
              fontFamily: "sli",
              color: Color(0xff3c3c43).withOpacity(0.6),
            )),
        SizedBox(
          height: 10,
        ),
        // item
        _itemView(0, oldValue),
        SizedBox(
          height: 10,
        ),
        _itemView(1, newValue)
      ],
    );
  }
}
