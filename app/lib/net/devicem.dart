import 'net.dart';

deviceAdd(Map<String, dynamic> data) async {
  var res = await $http("post", "/device", data: data);
  return res;
}

deviceDelete(String id) async {
  var res = await $http("delete", "/device/" + id);
  return res;
}

deviceUpdate(Map<String, dynamic> data) async {
  var res = await $http("put", "/device/" + data["_id"], data: data);
  return res;
}

deviceList(Map<String, dynamic> data) async {
  var res = await $http("get", "/device", data: data);
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

deviceDetail(String id) async {
  var res = await $http("get", "/device/${id}");
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

// approval

deviceAppList(Map<String, dynamic> data) async {
  var res = await $http("get", "/device/approval/index", data: data);
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

deviceAppPass(String id) async {
  var res = await $http("post", "/device/${id}/approval/pass");
  return res;
}

deviceAppReject(String id) async {
  var res = await $http("post", "/device/${id}/approval/reject");
  return res;
}
