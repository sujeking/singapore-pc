import 'package:app/comps/base_datatable.dart';
import 'package:flutter/material.dart';

class Test extends StatelessWidget {
  const Test({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BaseDataTable(
        columns: [BaseDataColumn(label: Text("123123"))],
        rows: [
          BaseDataRow(cells: [BaseDataCell(Text("123"))])
        ],
      ),
    );
  }
}
