import 'package:flutter/cupertino.dart';

enum Status { None, New, Approved, Szw, Deactivate }

final StatusColors = [
  Color(0xff68d86c),
  Color(0xffe3c46f),
  Color(0xff68d86c),
  Color(0xfff70b0b),
  Color(0xfff70b0b),
  Color(0xfff70b0b),
  Color(0xfff70b0b),
  Color(0xfff70b0b),
];
