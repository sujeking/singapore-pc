import 'package:app/base/base_page_router.dart';
import 'package:app/pages/ipm/add.dart';
import 'package:app/pages/ipm/approval_add.dart';
import 'package:app/pages/ipm/approval_edit.dart';
import 'package:app/pages/ipm/edit.dart';
import 'package:app/pages/ipm/list.dart';
import 'package:app/pages/ipm/read.dart';
import 'package:flutter/material.dart';

class IPIndex extends StatefulWidget {
  IPIndex({Key? key}) : super(key: key);

  @override
  _IPIndexState createState() => _IPIndexState();
}

class _IPIndexState extends State<IPIndex> {
  Map<String, Widget> dict = {
    '/list': IPListIndexPage(),
    '/read': IPReadPage(),
    "/add": IPAddPage(),
    "/edit": IPEditPage(),
    "/approval": Container(),
    "/appadd": IPApprovalAddPage(),
    "/appedit": IPApprovalEditPage(),
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BasePageRouter(
        initPath: "/list",
        dict: dict,
      ),
    );
  }
}
