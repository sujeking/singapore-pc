// ignore_for_file: import_of_legacy_library_into_null_safe

import 'package:app/Responsive.dart';
import 'package:app/pages/dashboard/index.dart';
import 'package:app/pages/devicem/index.dart';
import 'package:app/pages/branchm/index.dart';
import 'package:app/pages/ipm/index.dart';
import 'package:app/pages/logm/index.dart';
import 'package:app/pages/rolem/index.dart';
import 'package:app/pages/tenantm/index.dart';
import 'package:app/pages/userm/index.dart';
import 'package:app/views/main_menu/main_menu..dart';
import 'package:flutter/material.dart';
import 'package:window_utils/window_utils.dart';

var pages = [
  DashBoard(),
  UserIndex(),
  DeviceIndex(),
  RoleIndex(),
  TenantIndex(),
  IPIndex(),
  GroupIndex(),
  LogIndex(),
];

int pageIdx = 0;

///
///
class MobilePage extends StatefulWidget {
  MobilePage({Key? key}) : super(key: key);

  @override
  State<MobilePage> createState() => _MobilePageState();
}

class _MobilePageState extends State<MobilePage> {
  GlobalKey<ScaffoldState> mkey = GlobalKey();

  PageController _pageController = PageController(initialPage: pageIdx);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: mkey,
      backgroundColor: Colors.white,
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: Colors.white,
              padding: EdgeInsets.symmetric(horizontal: 20),
              height: 64,
              child: InkWell(
                onTap: () {
                  mkey.currentState!.openDrawer();
                },
                child: Icon(
                  Icons.menu,
                ),
              ),
            ),
            Expanded(
              child: PageView(
                physics: NeverScrollableScrollPhysics(),
                controller: _pageController,
                children: pages,
              ),
            ),
          ],
        ),
      ),
      drawer: Container(
        width: 260,
        child: SideMenu(
          onTap: (idx) {
            pageIdx = idx;
            Navigator.of(context).pop();
            Future.delayed(Duration(milliseconds: 300), () {
              _pageController.jumpToPage(idx);
            });
          },
        ),
      ),
    );
  }
}

///
///
class PadPage extends StatefulWidget {
  PadPage({Key? key}) : super(key: key);

  @override
  State<PadPage> createState() => _PadPageState();
}

class _PadPageState extends State<PadPage> {
  PageController _pageController = PageController(initialPage: pageIdx);
  GlobalKey<ScaffoldState> tkey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: tkey,
      backgroundColor: Colors.white,
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              height: 64,
              color: Colors.white,
              child: InkWell(
                onTap: () {
                  tkey.currentState!.openDrawer();
                },
                child: Icon(Icons.menu),
              ),
            ),
            Expanded(
              child: PageView(
                physics: NeverScrollableScrollPhysics(),
                controller: _pageController,
                children: pages,
              ),
            ),
          ],
        ),
      ),
      drawer: Container(
        width: 260,
        child: SideMenu(onTap: (idx) {
          pageIdx = idx;
          Navigator.of(context).pop();
          Future.delayed(Duration(milliseconds: 100), () {
            _pageController.jumpToPage(idx);
          });
        }),
      ),
    );
  }
}

///
///
class DesktopPage extends StatefulWidget {
  DesktopPage({Key? key}) : super(key: key);

  @override
  State<DesktopPage> createState() => _DesktopPageState();
}

class _DesktopPageState extends State<DesktopPage> {
  PageController _pageController = PageController(initialPage: pageIdx);
  GlobalKey<ScaffoldState> dkey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: dkey,
      body: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              width: 260,
              child: SideMenu(
                onTap: (idx) {
                  pageIdx = idx;
                  _pageController.jumpToPage(idx);
                },
              )),
          Expanded(
            child: PageView(
              physics: NeverScrollableScrollPhysics(),
              controller: _pageController,
              children: pages,
            ),
            flex: 1,
          )
        ],
      ),
    );
  }
}

///
///
///
///
///
///

class Layout extends StatefulWidget {
  @override
  _LayoutState createState() => _LayoutState();
}

class _LayoutState extends State<Layout> {
  @override
  void initState() {
    super.initState();
    initWindowSize();
  }

  initWindowSize() async {
    WindowUtils.setSize(Size(1174, 883));
    WindowUtils.centerWindow();
  }

  @override
  Widget build(BuildContext context) {
    return Responsive(
      mobile: MobilePage(),
      tablet: PadPage(),
      desktop: DesktopPage(),
    );
  }
}
