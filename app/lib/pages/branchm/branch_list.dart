import 'package:app/comps/app_table.dart';
import 'package:app/comps/app_table_cell.dart';
import 'package:app/comps/base_datatable.dart';
import 'package:app/comps/status_enum.dart';
import 'package:app/net/branchm.dart';
import 'package:app/net/net.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class BranchListPage extends StatefulWidget {
  String? kwd;
  BranchListPage({Key? key, this.kwd}) : super(key: key);

  @override
  _BranchApprovalPageState createState() => _BranchApprovalPageState();
}

class _BranchApprovalPageState extends State<BranchListPage> {
  int _page = 1;
  int _pageSize = 30;
  var datasource = [];
  late var pageInfo;
  String kwd = "";

  List<String> cols = [
    "SUBSIDIARY",
    "BRANCH NAME",
    "BRANCH CODE",
    "LOCATION",
    "CREATED BY",
    "APPROVED BY",
    "START DATE",
    "END DATE",
    "APPROVAL STATUS",
  ];
  List<String> keys = [
    "subsidiary",
    "name",
    "code",
    "location",
    "creatorName",
    "approvedName",
    "startDate",
    "endDate",
    "statusCode",
  ];
  bool resort = false;
  String sortKey = "";

  List<double> widths = [
    122.0,
    140.0,
    105.0,
    300.0,
    95.0,
    120.0,
    110.0,
    110.0,
    230.0,
  ];

  List<Widget> rows = [];
  bool isloading = true;
  @override
  void initState() {
    super.initState();
    sortKey = keys.first;
    netBranchList(null);
  }

  @override
  void didUpdateWidget(BranchListPage oldWidget) {
    super.didUpdateWidget(oldWidget);
    kwd = widget.kwd ?? "";
    _cleanData();
    netBranchList(null);
  }

  netBranchList(RefreshController? c) async {
    var fi = {
      "_page": _page,
      "_pageSize": _pageSize,
      "_keyword": kwd,
      "_orderby": resort ? "${sortKey} desc" : "${sortKey} asc",
    };
    var res = await branchList(fi);
    var list = res["list"];
    datasource.addAll(list);
    updateUI();
    isloading = false;
    setState(() {});
    c!.refreshCompleted();
    c.loadComplete();
  }

  rowSelectHandle(dict) {
    Navigator.pushNamed(context, "/read", arguments: dict["_id"]);
  }

  updateUI() {
    rows.clear();
    for (var i = 0; i < datasource.length; i++) {
      var dict = datasource[i];
      var row = InkWell(
        onTap: () {
          rowSelectHandle(dict);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(children: [
              AppTableCell(width: widths[0], text: "BAZLA IMAC"),
              AppTableCell(width: widths[1], text: dict["name"] ?? ""),
              AppTableCell(width: widths[2], text: dict["code"] ?? ""),
              AppTableCell(width: widths[3], text: dict["location"] ?? ""),
              AppTableCell(width: widths[4], text: dict["creatorName"] ?? ""),
              AppTableCell(width: widths[5], text: "---"),
              AppTableCell(width: widths[6], text: dict["startDate"] ?? ""),
              AppTableCell(width: widths[7], text: dict["endDate"] ?? ""),
              AppTableCell(
                  width: widths[8],
                  child: Row(
                    children: [
                      ClipOval(
                        child: Container(
                          width: 8,
                          height: 8,
                          color: StatusColors[dict["statusCode"] as int],
                        ),
                      ),
                      SizedBox(width: 10),
                      Text(
                        dict["statusName"] ?? "",
                        style: TextStyle(
                            fontSize: 12,
                            fontFamily: "sli",
                            color: Color(0xff080808)),
                      )
                    ],
                  )),
            ]),
            Container(
              height: 0.5,
              width: widths.reduce((a, b) => a + b),
              color: Color(0xffe0e0e0),
            )
          ],
        ),
      );
      rows.add(row);
    }
  }

  _cleanData() {
    _page = 1;
    rows.clear();
    datasource.clear();
  }

  _header() {
    List<AppTable3Header> items = [];
    for (var i = 0; i < cols.length; i++) {
      AppTable3Header ii = AppTable3Header(
        sortkey: keys[i],
        text: cols[i],
        width: widths[i],
        isup: !resort,
        onTap: (key) {
          isloading = true;
          setState(() {});
          sortKey = key;
          _page = 1;
          resort = !resort;
          datasource.clear();
          netBranchList(null);
        },
      );
      items.add(ii);
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: items),
        Container(
          height: 0.5,
          width: widths.reduce((a, b) => a + b),
          color: Color(0xffe0e0e0),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppTable3(
      isLoading: isloading,
      onRefresh: (RefreshController c) {
        isloading = true;
        setState(() {});
        _cleanData();
        netBranchList(c);
      },
      onLoadMore: (RefreshController c) {
        isloading = true;
        setState(() {});
        _page += 1;
        netBranchList(c);
      },
      children: [_header(), ...rows],
    );
  }
}
