import 'package:app/base/base_page_router.dart';
import 'package:app/pages/devicem/add.dart';
import 'package:app/pages/devicem/approval_add.dart';
import 'package:app/pages/devicem/approval_edit.dart';
import 'package:app/pages/devicem/delete.dart';
import 'package:app/pages/devicem/edit.dart';
import 'package:app/pages/devicem/list.dart';
import 'package:app/pages/devicem/read.dart';
import 'package:flutter/material.dart';

class DeviceIndex extends StatefulWidget {
  DeviceIndex({Key? key}) : super(key: key);

  @override
  _DeviceIndexState createState() => _DeviceIndexState();
}

class _DeviceIndexState extends State<DeviceIndex> {
  Map<String, Widget> dict = {
    '/read': DeviceReadPage(),
    '/list': DeviceListPage(),
    "/add": DeviceAddPage(),
    "/edit": DeviceEditPage(),
    "/appadd": DeviceApprovalAddPage(),
    "/appedit": DeviceApprovalEditPage(),
    "/delete": DeviceDeletePage()
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BasePageRouter(
      initPath: "/list",
      dict: dict,
    ));
  }
}
