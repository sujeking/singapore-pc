import 'package:app/comps/active_app_view.dart';
import 'package:app/comps/app_search_view.dart';
import 'package:app/comps/appitem.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/comps/tab_view.dart';
import 'package:app/net/tenantm.dart';
import 'package:app/pages/ipm/approval_list.dart';
import 'package:app/pages/ipm/ip_list.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';

class IPListIndexPage extends StatefulWidget {
  IPListIndexPage({Key? key}) : super(key: key);

  @override
  _IPListIndexPageState createState() => _IPListIndexPageState();
}

class _IPListIndexPageState extends State<IPListIndexPage>
    with SingleTickerProviderStateMixin {
  var apps = [];
  String kwd = "";

// action
  addIPHandle() {
    Navigator.pushNamed(context, "/add");
  }

  viewIPHandle() {
    Navigator.pushNamed(context, "/read");
  }

  late TabController tabController;
  @override
  void initState() {
    super.initState();
    netApps();
    tabController = TabController(length: 2, vsync: this);
  }

  netApps() async {
    var res = await tenantActives();
    apps = res["data"];
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TopBar(
              backTitle: "IP Mappings",
            ),
            PageTitleView(title: "IP Management"),
            ActiveAppView(
              currentIdx: -1,
              apps: apps,
            ),
            AppSearchView(
                onSubmit: (e) {
                  kwd = e;
                  setState(() {});
                },
                btnTitle: "Add IP",
                onTap: addIPHandle),
            AppTabView(
                titles: ["IP LIST", "APPROVAL LIST"],
                tabController: tabController),
            Expanded(
              child: TabBarView(
                physics: NeverScrollableScrollPhysics(),
                controller: tabController,
                children: [
                  IPListPage(
                    kwd: kwd,
                  ),
                  IPApprovalListPage(
                    kwd: kwd,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
