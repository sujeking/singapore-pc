import 'package:app/comps/app_read_item.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/material.dart';

class DeviceDeletePage extends StatefulWidget {
  DeviceDeletePage({Key? key}) : super(key: key);

  @override
  _DeviceDeletePageState createState() => _DeviceDeletePageState();
}

class _DeviceDeletePageState extends State<DeviceDeletePage> {
  deleteHandle() {}

  rowOne() {
    return Container(
      height: 100,
      padding: EdgeInsets.only(left: 80),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(title: "Request By", value: "Blasz"),
          ),
          Expanded(
            child: AppReadItem(title: "Approved By", value: "RIDZUAN"),
          ),
          Expanded(
            child: Container(),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
      height: 100,
      padding: EdgeInsets.only(left: 80),
      margin: EdgeInsets.only(top: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
                title: "Device Id", value: "Blasz64a564sda897d9asd4"),
          ),
          Expanded(
            child: AppReadItem(title: "Device Name", value: "Blasz"),
          ),
          Expanded(
            child: AppReadItem(title: "Assets Register", value: "Jeking"),
          ),
        ],
      ),
    );
  }

  rowThree() {
    return Container(
      height: 100,
      padding: EdgeInsets.only(left: 80),
      margin: EdgeInsets.only(top: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
                title: "MDM Registration Date", value: "10-12-2021"),
          ),
          Expanded(
            child: AppReadItem(
                title: "MDMDevice Registration Date", value: "10-12-2021"),
          ),
          Expanded(
            child:
                AppReadItem(title: "Device De-Registration Date", value: "-"),
          ),
        ],
      ),
    );
  }

  ActionView() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      child: Row(
        children: [
          InkWell(
            onTap: deleteHandle,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 50, vertical: 8),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(width: 1, color: Colors.red)),
              child: Text(
                "Submit For Deletion Approval",
                style: TextStyle(color: Colors.red),
              ),
            ),
          ),
          SizedBox(width: 12),
          InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 50, vertical: 8),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(width: 1, color: Colors.blue)),
              child: Text(
                "Cansel",
                style: TextStyle(color: Colors.blue),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Stack(
          children: [
            Column(
              children: [
                TopBar(
                  backTitle: "Device Management / View Device / Delete Device",
                  popAction: () {
                    Navigator.pop(context);
                  },
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        PageTitleView(title: "Delete Device"),
                        SizedBox(height: 80),
                        rowOne(),
                        rowTwo(),
                        rowThree(),
                      ],
                    ),
                  ),
                )
              ],
            ),
            Positioned(
              bottom: 80,
              left: 0,
              child: ActionView(),
            )
          ],
        ),
      ),
    );
  }
}
