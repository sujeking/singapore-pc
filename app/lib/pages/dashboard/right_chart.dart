import 'dart:math';

import 'package:app/comps/app_filtter_view.dart';
import 'package:app/comps/active_app_view.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/dashboardm.dart';
import 'package:app/net/tenantm.dart';
import 'package:app/pages/dashboard/bashboard_listview.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/material.dart';

import 'package:charts_flutter/flutter.dart' as charts;

class OrdinalSales {
  final String year;
  final int sales;

  OrdinalSales(
    this.year,
    this.sales,
  );
}

class RightChart extends StatefulWidget {
  int? idx;
  RightChart({Key? key, this.idx}) : super(key: key);

  @override
  _RightChartState createState() => _RightChartState();
}

class _RightChartState extends State<RightChart> {
  int idx = 0;
  var keys = ["Today", "LastWeek", "LastMonth", "Last3Month", "Last6Month"];
  List<charts.Series<dynamic, String>> seriesList = [];
  @override
  void initState() {
    super.initState();
    netData();
  }

  @override
  void didUpdateWidget(RightChart oldWidget) {
    super.didUpdateWidget(oldWidget);
    seriesList.clear();
    idx = widget.idx ?? 0;
    netData();
  }

  netData() async {
    var res = await statException(keys[idx]);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      updateUI(res["data"]);
    }
    setState(() {});
  }

  updateUI(data) {
    List<OrdinalSales> data1 = [];
    List<OrdinalSales> data2 = [];

    var tags = data["tags"];
    var ipNotMatch = data["ipNotMatch"];
    var deviceNotMatch = data["deviceNotMatch"];

    for (var i = 0; i < tags.length; i++) {
      var item = OrdinalSales(tags[i], ipNotMatch[i]);
      data1.add(item);
    }

    for (var i = 0; i < tags.length; i++) {
      var item = OrdinalSales(tags[i], deviceNotMatch[i]);
      data2.add(item);
    }
    seriesList = [
      charts.Series<OrdinalSales, String>(
        id: 'Sales',
        colorFn: (_, __) => charts.ColorUtil.fromDartColor(Color(0xffd8e7e2)),
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: data1,
      ),
      charts.Series<OrdinalSales, String>(
        id: 'Sales',
        colorFn: (_, __) => charts.ColorUtil.fromDartColor(Color(0xfff9eed8)),
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: data2,
      )
    ];
  }

  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(
      seriesList,
      animate: false,
      barGroupingType: charts.BarGroupingType.grouped,
    );
  }
}
