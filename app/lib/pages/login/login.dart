import 'package:app/net/net.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  loginHandle() {
    Navigator.of(context).pushNamed("/layout");
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 2), szw);
  }

  szw() {
    // var res = await $http("delete", "/ip/41bc52397a94e252c6fe9ae2dd513754");
    // print(res);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.all(30),
        child: Column(
          children: [
            Row(
              children: [
                Image.asset(
                  "assets/img/login_icon.png",
                  width: 39,
                  height: 39,
                ),
                SizedBox(
                  width: 12,
                ),
                Text(
                  "SSO",
                  style: TextStyle(
                      fontSize: 35,
                      fontFamily: "sre",
                      fontWeight: FontWeight.w600),
                )
              ],
            ),
            Expanded(
                child: Container(
              constraints: BoxConstraints(maxWidth: 450),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Log in",
                    style: TextStyle(
                        fontSize: 87,
                        fontFamily: "sli",
                        fontWeight: FontWeight.w700),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  // Enter your username
                  TextField(
                    style: TextStyle(fontSize: 24),
                    decoration: InputDecoration(
                        fillColor: Color(0xfff7f7f4),
                        filled: true,
                        hintText: "Enter your username",
                        hintStyle: TextStyle(
                            fontSize: 24,
                            fontFamily: "sli",
                            color: Color(0xff080808).withOpacity(0.5)),
                        isCollapsed: true,
                        contentPadding: EdgeInsets.fromLTRB(23, 15, 0, 15),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide:
                                BorderSide(width: 1, color: Color(0xffe0e0dd))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide(
                                width: 1, color: Color(0xffe0e0dd)))),
                  ),
                  SizedBox(
                    height: 13,
                  ),
                  // Password
                  TextField(
                    style: TextStyle(fontSize: 24),
                    decoration: InputDecoration(
                        fillColor: Color(0xfff7f7f4),
                        filled: true,
                        hintText: "Password",
                        hintStyle: TextStyle(
                            fontSize: 24,
                            fontFamily: "sli",
                            color: Color(0xff080808).withOpacity(0.5)),
                        isCollapsed: true,
                        contentPadding: EdgeInsets.fromLTRB(23, 15, 0, 15),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide:
                                BorderSide(width: 1, color: Color(0xffe0e0dd))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide(
                                width: 1, color: Color(0xffe0e0dd)))),
                  ),
                  SizedBox(
                    height: 13,
                  ),
                  InkWell(
                    onTap: loginHandle,
                    child: Container(
                      height: 60,
                      decoration: BoxDecoration(
                        color: Color(0xfffff5f1),
                        border: Border.all(width: 1, color: Color(0xffffc5c5)),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      width: double.infinity,
                      child: Center(
                        child: Text(
                          "Log In",
                          style: TextStyle(
                              fontFamily: "sre",
                              fontSize: 24,
                              color: Color(0xfffe4850)),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 53,
                  ),
                  InkWell(
                    child: Text(
                      "Forget password?",
                      style: TextStyle(
                        color: Color(0x83080808),
                        fontSize: 24,
                        decoration: TextDecoration.underline,
                        fontFamily: "sli",
                      ),
                    ),
                  )
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
