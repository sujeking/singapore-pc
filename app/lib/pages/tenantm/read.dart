import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_pick_field.dart';
import 'package:app/comps/app_read_item.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/tenantm.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class TenantReadPage extends StatefulWidget {
  TenantReadPage({Key? key}) : super(key: key);

  @override
  _TenantReadPageState createState() => _TenantReadPageState();
}

class _TenantReadPageState extends State<TenantReadPage>
    with SingleTickerProviderStateMixin {
  Map<String, dynamic> info = {};
  late String idstr;
  int tab_index = 0;
  late TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(initialIndex: 0, vsync: this, length: 2);
    Future.delayed(Duration.zero, () {
      String id = ModalRoute.of(context)?.settings.arguments as String;
      idstr = id;
      netTenantDetail(id);
    });
  }

  netTenantDetail(String id) async {
    if (null == id) {
      return;
    }
    var res = await tenantDetail(id);
    info = res;
    setState(() {});
  }

  netTenantDel(String id) async {
    var res = await tenantDelete(id);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Navigator.of(context).pop();
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  rowOne() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PageTitleView(title: "View Details"),
          Container(
            padding: EdgeInsets.symmetric(vertical: 40),
            child: Row(
              children: [
                ClipOval(
                  child: Container(
                    color: Color(0xfff5f5f5),
                    width: 250,
                    height: 250,
                    child: CachedNetworkImage(
                        imageUrl: info["image"] ?? "",
                        errorWidget: (
                          BuildContext context,
                          String url,
                          dynamic error,
                        ) =>
                            Icon(
                              Icons.person,
                              size: 125,
                              color: Color(0xff999999),
                            )),
                  ),
                ),
                SizedBox(
                  width: 50,
                ),
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      info["name"] ?? "",
                      style: TextStyle(
                        fontSize: 60,
                        fontFamily: "sbo",
                        color: Color(0xff000000),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    AppReadItemSmall(
                        title: "Description (optional)",
                        value: info["description"] ?? ""),
                  ],
                ))
              ],
            ),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    var list = [
      [
        {"title": "Tenant Start Date", "value": info["startDate"] ?? ""},
        {"title": "Tenant End Date", "value": info["endDate"] ?? ""},
      ],
      [
        {"title": "Tenant Secret", "value": info["clientUniqueId"] ?? ""},
        {"title": "Cert Endpoint", "value": info["szw"] ?? "--"},
      ],
      [
        {"title": "Token Endpoint", "value": info["szw"] ?? "--"},
        {"title": "Auth Endpoint", "value": info["szw"] ?? "--"},
      ],
      [
        {"title": "UserInfo Endpoint", "value": info["szw"] ?? "--"},
        {"title": "Webhook URL", "value": info["webhookUrl"] ?? ""},
      ],
      [
        {"title": "Add Role URL", "value": info["addRoleUrl"]},
        {"title": "Delete Role URL", "value": info["deleteRoleUrl"] ?? ""},
      ],
    ];

    List<Widget> rows = [];

    for (var i = 0; i < list.length; i++) {
      var arr = list[i];
      var leftdata = arr[0];
      var rightdata = arr[1];
      var row = Container(
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                    child: AppReadItemSmall(
                        title: leftdata["title"] as String,
                        value: leftdata["value"] as String)),
                Expanded(
                    child: AppReadItemSmall(
                        title: rightdata["title"] as String,
                        value: rightdata["value"] as String)),
              ],
            ),
            SizedBox(height: 60)
          ],
        ),
      );
      rows.add(row);
    }

    return Column(
      children: rows,
    );
  }

  rowThree() {
    var webOrigins = info["webOrigins"] ?? [];
    var redirectUris = info["redirectUris"] ?? [];
    var weblist = [];
    var redlist = [];

    print(webOrigins);
    print(redirectUris);

    for (int i = 1; i <= webOrigins.length; i++) {
      var obj = redirectUris[i - 1];
      var item = {"title": "Web Origin" + i.toString(), "value": obj};
      weblist.add(item);
    }

    for (int i = 1; i <= redirectUris.length; i++) {
      var obj = redirectUris[i - 1];
      var item = {
        "title": "Authentication Callback URL_" + i.toString(),
        "value": obj
      };
      redlist.add(item);
    }

    var list = [
      {"title": "Web Origin", "items": weblist},
      {"title": "Authentication Callback URL", "items": redlist},
    ];

    List<Widget> cols = [];
    for (var i = 0; i < list.length; i++) {
      var dict = list[i];

      List items = dict["items"] as List;
      var itemsw = [];

      for (var j = 0; j < items.length; j++) {
        var dd = items[j] as Map;
        var ii = Container(
            padding: EdgeInsets.only(bottom: 60),
            child: AppReadItemSmall(title: dd["title"], value: dd["value"]));
        itemsw.add(ii);
      }

      var col = Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              dict["title"] as String,
              style: TextStyle(fontSize: 25, fontFamily: "sbo"),
            ),
            SizedBox(
              height: 40,
            ),
            ...itemsw,
          ],
        ),
      );
      cols.add(col);
    }

    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: Row(
        children: [
          Expanded(child: cols[0]),
          Expanded(child: cols[1]),
        ],
      ),
    );
  }

  tabView() {
    return Container(
      padding: EdgeInsets.only(top: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Column(
                children: [
                  InkWell(
                    onTap: () {
                      tab_index = 0;
                      tabController.animateTo(tab_index);
                      setState(() {});
                    },
                    child: Container(
                        constraints: BoxConstraints(minWidth: 120),
                        alignment: Alignment.center,
                        child: Text(
                          "ULTRON +",
                          style: TextStyle(
                              fontSize: 20,
                              color: tab_index == 0
                                  ? Color(0xff080808)
                                  : Color(0xff999999),
                              fontFamily: "sf",
                              fontWeight: tab_index == 0
                                  ? FontWeight.bold
                                  : FontWeight.normal),
                        )),
                  ),
                  Container(
                    height: 2,
                    width: 120,
                    color: tab_index == 0 ? Color(0xff000000) : Colors.white,
                    margin: EdgeInsets.only(top: 12),
                  )
                ],
              ),
              SizedBox(
                width: 12,
              ),
              Column(
                children: [
                  InkWell(
                    onTap: () {
                      tab_index = 1;
                      tabController.animateTo(tab_index);
                      setState(() {});
                    },
                    child: Container(
                        constraints: BoxConstraints(minWidth: 120),
                        alignment: Alignment.center,
                        child: Text(
                          "EXPONENTIAL PRO",
                          style: TextStyle(
                              fontSize: 20,
                              color: tab_index == 1
                                  ? Color(0xff080808)
                                  : Color(0xff999999),
                              fontFamily: "sf",
                              fontWeight: tab_index == 1
                                  ? FontWeight.bold
                                  : FontWeight.normal),
                        )),
                  ),
                  Container(
                    height: 2,
                    width: 120,
                    color: tab_index == 1 ? Color(0xff000000) : Colors.white,
                    margin: EdgeInsets.only(top: 12),
                  )
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  ActionView() {
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: Row(
        children: info["statusCode"] == 1
            ? [
                AppBottomBtn(
                    label: "Back",
                    color: Colors.blue,
                    onTap: () {
                      Navigator.of(context).pop();
                    }),
              ]
            : [
                AppBottomBtn(
                    label: "Edit Tenant",
                    color: Colors.blue,
                    onTap: () {
                      Navigator.of(context).pushNamed("/edit");
                    }),
                SizedBox(width: 12),
                AppBottomBtn(
                    label: "Delete Tenant",
                    color: Colors.red,
                    onTap: () {
                      showDelAlertHandle(context, () {
                        netTenantDel(idstr);
                      });
                    }),
              ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Column(
          children: [
            TopBar(
              backTitle: "Tenant Management / View Tenant",
              popAction: () {
                Navigator.pop(context);
              },
            ),
            Expanded(
              child: ListView(
                controller: ScrollController(),
                children: [
                  rowOne(),
                  rowTwo(),
                  rowThree(),
                  // action
                  Offstage(
                    offstage: info["statusCode"] == null,
                    child: ActionView(),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
