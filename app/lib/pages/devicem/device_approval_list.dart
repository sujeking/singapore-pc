import 'package:app/comps/app_table.dart';
import 'package:app/comps/app_table_cell.dart';
import 'package:app/comps/base_datatable.dart';
import 'package:app/net/devicem.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class DeviceApprovalList extends StatefulWidget {
  String? kwd;
  DeviceApprovalList({Key? key, this.kwd}) : super(key: key);

  @override
  _DeviceApprovalListState createState() => _DeviceApprovalListState();
}

class _DeviceApprovalListState extends State<DeviceApprovalList> {
  String kwd = "";
  List<String> cols = [
    'DEVICE NAME',
    'DEVICE ID',
    'ASSET REGISTER',
    'MDM REGISTRATION DATE',
    'REQUESTED BY',
    'REQUESTED DATE',
    'ACTION',
  ];
  List<String> keys = [
    "name",
    "deviceHardwareId",
    "assetRegister",
    "mdmRegistrationDate",
    "creatorName",
    "createdAt",
    "operationType",
  ];
  bool resort = false;
  String sortKey = "";
  List<double> widths = [
    150,
    230,
    150,
    200,
    160,
    120,
    300,
  ];

  List<Widget> rows = [];
  int _page = 1;
  int _pageSize = 30;
  var datasource = [];

  bool isloading = true;
  @override
  void initState() {
    super.initState();
    sortKey = keys.first;
    netDeviceAppList(null);
  }

  @override
  void didUpdateWidget(DeviceApprovalList oldWidget) {
    super.didUpdateWidget(oldWidget);
    _page = 1;
    rows.clear();
    datasource.clear();
    kwd = this.kwd;
    netDeviceAppList(null);
  }

  netDeviceAppList(RefreshController? c) async {
    var fi = {
      "_page": _page,
      "_pageSize": _pageSize,
      "_keyword": kwd,
      "_orderby": resort ? "${sortKey} desc" : "${sortKey} asc",
    };
    var res = await deviceAppList(fi);
    var list = res["list"];
    print(res);
    datasource.addAll(list);
    updateUI();
    isloading = false;
    setState(() {});
    c!.refreshCompleted();
    c.loadComplete();
  }

  rowSelectHandle(dict) {
    var type = dict["operationType"];
    var newData = dict["newData"];
    if (type == "create") {
      Navigator.pushNamed(context, "/appadd", arguments: newData);
    } else {
      Navigator.pushNamed(context, "/appedit", arguments: dict);
    }
  }

  updateUI() {
    rows.clear();
    for (var i = 0; i < datasource.length; i++) {
      var item = datasource[i];
      var dict = item["approval"];
      var newData = dict["newData"];
      var row = InkWell(
        onTap: () {
          rowSelectHandle(dict);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(children: [
              AppTableCell(width: widths[0], text: newData["name"] ?? ""),
              AppTableCell(
                  width: widths[1], text: newData["deviceHardwareId"] ?? ""),
              AppTableCell(
                  width: widths[2], text: newData["assetRegister"] ?? ""),
              AppTableCell(
                  width: widths[3], text: newData["mdmRegistrationDate"] ?? ""),
              AppTableCell(width: widths[4], text: dict["creatorName"] ?? ""),
              AppTableCell(width: widths[5], text: newData["createdAt"] ?? ""),
              AppTableCell(width: widths[6], text: dict["operationType"] ?? ""),
            ]),
            Container(
              height: 0.5,
              width: widths.reduce((a, b) => a + b),
              color: Color(0xffe0e0e0),
            )
          ],
        ),
      );
      rows.add(row);
    }
  }

  _cleanData() {
    _page = 1;
    rows.clear();
    datasource.clear();
  }

  _header() {
    List<AppTable3Header> items = [];
    for (var i = 0; i < cols.length; i++) {
      AppTable3Header ii = AppTable3Header(
        sortkey: keys[i],
        text: cols[i],
        isup: !resort,
        width: widths[i],
        onTap: (key) {
          isloading = true;
          setState(() {});
          sortKey = key;
          _page = 1;
          resort = !resort;
          datasource.clear();
          netDeviceAppList(null);
        },
      );
      items.add(ii);
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: items),
        Container(
          height: 0.5,
          width: widths.reduce((a, b) => a + b),
          color: Color(0xffe0e0e0),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppTable3(
      isLoading: isloading,
      onRefresh: (RefreshController c) {
        isloading = true;
        setState(() {});
        _cleanData();
        netDeviceAppList(c);
      },
      onLoadMore: (RefreshController c) {
        isloading = true;
        setState(() {});
        _page += 1;
        netDeviceAppList(c);
      },
      children: [_header(), ...rows],
    );
  }
}
