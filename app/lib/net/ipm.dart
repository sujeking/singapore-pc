import 'net.dart';

ipAdd(Map<String, dynamic> data) async {
  var res = await $http("post", "/ip", data: data);
  return res;
}

ipDelete(String id) async {
  var res = await $http("DELETE", "/ip/" + id);
  return res;
}

ipUpdate(Map<String, dynamic> data) async {
  var res = await $http("put", "/ip/" + data["_id"], data: data);
  return res;
}

ipList(Map<String, dynamic> data) async {
  var res = await $http("get", "/ip", data: data);
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

ipDetail(String id) async {
  var res = await $http("get", "/ip/${id}");
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

// approval

ipAppList(Map<String, dynamic> data) async {
  var res = await $http("get", "/ip/approval/index", data: data);
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

ipAppPass(String id) async {
  var res = await $http("post", "/ip/${id}/approval/pass");
  return res;
}

ipAppReject(String id) async {
  var res = await $http("post", "/ip/${id}/approval/reject");
  return res;
}
