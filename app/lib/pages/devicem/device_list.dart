import 'package:app/comps/app_table.dart';
import 'package:app/comps/app_table_cell.dart';
import 'package:app/comps/base_datatable.dart';
import 'package:app/comps/status_enum.dart';
import 'package:app/net/devicem.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class DeviceList extends StatefulWidget {
  String? kwd;
  DeviceList({Key? key, this.kwd}) : super(key: key);

  @override
  _DeviceListState createState() => _DeviceListState();
}

class _DeviceListState extends State<DeviceList> {
  String kwd = "";
  List<String> cols = [
    'DEVICE NAME',
    'DEVICE ID',
    'ASSET REGISTER',
    'MDM REGISTRATION DATE',
    'CREATED BY',
    'APPROVED BY',
    'START DATE',
    'END DATE',
    'APPROVAL STATUS',
  ];
  List<String> keys = [
    "name",
    "deviceHardwareId",
    "assetRegister",
    "mdmRegistrationDate",
    "creatorName",
    "approvedAt",
    "startDate",
    "endDate",
    "statusCode",
  ];
  bool resort = false;
  String sortKey = "";
  List<double> widths = [
    130.0,
    233.0,
    130.0,
    200.0,
    130.0,
    130.0,
    130.0,
    130.0,
    222.0,
  ];

  List<Widget> rows = [];

  int _page = 1;
  int _pageSize = 30;
  var datasource = [];
  bool isloading = true;
  @override
  void initState() {
    super.initState();
    sortKey = keys.first;
    netDevicesList(null);
  }

  @override
  void didUpdateWidget(DeviceList oldWidget) {
    super.didUpdateWidget(oldWidget);
    kwd = widget.kwd ?? "";
    _cleanData();
    netDevicesList(null);
  }

  netDevicesList(RefreshController? c) async {
    var fi = {
      "_page": _page,
      "_pageSize": _pageSize,
      "_keyword": kwd,
      "_orderby": resort ? "${sortKey} desc" : "${sortKey} asc",
    };
    var res = await deviceList(fi);
    var list = res["list"];
    datasource.addAll(list);
    print(datasource);
    updateUI();
    isloading = false;
    setState(() {});

    c!.refreshCompleted();
    c.loadComplete();
  }

  rowSelectHandle(dict) {
    var id = dict["_id"];
    Navigator.pushNamed(context, "/read", arguments: id);
  }

  updateUI() {
    rows.clear();
    for (var i = 0; i < datasource.length; i++) {
      var dict = datasource[i];
      var row = InkWell(
        onTap: () {
          rowSelectHandle(dict);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(children: [
              AppTableCell(width: widths[0], text: dict["name"] ?? ""),
              AppTableCell(
                  width: widths[1], text: dict["deviceHardwareId"] ?? ""),
              AppTableCell(width: widths[2], text: dict["assetRegister"] ?? ""),
              AppTableCell(
                  width: widths[3], text: dict["mdmRegistrationDate"] ?? ""),
              AppTableCell(width: widths[4], text: dict["creatorName"] ?? ""),
              AppTableCell(width: widths[5], text: dict["approvedAt"] ?? ""),
              AppTableCell(width: widths[6], text: dict["startDate"] ?? ""),
              AppTableCell(width: widths[7], text: dict["endDate"] ?? ""),
              AppTableCell(
                  width: widths[8],
                  child: Row(
                    children: [
                      ClipOval(
                        child: Container(
                          width: 8,
                          height: 8,
                          color: StatusColors[dict["statusCode"] as int],
                        ),
                      ),
                      SizedBox(width: 10),
                      Text(
                        dict["statusName"] ?? "",
                        style: TextStyle(
                            fontSize: 12,
                            fontFamily: "sli",
                            color: Color(0xff080808)),
                      )
                    ],
                  )),
            ]),
            Container(
              height: 0.5,
              width: widths.reduce((a, b) => a + b),
              color: Color(0xffe0e0e0),
            )
          ],
        ),
      );
      rows.add(row);
    }
  }

  _cleanData() {
    _page = 1;
    rows.clear();
    datasource.clear();
  }

  _header() {
    List<AppTable3Header> items = [];
    for (var i = 0; i < cols.length; i++) {
      AppTable3Header ii = AppTable3Header(
        sortkey: keys[i],
        text: cols[i],
        width: widths[i],
        isup: !resort,
        onTap: (key) {
          isloading = true;
          setState(() {});
          sortKey = key;
          _page = 1;
          resort = !resort;
          datasource.clear();
          netDevicesList(null);
        },
      );
      items.add(ii);
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: items),
        Container(
          height: 0.5,
          width: widths.reduce((a, b) => a + b),
          color: Color(0xffe0e0e0),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppTable3(
      isLoading: isloading,
      onRefresh: (RefreshController c) {
        isloading = true;
        setState(() {});
        _cleanData();
        netDevicesList(c);
      },
      onLoadMore: (RefreshController c) {
        isloading = true;
        setState(() {});
        _page += 1;
        netDevicesList(c);
      },
      children: [_header(), ...rows],
    );
  }
}
