import 'package:app/comps/app_read_item.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/material.dart';

class TenantDelPage extends StatefulWidget {
  TenantDelPage({Key? key}) : super(key: key);

  @override
  _TenantDelPageState createState() => _TenantDelPageState();
}

class _TenantDelPageState extends State<TenantDelPage>
    with SingleTickerProviderStateMixin {
  rowOne() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PageTitleView(title: "Delete Tenant"),
          Container(
            padding: EdgeInsets.symmetric(vertical: 40),
            child: Row(
              children: [
                CircleAvatar(
                    backgroundColor: Color(0xfff5f5f5),
                    radius: 125,
                    backgroundImage: AssetImage("assets/img/user_avatar.jpeg")),
                SizedBox(
                  width: 50,
                ),
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "ULTRON +",
                      style: TextStyle(
                        fontSize: 60,
                        fontFamily: "sbo",
                        color: Color(0xff000000),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    AppReadItemSmall(
                        title: "Description (optional)",
                        value:
                            "Ultron+ is one of the first amazing iPad app in ATW."),
                  ],
                ))
              ],
            ),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    var list = [
      [
        {"title": "Tenant Start Date", "value": "01-01-2015"},
        {"title": "Tenant End Date", "value": ""},
      ],
      [
        {
          "title": "Tenant Secret",
          "value": "167f4907-d07c-4eef-8270-287857ec3e54"
        },
        {
          "title": "Cert Endpoint",
          "value":
              "https://192.168.11.115:8443/auth/realms/oidc/protocol/openid-connect/certs"
        },
      ],
      [
        {
          "title": "Token Endpoint",
          "value":
              "https://192.168.11.115:8443/auth/realms/oidc/protocol/openid-connect/token"
        },
        {
          "title": "Auth Endpoint",
          "value":
              "https://192.168.11.115:8443/auth/realms/oidc/protocol/openid-connect/auth"
        },
      ],
      [
        {
          "title": "UserInfo Endpoint",
          "value":
              "https://192.168.11.115:8443/auth/realms/oidc/protocol/openid-connect/userinfo"
        },
        {"title": "Webhook URL", "value": "http://ultron.com/webhook"},
      ],
      [
        {
          "title": "Add Role URL",
          "value":
              "https://192.168.11.115:8443/auth/realms/oidc/protocol/openid-connect/userinfo"
        },
        {"title": "Delete Role URL", "value": "http://ultronl.com/webhook"},
      ],
    ];

    List<Widget> rows = [];

    for (var i = 0; i < list.length; i++) {
      var arr = list[i];
      var leftdata = arr[0];
      var rightdata = arr[1];
      var row = Container(
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                    child: AppReadItemSmall(
                        title: leftdata["title"] as String,
                        value: leftdata["value"] as String)),
                Expanded(
                    child: AppReadItemSmall(
                        title: rightdata["title"] as String,
                        value: rightdata["value"] as String)),
              ],
            ),
            SizedBox(height: 60)
          ],
        ),
      );
      rows.add(row);
    }

    return Column(
      children: rows,
    );
  }

  rowThree() {
    var list = [
      {
        "title": "Web Origin",
        "items": [
          {"title": "Web Origin1", "value": "https://192.168.11.115"},
          {
            "title": "Web Origin2",
            "value": "https://authuat.bibdat-tamwil.com"
          },
        ]
      },
      {
        "title": "Authentication Callback URL",
        "items": [
          {
            "title": "Authentication Callback URL_1",
            "value": "https://192.168.12.131:9801/Login/Login.aspx"
          },
          {
            "title": "Authentication Callback URL_2",
            "value": "https://192.168.11.115/*"
          },
        ]
      },
    ];

    List<Widget> cols = [];
    for (var i = 0; i < list.length; i++) {
      var dict = list[i];

      List items = dict["items"] as List;
      var itemsw = [];

      for (var j = 0; j < items.length; j++) {
        var dd = items[j] as Map;
        var ii = Container(
            padding: EdgeInsets.only(bottom: 60),
            child: AppReadItemSmall(title: dd["title"], value: dd["value"]));
        itemsw.add(ii);
      }

      var col = Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              dict["title"] as String,
              style: TextStyle(fontSize: 25, fontFamily: "sbo"),
            ),
            SizedBox(
              height: 40,
            ),
            ...itemsw,
          ],
        ),
      );
      cols.add(col);
    }

    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: Row(
        children: [
          Expanded(child: cols[0]),
          Expanded(child: cols[1]),
        ],
      ),
    );
  }

  int tab_index = 0;
  late TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(initialIndex: 0, vsync: this, length: 2);
  }

  tabView() {
    return Container(
      padding: EdgeInsets.only(top: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Column(
                children: [
                  InkWell(
                    onTap: () {
                      tab_index = 0;
                      tabController.animateTo(tab_index);
                      setState(() {});
                    },
                    child: Container(
                        constraints: BoxConstraints(minWidth: 120),
                        alignment: Alignment.center,
                        child: Text(
                          "ULTRON +",
                          style: TextStyle(
                              fontSize: 20,
                              color: tab_index == 0
                                  ? Color(0xff080808)
                                  : Color(0xff999999),
                              fontFamily: "sf",
                              fontWeight: tab_index == 0
                                  ? FontWeight.bold
                                  : FontWeight.normal),
                        )),
                  ),
                  Container(
                    height: 2,
                    width: 120,
                    color: tab_index == 0 ? Color(0xff000000) : Colors.white,
                    margin: EdgeInsets.only(top: 12),
                  )
                ],
              ),
              SizedBox(
                width: 12,
              ),
              Column(
                children: [
                  InkWell(
                    onTap: () {
                      tab_index = 1;
                      tabController.animateTo(tab_index);
                      setState(() {});
                    },
                    child: Container(
                        constraints: BoxConstraints(minWidth: 120),
                        alignment: Alignment.center,
                        child: Text(
                          "EXPONENTIAL PRO",
                          style: TextStyle(
                              fontSize: 20,
                              color: tab_index == 1
                                  ? Color(0xff080808)
                                  : Color(0xff999999),
                              fontFamily: "sf",
                              fontWeight: tab_index == 1
                                  ? FontWeight.bold
                                  : FontWeight.normal),
                        )),
                  ),
                  Container(
                    height: 2,
                    width: 120,
                    color: tab_index == 1 ? Color(0xff000000) : Colors.white,
                    margin: EdgeInsets.only(top: 12),
                  )
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Column(
          children: [
            TopBar(
              backTitle: "Tenant Management / View Tenant",
              popAction: () {
                Navigator.pop(context);
              },
            ),
            Expanded(
              child: ListView(
                controller: ScrollController(),
                children: [
                  rowOne(),
                  rowTwo(),
                  rowThree(),
                  // action
                  Container(
                    padding: EdgeInsets.only(bottom: 80),
                    child: Row(
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, "/edit");
                          },
                          child: Container(
                            width: 300,
                            alignment: Alignment.center,
                            padding: EdgeInsets.symmetric(
                                horizontal: 50, vertical: 8),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                border:
                                    Border.all(width: 1, color: Colors.red)),
                            child: Text(
                              "Submit For Deletion Approval",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 35,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            width: 300,
                            alignment: Alignment.center,
                            padding: EdgeInsets.symmetric(
                                horizontal: 50, vertical: 8),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                border:
                                    Border.all(width: 1, color: Colors.blue)),
                            child: Text(
                              "Cancel",
                              style: TextStyle(color: Colors.blue),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
