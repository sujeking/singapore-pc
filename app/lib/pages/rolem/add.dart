// ignore_for_file: import_of_legacy_library_into_null_safe

import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_pick_field.dart';
import 'package:app/comps/app_select_field.dart';
import 'package:app/comps/app_text_field.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/rolem.dart';
import 'package:app/net/tenantm.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class RoleAddPage extends StatefulWidget {
  RoleAddPage({Key? key}) : super(key: key);

  @override
  _RoleAddPageState createState() => _RoleAddPageState();
}

class _RoleAddPageState extends State<RoleAddPage> {
  var body = {
    "name": "",
    "tenantId": "",
    "startDate": "",
    "endDate": "",
  };

  List apps = [];
  List showApps = [];
  String selectedAppName = "";

  @override
  void initState() {
    super.initState();
    netTeantList();
  }

  netTeantList() async {
    Map<String, dynamic> fi = {
      "_page": 1,
      "_pageSize": 200,
      "_orderby": "name asc",
    };
    var res = await tenantList(fi);
    apps = res["list"];
    for (var dict in apps) {
      if (dict["statusCode"] == 2) {
        showApps.add(dict["name"]);
      }
    }
    setState(() {});
  }

  netAddRole() async {
    var res = await roleAdd(body);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show(
        "Successful Operation",
        context,
        gravity: Toast.CENTER,
        duration: Toast.LENGTH_LONG,
      );
      Future.delayed(
        Duration(seconds: 2),
        () {
          Navigator.of(context).pop();
        },
      );
    } else {
      Toast.show(res["message"], context, gravity: Toast.CENTER);
    }
  }

  rowOne() {
    return Container(
      height: 100,
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Row(
        children: [
          Expanded(
            child: AppTextField(
              value: body["name"],
              onChanged: (v) {
                body["name"] = v;
              },
              titleStr: "Role Name",
              must: true,
              placeholder: "Enter Role Name",
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: AppSelectField(
              items: showApps,
              titleStr: "Tenant",
              placeholder: "Select Tenant",
              must: true,
              value: selectedAppName,
              onChanged: (v) {
                body["tenantId"] = apps[v]["tenantId"];
                selectedAppName = showApps[v];
                setState(() {});
              },
            ),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
      height: 100,
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Row(
        children: [
          Expanded(
            child: AppPickField(
              value: body["startDate"],
              onTap: () {
                showCalenderHandle(context, (e) {
                  body["startDate"] = e;
                  setState(() {});
                });
              },
              titleStr: "Role Start Date",
              must: true,
              placeholder: "Select Role Start Date",
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: AppPickField(
              value: body["endDate"],
              onTap: () {
                showCalenderHandle(context, (e) {
                  body["endDate"] = e;
                  setState(() {});
                });
              },
              titleStr: "Role End Date (optional)",
              placeholder: "Select Role End Date",
            ),
          ),
        ],
      ),
    );
  }

  ActionView() {
    return Container(
      child: Row(
        children: [
          AppBottomBtn(
            label: 'Submit For Approval',
            color: Colors.blue,
            onTap: () {
              netAddRole();
            },
          ),
          SizedBox(width: 12),
          AppBottomBtn(
            label: 'Cancel',
            color: Color(0xffff0000),
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.fromLTRB(40, 12, 40, 80),
        child: Column(
          children: [
            TopBar(
              backTitle: "Add New Role",
              popAction: () {
                Navigator.pop(context);
              },
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  PageTitleView(title: "Add New Role"),
                  rowOne(),
                  rowTwo(),
                ],
              ),
            ),
            ActionView()
          ],
        ),
      ),
    );
  }
}
