import 'package:app/comps/app_approval_items.dart';
import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_read_item.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/devicem.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class DeviceApprovalEditPage extends StatefulWidget {
  DeviceApprovalEditPage({Key? key}) : super(key: key);

  @override
  _DeviceApprovalEditPageSatate createState() =>
      _DeviceApprovalEditPageSatate();
}

class _DeviceApprovalEditPageSatate extends State<DeviceApprovalEditPage> {
  Map<String, dynamic> body = {};

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      body = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
      setState(() {});
    });
  }

  netPassApproval() async {
    var id = body["newData"]["_id"];
    if (null == id) {
      return;
    }
    var res = await deviceAppPass(id);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show("Successful Operation", context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      Future.delayed(Duration(seconds: 2), () {
        Navigator.of(context).pop();
      });
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  netRejectApproval() async {
    var id = body["newData"]["_id"];
    if (null == id) {
      return;
    }
    var res = await deviceAppReject(id);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show("Successful Operation", context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      Future.delayed(Duration(seconds: 2), () {
        Navigator.of(context).pop();
      });
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  rowOne() {
    return Container(
      height: 100,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: AppReadItem(title: "Request By", value: body["creatorName"]),
          ),
          SizedBox(width: 50),
          Expanded(
            child:
                AppReadItem(title: "Requested Date", value: body["createdAt"]),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 12),
        child: Row(
          children: [
            Expanded(
                child: AppApprovalItems(
              title: "Device ID",
              oldValue: body["existingData"]["deviceHardwareId"] ?? "",
              newValue: body["newData"]["deviceHardwareId"] ?? "",
            )),
            SizedBox(
              width: 50,
            ),
            Expanded(
                child: AppApprovalItems(
              title: "Device Name",
              oldValue: body["existingData"]["name"] ?? "",
              newValue: body["newData"]["name"] ?? "",
            )),
          ],
        ));
  }

  rowThree() {
    return Container(
        margin: EdgeInsets.only(top: 48),
        child: Row(
          children: [
            Expanded(
                child: AppApprovalItems(
              title: "Asset Register",
              oldValue: body["existingData"]["assetRegister"] ?? "",
              newValue: body["newData"]["assetRegister"] ?? "",
            )),
            SizedBox(
              width: 50,
            ),
            Expanded(
                child: AppApprovalItems(
              title: "MDM Registration Date",
              oldValue: body["existingData"]["mdmRegistrationDate"] ?? "",
              newValue: body["newData"]["mdmRegistrationDate"] ?? "",
            )),
          ],
        ));
  }

  rowFour() {
    return Container(
        margin: EdgeInsets.only(top: 48),
        child: Row(
          children: [
            Expanded(
                child: AppApprovalItems(
              title: "Device Registration Date",
              oldValue: body["existingData"]["createdAt"] ?? "",
              newValue: body["newData"]["createdAt"] ?? "",
            )),
            SizedBox(
              width: 50,
            ),
            Expanded(
                child: AppApprovalItems(
              title: "Device De-Registration Date",
              oldValue: body["existingData"]["updatedAt"] ?? "",
              newValue: body["newData"]["updatedAt"] ?? "",
            )),
          ],
        ));
  }

  ActionView() {
    return Container(
      padding: EdgeInsets.only(top: 20),
      child: Row(
        children: [
          AppBottomBtn(
              label: "Approval Edit Device",
              color: Colors.blue,
              onTap: () {
                netPassApproval();
              }),
          SizedBox(
            width: 12,
          ),
          AppBottomBtn(
              label: "Reject Edit Device",
              color: Colors.red,
              onTap: () {
                netRejectApproval();
              }),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TopBar(
                backTitle: "Approval Device",
                popAction: () {
                  Navigator.pop(context);
                },
              ),
              PageTitleView(title: "Edit Device Approval"),
              SizedBox(
                height: 40,
              ),
              Expanded(
                child: ListView(
                  children: [
                    rowOne(),
                    rowTwo(),
                    rowThree(),
                    rowFour(),
                    ActionView(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
