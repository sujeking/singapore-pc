// ignore_for_file: import_of_legacy_library_into_null_safe

import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_read_item.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/devicem.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class DeviceApprovalAddPage extends StatefulWidget {
  DeviceApprovalAddPage({Key? key}) : super(key: key);

  @override
  _DeviceApprovalAddPageState createState() => _DeviceApprovalAddPageState();
}

class _DeviceApprovalAddPageState extends State<DeviceApprovalAddPage> {
  Map<String, dynamic> body = {};

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      body = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
      setState(() {});
    });
  }

  netPassApproval() async {
    var id = body["newData"]["_id"];
    if (null == id) {
      return;
    }
    var res = await deviceAppPass(id);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show("Successful Operation", context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      Future.delayed(Duration(seconds: 2), () {
        Navigator.of(context).pop();
      });
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  netRejectApproval() async {
    var id = body["_id"];
    if (null == id) {
      return;
    }
    var res = await deviceAppReject(id);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show("Successful Operation", context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      Future.delayed(Duration(seconds: 2), () {
        Navigator.of(context).pop();
      });
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  rowOne() {
    return Container(
      height: 100,
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
              title: "Request By",
              value: body["creatorName"] ?? "",
            ),
          ),
          Expanded(
            child: AppReadItem(
              title: "Request Date",
              value: body["creatorName"] ?? "",
            ),
          ),
          Expanded(
            child: Container(),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
      height: 100,
      margin: EdgeInsets.only(top: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
                title: "Device Id", value: body["deviceHardwareId"] ?? ""),
          ),
          Expanded(
            child: AppReadItem(title: "Device Name", value: body["name"] ?? ""),
          ),
          Expanded(
            child: AppReadItem(
                title: "Assets Register", value: body["assetRegister"] ?? ""),
          ),
        ],
      ),
    );
  }

  rowThree() {
    return Container(
      height: 100,
      margin: EdgeInsets.only(top: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
                title: "MDM Registration Date",
                value: body["mdmRegistrationDate"] ?? ""),
          ),
          Expanded(
            child: AppReadItem(
                title: "Device Registration Date",
                value: body["createdAt"] ?? ""),
          ),
          Expanded(
            child: AppReadItem(
                title: "Device De-Registration Date",
                value: body["updateAt"] ?? ""),
          ),
        ],
      ),
    );
  }

  ActionView() {
    return Container(
      child: Row(
        children: [
          AppBottomBtn(
              label: "Approval Add New Device",
              color: Colors.blue,
              onTap: () {
                netPassApproval();
              }),
          SizedBox(
            width: 12,
          ),
          AppBottomBtn(
              label: "Reject Add New Device",
              color: Colors.red,
              onTap: () {
                netRejectApproval();
              }),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TopBar(
                  backTitle: "Approval Device",
                  popAction: () {
                    Navigator.pop(context);
                  },
                ),
                PageTitleView(title: "Add Device Approval"),
                SizedBox(
                  height: 40,
                ),
                Column(
                  children: [
                    rowOne(),
                    rowTwo(),
                    rowThree(),
                  ],
                )
              ],
            ),
            Positioned(
              bottom: 80,
              child: ActionView(),
            )
          ],
        ),
      ),
    );
  }
}
