import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class AppItem extends StatefulWidget {
  Function(String name)? onTap;
  bool isSeleced;
  Map<String, dynamic>? data;
  AppItem({
    Key? key,
    this.data,
    this.onTap,
    required this.isSeleced,
  }) : super(key: key);

  @override
  _AppItemState createState() => _AppItemState();
}

class _AppItemState extends State<AppItem> {
  static Color bordeColor = Color(0xffe6e6e6);
  Map<String, dynamic> data = {"name": "---", "count": 0};
  @override
  void initState() {
    super.initState();
    data = widget.data ?? {"name": "---", "count": 0};
  }

  @override
  void didUpdateWidget(covariant AppItem oldWidget) {
    super.didUpdateWidget(oldWidget);
    data = widget.data ?? {"name": "---", "count": 0};
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          widget.onTap!(data["name"]);
        },
        child: Container(
            child: Row(
              children: [
                Container(
                  width: 78.0,
                  height: 78.0,
                  decoration: BoxDecoration(
                    color: const Color(0xff7c94b6),
                    image: DecorationImage(
                      image: AssetImage("assets/img/appicon.png"),
                      fit: BoxFit.fill,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(76.0)),
                    border: Border.all(
                      color: bordeColor,
                      width: 1.0,
                    ),
                  ),
                ),
                Expanded(
                  child: Column(children: [
                    Text(
                      data["name"]!,
                      style: TextStyle(fontFamily: "sli"),
                    ),
                    Expanded(
                        child: Center(
                      child: AutoSizeText("${data["count"]!}",
                          maxLines: 2,
                          style: TextStyle(
                            fontSize: 40,
                            fontFamily: "sbo",
                            fontWeight: FontWeight.bold,
                          )),
                    )),
                  ]),
                )
              ],
            ),
            decoration: BoxDecoration(
                color: widget.isSeleced ? Color(0xfff5f5f5) : Colors.white,
                border: Border.all(width: 1, color: bordeColor),
                borderRadius: BorderRadius.circular(8)),
            width: 220,
            height: 114,
            padding: EdgeInsets.fromLTRB(30, 20, 5, 20),
            margin: EdgeInsets.only(right: 12)));
  }
}
