import 'package:app/comps/app_table.dart';
import 'package:app/comps/app_table_cell.dart';
import 'package:app/comps/base_datatable.dart';
import 'package:app/net/rolem.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class RoleApprovalListPage extends StatefulWidget {
  String? kwd;
  RoleApprovalListPage({Key? key, this.kwd}) : super(key: key);

  @override
  _RoleApprovalListPageState createState() => _RoleApprovalListPageState();
}

class _RoleApprovalListPageState extends State<RoleApprovalListPage> {
  int _page = 1;
  int _pageSize = 30;
  var datasource = [];
  late var pageInfo;
  String kwd = "";
  List<String> cols = [
    'ROLE NAME',
    'CREATED BY',
    'APPROVED BY',
    'START DATE',
    'REQUESTED BY',
    'REQUESTED DATE',
    'ACTION',
  ];
  List<String> keys = [
    "name",
    "creatorName",
    "approvalId",
    "startDate",
    "approvalId",
    "updatedAt",
    "operationType",
  ];
  bool resort = false;
  String sortKey = "";

  List<double> widths = [
    130.0,
    233.0,
    130.0,
    130.0,
    160.0,
    160.0,
    160.0,
    160.0,
    160.0,
    222.0,
  ];

  List<Widget> rows = [];
  bool isloading = true;
  @override
  void initState() {
    super.initState();
    sortKey = keys.first;
    netRoleAppList(null);
  }

  @override
  void didUpdateWidget(RoleApprovalListPage oldWidget) {
    super.didUpdateWidget(oldWidget);
    kwd = widget.kwd ?? "";
    rows.clear();
    datasource.clear();
    _page = 1;
    netRoleAppList(null);
  }

  netRoleAppList(RefreshController? c) async {
    var fi = {
      "_page": _page,
      "_pageSize": _pageSize,
      "_keyword": kwd,
      "_orderby": resort ? "${sortKey} desc" : "${sortKey} asc",
    };
    var res = await roleAppList(fi);
    pageInfo = res["pageInfo"];
    var list = res["list"];
    datasource.addAll(list);
    print(datasource);
    updateUI();
    isloading = false;
    setState(() {});
    c!.refreshCompleted();
    c.loadComplete();
  }

  rowSelectHandle(dict) {
    var type = dict["operationType"];
    if (type == "create") {
      Navigator.pushNamed(context, "/appadd", arguments: dict["newData"]);
    } else {
      Navigator.pushNamed(context, "/appedit", arguments: dict);
    }
  }

  updateUI() {
    rows.clear();
    for (var i = 0; i < datasource.length; i++) {
      var dict = datasource[i]["approval"];
      var newData = dict["newData"];
      var row = InkWell(
        onTap: () {
          rowSelectHandle(dict);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(children: [
              AppTableCell(width: widths[0], text: newData["name"] ?? ""),
              AppTableCell(
                  width: widths[1], text: newData["creatorName"] ?? ""),
              AppTableCell(width: widths[2], text: newData["approvalId"] ?? ""),
              AppTableCell(width: widths[3], text: newData["startDate"] ?? ""),
              AppTableCell(width: widths[4], text: newData["approvalId"] ?? ""),
              AppTableCell(width: widths[5], text: newData["updatedAt"] ?? ""),
              AppTableCell(width: widths[6], text: dict["operationType"] ?? ""),
            ]),
            Container(
              height: 0.5,
              width: widths.reduce((a, b) => a + b),
              color: Color(0xffe0e0e0),
            )
          ],
        ),
      );
      rows.add(row);
    }
  }

  _cleanData() {
    _page = 1;
    rows.clear();
    datasource.clear();
  }

  _header() {
    List<AppTable3Header> items = [];
    for (var i = 0; i < cols.length; i++) {
      AppTable3Header ii = AppTable3Header(
        sortkey: keys[i],
        isup: !resort,
        text: cols[i],
        width: widths[i],
        onTap: (key) {
          isloading = true;
          setState(() {});
          sortKey = key;
          _page = 1;
          resort = !resort;
          datasource.clear();
          netRoleAppList(null);
        },
      );
      items.add(ii);
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: items),
        Container(
          height: 0.5,
          width: widths.reduce((a, b) => a + b),
          color: Color(0xffe0e0e0),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppTable3(
      isLoading: isloading,
      onRefresh: (RefreshController c) {
        isloading = true;
        setState(() {});
        _cleanData();
        netRoleAppList(c);
      },
      onLoadMore: (RefreshController c) {
        isloading = true;
        setState(() {});
        _page += 1;
        netRoleAppList(c);
      },
      children: [_header(), ...rows],
    );
  }
}
