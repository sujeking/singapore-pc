import 'package:app/comps/app_table.dart';
import 'package:app/comps/app_table_cell.dart';
import 'package:app/comps/base_datatable.dart';
import 'package:app/net/userm.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class UserListPage extends StatefulWidget {
  String? clientId;
  UserListPage({
    Key? key,
    this.clientId,
  }) : super(key: key);

  @override
  _UserListPageState createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  List<String> cols = [
    'USER ACCOUNT NAME',
    'EMAIL ADDRESS',
    'OTP PHONE',
    'ROLE',
    'BRANCH',
    'CREATED  BY',
    'APPROVED BY',
    'START DATE',
    'END DATE',
    'APPROVAL STATUS',
  ];
  List<String> keys = [
    "name",
    "email",
    "phone",
    "role",
    'branch',
    'createdBy',
    'approvedBy',
    'startDate',
    'endDate',
    'approvalStatus',
  ];

  bool resort = false;
  String sortKey = "";

  List datasource = [];
  int _page = 1;
  int _pageSize = 30;
  String kwd = "";
  List<Widget> rows = [];
  var pageInfo = {};

  List<double> widths = [
    190,
    130,
    130,
    130,
    130,
    130,
    130,
    130,
    130,
    130,
  ];

  bool isloading = true;

  @override
  void initState() {
    super.initState();
    sortKey = keys.first;
    netUserList(null);
  }

  @override
  void didUpdateWidget(UserListPage oldWidget) {
    super.didUpdateWidget(oldWidget);
    sortKey = keys.first;
    netUserList(null);
  }

  netUserList(RefreshController? c) async {
    var fi = {
      "_page": _page,
      "_pageSize": _pageSize,
      "_keyword": kwd,
      "clientId": widget.clientId,
      "_orderby": resort ? "${sortKey} desc" : "${sortKey} asc",
    };
    var res = await userList(fi);
    pageInfo = res["pageInfo"];
    var list = res["list"];
    datasource.addAll(list);
    print(datasource);
    updateUI();
    isloading = false;
    setState(() {});
    c!.refreshCompleted();
    c.loadComplete();
  }

  viewUserHandle(String uid) {
    Navigator.pushNamed(context, "/read", arguments: {"uid": uid});
  }

  updateUI() {
    rows.clear();
    for (var i = 0; i < datasource.length; i++) {
      var dict = datasource[i];
      var row = InkWell(
        onTap: () {
          viewUserHandle(dict["_id"]);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(children: [
              AppTableCell(
                  width: widths[0],
                  child: Row(
                    children: [
                      CircleAvatar(
                        radius: 16,
                        // backgroundImage: AssetImage("assets/img/user_avatar.jpeg"),
                        backgroundImage: NetworkImage(dict["image"] ?? ""),
                        backgroundColor: Color(0xfff5f5f5),
                      ),
                      SizedBox(width: 10),
                      Text(
                        dict["name"] ?? "",
                        style: TextStyle(
                            fontSize: 12,
                            fontFamily: "sli",
                            color: Color(0xff080808)),
                      )
                    ],
                  )),
              AppTableCell(width: widths[1], text: dict["email"] ?? ""),
              AppTableCell(width: widths[2], text: dict["phone"] ?? ""),
              AppTableCell(width: widths[3], text: dict["role"] ?? ""),
              AppTableCell(width: widths[4], text: "BAZLA"),
              AppTableCell(width: widths[5], text: "AMIR"),
              AppTableCell(width: widths[6], text: "AMIR"),
              AppTableCell(width: widths[7], text: "12-10-2019"),
              AppTableCell(width: widths[8], text: "03-01-2021"),
              AppTableCell(
                  width: widths[9],
                  child: Row(
                    children: [
                      ClipOval(
                        child: Container(
                          width: 8,
                          height: 8,
                          color: Color(0xff68d86c),
                        ),
                      ),
                      SizedBox(width: 10),
                      Text(
                        "Active",
                        style: TextStyle(
                            fontSize: 12,
                            fontFamily: "sli",
                            color: Color(0xff080808)),
                      )
                    ],
                  )),
            ]),
            Container(
              height: 0.5,
              width: widths.reduce((a, b) => a + b),
              color: Color(0xffe0e0e0),
            )
          ],
        ),
      );
      rows.add(row);
    }
  }

  _cleanData() {
    // _page = 1;
    // rows.clear();
    // datasource.clear();
  }

  _header() {
    List<AppTable3Header> items = [];
    for (var i = 0; i < cols.length; i++) {
      AppTable3Header ii = AppTable3Header(
        sortkey: keys[i],
        text: cols[i],
        isup: !resort,
        width: widths[i],
        onTap: (key) {
          isloading = true;
          setState(() {});
          sortKey = key;
          _page = 1;
          resort = !resort;
          datasource.clear();
          netUserList(null);
        },
      );
      items.add(ii);
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: items),
        Container(
          height: 0.5,
          width: widths.reduce((a, b) => a + b),
          color: Color(0xffe0e0e0),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppTable3(
      isLoading: isloading,
      onRefresh: (RefreshController c) {
        isloading = true;
        setState(() {});
        _cleanData();
        _page = 1;
        netUserList(c);
      },
      onLoadMore: (RefreshController c) {
        isloading = true;
        setState(() {});
        _page += 1;
        netUserList(c);
      },
      children: [_header(), ...rows],
    );
  }
}
