import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_pick_field.dart';
import 'package:app/comps/app_text_field.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/devicem.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class DeviceAddPage extends StatefulWidget {
  DeviceAddPage({Key? key}) : super(key: key);

  @override
  _DeviceAddPageState createState() => _DeviceAddPageState();
}

class _DeviceAddPageState extends State<DeviceAddPage> {
  var body = {
    "deviceHardwareId": "",
    "name": "",
    "assetRegister": "",
    "mdmRegistrationDate": "",
    "startDate": "",
    "endDate": ""
  };
  @override
  void initState() {
    super.initState();
  }

  netAddDevice() async {
    var res = await deviceAdd(body);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show(
        "Successful Operation",
        context,
        gravity: Toast.CENTER,
        duration: Toast.LENGTH_LONG,
      );
      Future.delayed(
        Duration(seconds: 2),
        () {
          Navigator.of(context).pop();
        },
      );
    } else {
      Toast.show(res["message"], context, gravity: Toast.CENTER);
    }
  }

  rowOne() {
    return Container(
      height: 100,
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Row(
        children: [
          Expanded(
            child: Container(
              child: AppTextField(
                titleStr: "Device ID",
                must: true,
                value: body["deviceHardwareId"],
                placeholder: "Enter Device ID",
                onChanged: (v) {
                  body["deviceHardwareId"] = v;
                },
              ),
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: Container(
              child: AppTextField(
                must: true,
                titleStr: "Device Name",
                value: body["name"],
                placeholder: "Enter Device Name",
                onChanged: (v) {
                  body["name"] = v;
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
      height: 100,
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Row(
        children: [
          Expanded(
            child: Container(
              child: AppTextField(
                titleStr: "Assets Register",
                must: true,
                value: body["assetRegister"],
                placeholder: "Enter Assets Register",
                onChanged: (v) {
                  body["assetRegister"] = v;
                },
              ),
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: Container(
              child: AppPickField(
                  titleStr: "MDM Regestion Date",
                  must: true,
                  value: body["mdmRegistrationDate"],
                  placeholder: "Enter MDM Regestion Date",
                  onTap: () {
                    showCalenderHandle(context, (e) {
                      body["mdmRegistrationDate"] = e;
                      setState(() {});
                    });
                  }),
            ),
          ),
        ],
      ),
    );
  }

  rowThree() {
    return Container(
      height: 100,
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Row(
        children: [
          Expanded(
            child: Container(
              child: AppPickField(
                must: true,
                value: body["startDate"],
                titleStr: "Device Registion Date",
                placeholder: "Select Device Registration Date",
                onTap: () {
                  showCalenderHandle(context, (e) {
                    body["startDate"] = e;
                    setState(() {});
                  });
                },
              ),
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: Container(
              child: AppPickField(
                value: body["endDate"],
                onTap: () {
                  showCalenderHandle(context, (e) {
                    body["endDate"] = e;
                    setState(() {});
                  });
                },
                titleStr: "Device De-Registion Date",
                placeholder: "Select Device Registration Date",
              ),
            ),
          ),
        ],
      ),
    );
  }

  backHandle() {
    Navigator.pop(context);
  }

  ActionView() {
    return Container(
      child: Row(
        children: [
          AppBottomBtn(
            label: 'Submit For Approval',
            color: Colors.blue,
            onTap: () {
              netAddDevice();
            },
          ),
          SizedBox(width: 12),
          AppBottomBtn(
            label: 'Cancel',
            color: Color(0xffff0000),
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  //

  late OverlayEntry entry;
  void showDatePickHandle() {
    entry = OverlayEntry(builder: (BuildContext context) {
      var size = MediaQuery.of(context).size;
      return Container(
        color: Colors.black.withOpacity(0.4),
        child: Center(
          child: Container(
              color: Colors.white,
              constraints: BoxConstraints(
                maxHeight: size.height / 3 * 2,
                maxWidth: size.width / 3 * 2,
              ),
              child: SfDateRangePicker(
                onSelectionChanged: (instance) async {
                  entry.remove();
                  print(instance.value);
                },
              )),
        ),
      );
    });
//往Overlay中插入插入OverlayEntry
    Overlay.of(context)?.insert(entry);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: Column(
          children: [
            TopBar(
              backTitle: "Add Device",
              popAction: () {
                Navigator.pop(context);
              },
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.fromLTRB(40, 12, 40, 80),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    PageTitleView(
                      title: "Add New Device",
                    ),
                    SizedBox(height: 40),
                    Expanded(
                      child: ListView(
                        children: [
                          rowOne(),
                          rowTwo(),
                          rowThree(),
                        ],
                      ),
                    ),
                    ActionView(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
