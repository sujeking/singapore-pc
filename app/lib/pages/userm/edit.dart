// ignore_for_file: unused_field, import_of_legacy_library_into_null_safe

import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_event_bus.dart';
import 'package:app/comps/app_normal_form.dart';
import 'package:app/comps/app_pick_field.dart';
import 'package:app/comps/app_select_field.dart';
import 'package:app/comps/app_text_field.dart';
import 'package:app/net/branchm.dart';
import 'package:app/net/devicem.dart';
import 'package:app/net/ipm.dart';
import 'package:app/net/rolem.dart';
import 'package:app/net/userm.dart';
import 'package:app/pages/userm/tenant_pop.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:date_format/date_format.dart';
import 'package:event_bus/event_bus.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

part 'edit_part_device.dart';
part 'edit_part_ip.dart';
part 'edit_part_role.dart';
part 'edit_part_branch.dart';

EventBus bus = EventBus();

const HOSTS = "https://ssoapi.fornewtech.com";

enum AvaterChange {
  NoChange,
  ChangeSuccess,
  ChangeFailed,
}

class TenantModel {
  String title;
  String? id;
  List? items;
  TenantModel({
    required this.title,
    this.id,
    this.items,
  });
  toJSON() {
    return {
      "title": this.title,
      "id": this.id,
      "items": this.items,
    };
  }
}

class UserEditPage extends StatefulWidget {
  UserEditPage({Key? key}) : super(key: key);

  @override
  _UserEditPageState createState() => _UserEditPageState();
}

class _UserEditPageState extends State<UserEditPage>
    with SingleTickerProviderStateMixin {
  List<Widget> deviceInputs = [];
  List<Widget> ipInputs = [];
  List<Widget> roleInputs = [];
  List<Widget> tenatInputs = [];

  List<TenantModel> tenantsModels = [];
  List<Widget> bodys = [];

  Uint8List avatars = Uint8List(0);
  String selectedFilepath = "";

  Map<String, dynamic> info = {};
  late OverlayEntry _entry;
  int tab_index = 0;
  late PageController _pageController;

  late PlatformFile avatarFile;

  bool loading = false;
  var avatarChangeStatus = AvaterChange.NoChange;

  ///
  ///xxxx
  ///data
  ///
  List deviceList = [];
  List ipList = [];
  List tenantList = [];

  @override
  void initState() {
    super.initState();
    netDeviceList();
    netIPList();
    _pageController = PageController(initialPage: tab_index, keepPage: true);

    Future.delayed(Duration.zero, () {
      info = ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
      deviceList = info["deviceList"] ?? [];
      ipList = info["ipList"] ?? [];
      tenantList = info["tenantList"] ?? [];

      for (var dict in deviceList) {
        _addDevice(dict);
      }
      for (var dict in ipList) {
        _addIP(dict);
      }

      for (var dict in tenantList) {
        TenantModel model = TenantModel(
          title: dict["tenantName"],
          id: dict["tenantId"],
        );
        tenantsModels.add(model);
        List roleList = dict["roleList"] ?? [];
        for (var data in roleList) {
          NormalTenantBody body = NormalTenantBody(
            bus: bus,
            appName: model.title,
            appId: model.id.toString(),
            delApp: _delTenant,
            data: data,
            callBack: (apps) {
              model.items = apps;
              info["tenantList"] = apps;
            },
          );
          bodys.add(body);
        }
      }
      tab_index = 0;
      setState(() {});
    });
  }

  netAddUser() async {
    var res = await userAdd(info);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show(
        "Successful Operation",
        context,
        gravity: Toast.CENTER,
        duration: Toast.LENGTH_LONG,
      );
      Future.delayed(
        Duration(seconds: 2),
        () {
          Navigator.of(context).pop();
        },
      );
    } else {
      Toast.show(
        res["message"],
        context,
        gravity: Toast.CENTER,
        duration: Toast.LENGTH_LONG,
      );
    }
    loading = false;
    setState(() {});
  }

  netEditUser() async {
    var res = await userUpdate(info);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show(
        "Successful Operation",
        context,
        gravity: Toast.CENTER,
        duration: Toast.LENGTH_LONG,
      );
      Future.delayed(
        Duration(seconds: 2),
        () {
          Navigator.of(context).pop();
        },
      );
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
    loading = false;
    setState(() {});
  }

  selectImgHandle() async {
    try {
      FilePickerResult? result = await FilePicker.platform
          .pickFiles(type: FileType.image, withData: true);
      PlatformFile file = await result!.files.first;
      avatarFile = file;
      avatars = file.bytes!;
      setState(() {});
      netUploadAvatar();
    } catch (e) {
      Toast.show(
        e.toString(),
        context,
        gravity: Toast.CENTER,
        duration: Toast.LENGTH_LONG,
      );
    }
  }

  netUploadAvatar() async {
    var res = await userUploadImg(info["authUserId"], avatarFile.path!);
    if (res["statusCode"] == 200) {
      info["image"] = HOSTS + res["data"];
      avatarChangeStatus = AvaterChange.ChangeSuccess;
    } else {
      avatarChangeStatus = AvaterChange.ChangeSuccess;
      Toast.show(
        "avatar upload failed,try again",
        context,
        gravity: Toast.CENTER,
        duration: Toast.LENGTH_LONG,
      );
    }
  }

  submitHandle() {
    ipList.clear();
    deviceList.clear();
    bus.fire(AppEventBus(key: "user_submit"));
    Future.delayed(Duration(milliseconds: 1000), () async {
      print("555555555555555555555555555555");
      print(deviceList);
      print("555555555555555555555555555555");
      info["ipList"] = ipList;
      info["deviceList"] = deviceList;
      return;
      //loading...
      loading = true;
      setState(() {});
      //
      if (info["_id"] == null) {
        netAddUser();
      } else {
        netEditUser();
      }
    });
  }

// profile
  _rowProfile() {
    _tipview(title, value) {
      return Container(
        constraints: BoxConstraints(
          minWidth: 118,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: TextStyle(
                fontSize: 13,
                fontFamily: "sno",
                color: Color(0xff3c3c43).withOpacity(0.6),
              ),
            ),
            Text(
              value,
              style: TextStyle(
                fontSize: 15,
                fontFamily: "sli",
                color: Color(0xff080808),
              ),
            )
          ],
        ),
      );
    }

    return Container(
      padding: EdgeInsets.only(bottom: 50),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Edit User",
            style: TextStyle(fontSize: 43, fontFamily: "sf"),
          ),
          SizedBox(
            height: 50,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                children: [
                  ClipOval(
                    child: avatars.length == 0
                        ? Image.asset(
                            "assets/img/user_avatar.jpeg",
                            width: 233,
                            height: 233,
                          )
                        : Image.memory(
                            avatars,
                            width: 233,
                            height: 233,
                          ),
                  ),
                  SizedBox(
                    height: 26,
                  ),
                  InkWell(
                    onTap: () {
                      selectImgHandle();
                    },
                    child: Text(
                      "Edit Photo",
                      style: TextStyle(
                        fontSize: 15,
                        fontFamily: "sli",
                        color: Color(0xff007aff),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                width: 80,
              ),
              Expanded(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.only(right: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          info["name"] ?? "",
                          maxLines: 2,
                          style: TextStyle(fontSize: 51, fontFamily: "sf"),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                _tipview(
                                    "OTP Phone Number", info["phone"] ?? ""),
                                SizedBox(
                                  width: 50,
                                ),
                                _tipview("Email Address", info["email"] ?? ""),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                _tipview("User Name", info["name"] ?? ""),
                                SizedBox(
                                  width: 50,
                                ),
                                _tipview("User SamAccountName Logon",
                                    info["description"] ?? ""),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ))
            ],
          ),
        ],
      ),
    );
  }

// device
  _addDevice(Map<String, dynamic> dict) {
    int idx = deviceList.indexOf(dict);

    var dic = {
      "name": dict["deviceName"],
      "sdate": dict["startDate"],
      "edate": dict["endDate"],
    };

    String keystr = "device" + deviceInputs.length.toString();
    deviceInputs.add(AppNormalForm(
      itemTitle: "Device Name",
      bus: bus,
      delBtnText: "Terminate Device Access",
      items: ndeviceList.map((e) => e["name"]).toList(),
      dict: dic,
      key: ValueKey(keystr),
      onDelete: (_) {
        _removeDevice(keystr);
      },
      onSave: (data) {
        int idx = data["idx"];
        var dic = ndeviceList[idx];
        data["deviceId"] = dic["_id"];
        deviceList.add(data);
      },
    ));
    setState(() {});
  }

  _removeDevice(String key) {
    deviceInputs
        .removeWhere((element) => (element.key as ValueKey).value == key);
    setState(() {});
  }

  _rowDevice() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 12),
            child: Text(
              "DEVICE ACCESS HISTORY",
              style: TextStyle(fontSize: 25, fontFamily: "sf"),
            ),
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: deviceInputs,
            ),
          ),
          // add
          Container(
            child: Row(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 12),
                  child: InkWell(
                    onTap: () {
                      _addDevice({});
                    },
                    child: Row(
                      children: [
                        Icon(
                          Icons.add,
                          color: Colors.blue,
                          size: 20,
                        ),
                        Text(
                          "Add Device Access",
                          style: TextStyle(fontSize: 15, color: Colors.blue),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

// ip

  _addIP(Map<String, dynamic> dict) {
    String keystr = "ip" + ipInputs.length.toString();
    var dic = {
      "name": dict["ipName"],
      "sdate": dict["startDate"],
      "edate": dict["endDate"],
    };
    ipInputs.add(AppNormalForm(
      itemTitle: "IP Name",
      bus: bus,
      delBtnText: "Terminate IP Access",
      items: nipList.map((e) => e["name"]).toList(),
      dict: dic,
      key: ValueKey(keystr),
      onDelete: (_) {
        _removeIP(keystr);
      },
      onSave: (data) {
        int idx = data["idx"];
        var dic = nipList[idx];
        data["ipId"] = dic["_id"];
        ipList.add(data);
      },
    ));
    setState(() {});
  }

  _removeIP(String key) {
    ipInputs.removeWhere((element) => (element.key as ValueKey).value == key);
    setState(() {});
  }

  _rowIP() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 12),
            child: Text(
              "IP ACCESS",
              style: TextStyle(fontSize: 25, fontFamily: "sf"),
            ),
          ),
          //add
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: ipInputs,
            ),
          ),
          Container(
            child: Row(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 12),
                  child: InkWell(
                    onTap: () {
                      _addIP({});
                    },
                    child: Row(
                      children: [
                        Icon(
                          Icons.add,
                          size: 20,
                          color: Colors.blue,
                        ),
                        Text(
                          "Add IP Access",
                          style: TextStyle(fontSize: 15, color: Colors.blue),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

// tenant

  showPopViewHandle() {
    _entry = OverlayEntry(builder: (context) {
      return Material(
        color: Colors.black.withOpacity(0.4),
        child: Container(
          child: Center(
            child: UserTenantPopPage(
              confirmHandle: (data) {
                _entry.remove();
                TenantModel model = TenantModel(
                  title: data["name"],
                );
                NormalTenantBody body = NormalTenantBody(
                  appName: model.title,
                  bus: bus,
                  appId: data["_id"].toString(),
                  delApp: _delTenant,
                  callBack: (apps) {
                    model.items = apps;
                    info["tenantList"] = apps;
                  },
                );
                bodys.add(body);
                tenantsModels.add(model);
                tab_index = tenantsModels.length - 1;
                setState(() {});
              },
              cancelHandle: () {
                _entry.remove();
              },
            ),
          ),
        ),
      );
    });
    Overlay.of(context)!.insert(_entry);
  }

  _delTenant() {
    bodys.removeAt(tab_index);
    tenantsModels.removeAt(tab_index);
    tab_index = tab_index - 1;
    if (tab_index <= 0) {
      tab_index = 0;
    }
    setState(() {});
  }

  _addTenant() {
    showPopViewHandle();
  }

// TabBarView

  _rowTenantTitleView() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      color: Colors.white,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(right: 12),
            child: Text(
              "TENANT ACCESS",
              style: TextStyle(fontSize: 25, fontFamily: "sf"),
            ),
          ),
          Container(
            child: InkWell(
              onTap: () {
                _addTenant();
              },
              child: Row(
                children: [
                  Icon(
                    Icons.add,
                    size: 20,
                    color: Colors.blue,
                  ),
                  Text(
                    "Add New Tenant(s)",
                    style: TextStyle(fontSize: 15, color: Colors.blue),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _rowTabview() {
    List<Widget> tags = [];
    for (var i = 0; i < tenantsModels.length; i++) {
      TenantModel model = tenantsModels[i];
      var tag = TabItemView(
        text: model.title,
        onTap: () {
          tab_index = i;
          setState(() {});
        },
        selected: i == tab_index,
      );
      tags.add(tag);
    }

    return Container(
      padding: EdgeInsets.only(top: 12),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Column(
          children: [
            Row(children: tags),
          ],
        ),
      ),
    );
  }

  actionView() {
    return Container(
      padding: EdgeInsets.only(top: 20, bottom: 20),
      child: Row(
        children: [
          AppBottomBtn(
              label: "Submit for Approval",
              color: Colors.blue,
              onTap: () {
                // szw
                submitHandle();
              }),
          SizedBox(width: 12),
          AppBottomBtn(
              label: "Cancel",
              color: Colors.blue,
              onTap: () {
                Navigator.of(context).pop();
              })
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TopBar(
                  backTitle: "Edit User",
                  popAction: () {
                    Navigator.pop(context);
                  },
                ),
                Expanded(
                  child: ListView(
                    controller: ScrollController(),
                    children: [
                      _rowProfile(),
                      _rowDevice(),
                      _rowIP(),
                      _rowTenantTitleView(),
                      _rowTabview(),
                      bodys.isNotEmpty
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  child: IndexedStack(
                                    index: tab_index,
                                    children: bodys,
                                  ),
                                ),
                              ],
                            )
                          : Container(),
                      // action
                      actionView()
                    ],
                  ),
                )
              ],
            ),
          ),
          Offstage(
            offstage: loading == false,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
        ],
      ),
    );
  }
}

/**
 * 
 */

class TabItemView extends StatelessWidget {
  Function() onTap;
  bool selected;
  String text;

  TabItemView(
      {Key? key,
      required this.onTap,
      required this.text,
      required this.selected})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 12),
      child: Column(
        children: [
          InkWell(
            onTap: onTap,
            child: Container(
                constraints: BoxConstraints(minWidth: 120),
                alignment: Alignment.center,
                child: Text(
                  text,
                  style: TextStyle(
                      fontSize: 20,
                      color: selected ? Color(0xff080808) : Color(0xff999999),
                      fontFamily: "sbo",
                      fontWeight:
                          selected ? FontWeight.bold : FontWeight.normal),
                )),
          ),
          Container(
            height: 2,
            width: 120,
            color: selected ? Color(0xff000000) : Colors.white,
            margin: EdgeInsets.only(top: 12),
          )
        ],
      ),
    );
  }
}

/**
 * 
 */

class ULTRONBody extends StatefulWidget {
  Function()? addRole;
  Function()? delRole;
  Function()? delApp;
  ULTRONBody({Key? key, this.delApp, this.addRole, this.delRole})
      : super(key: key);

  @override
  _ULTRONBodyState createState() => _ULTRONBodyState();
}

class _ULTRONBodyState extends State<ULTRONBody>
    with AutomaticKeepAliveClientMixin {
  List<Widget> rows = [];
  @override
  void initState() {
    super.initState();
    rows = [_buildRow()];
  }

  _buildRow() {
    return Row(
      // key: gkey,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: 200,
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: AppSelectField(
            items: [],
            must: true,
            placeholder: "Select Role",
            titleStr: "Role",
          ),
        ),
        Container(
          width: 200,
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: AppSelectField(
            items: [],
            must: true,
            placeholder: "Select Branch",
            titleStr: "Branch",
          ),
        ),
        Container(
          width: 200,
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: AppTextField(
            must: true,
            placeholder: "Enter Teller Number",
            titleStr: "Teller Number",
          ),
        ),
        Container(
          width: 200,
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: AppSelectField(
            items: [],
            must: true,
            placeholder: "Select Rank Description",
            titleStr: "Rank Description",
          ),
        ),
        Container(
          width: 240,
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: AppSelectField(
            items: [],
            must: false,
            placeholder: "Select Cash GL Description",
            titleStr: "Cash GL Description",
          ),
        ),
        Container(
          width: 240,
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          // child: AppSelectField(
          //   parentKey: gkey,
          //   must: true,
          //   placeholder: "Select Access Module",
          //   titleStr: "Access Module",
          // ),
          child: DropdownButton<dynamic>(
            menuMaxHeight: 160,
            alignment: Alignment.bottomLeft,
            items: List.generate(30, (index) {
              return DropdownMenuItem(
                value: index,
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 20,
                      height: 20,
                      color: Colors.orange,
                    ),
                    Text('First'),
                  ],
                ),
              );
            }).toList(),
            onChanged: (_) {},
          ),
        ),
        Container(
          width: 200,
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: AppPickField(
            must: true,
            placeholder: "Select Start Date",
            titleStr: "Start Date",
          ),
        ),
        Container(
          width: 200,
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: AppPickField(
            must: true,
            placeholder: "Select End Date",
            titleStr: "End Date",
          ),
        ),
      ],
    );
  }

  _addHandle() {
    rows.add(_buildRow());
    print(rows.length);
    setState(() {});
    widget.addRole!();
  }

  _delHandle() {
    rows.removeLast();
    setState(() {});
    if (rows.isEmpty) {
      widget.delApp!();
    } else {
      widget.delRole!();
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ...rows,
          Row(
            children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 12),
                child: InkWell(
                  onTap: () {
                    _addHandle();
                  },
                  child: Row(
                    children: [
                      Icon(
                        Icons.add,
                        size: 20,
                        color: Colors.blue,
                      ),
                      Text(
                        "Add Role",
                        style: TextStyle(fontSize: 15, color: Colors.blue),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: 50,
              ),
              InkWell(
                onTap: () {
                  _delHandle();
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.delete,
                      size: 20,
                      color: Colors.red,
                    ),
                    Text(
                      "Remove Role",
                      style: TextStyle(fontSize: 15, color: Colors.red),
                    ),
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

/**
 * Normal Tenant Body
 */

class NormalTenantBody extends StatefulWidget {
  Function()? addRole;
  Function()? delRole;
  Function()? delApp;
  Function(dynamic)? callBack;
  String appName;
  String appId;
  dynamic data;
  EventBus bus;
  NormalTenantBody({
    Key? key,
    this.addRole,
    this.callBack,
    required this.bus,
    required this.appName,
    required this.appId,
    this.delRole,
    this.delApp,
    this.data,
  }) : super(key: key);

  @override
  _NormalTenantBodyState createState() => _NormalTenantBodyState();
}

class _NormalTenantBodyState extends State<NormalTenantBody> {
  List<Widget> rows = [];
  dynamic data;
  List apps = [];
  late StreamSubscription ev;

  @override
  void initState() {
    super.initState();
    ev = widget.bus.on<AppEventBus>().listen((event) {
      if (widget.callBack != null) {
        widget.callBack!(apps);
      }
    });
    data = widget.data ?? {};
    apps = [data];
    rows.add(
      NormalTenantBodyCell(
        data: data,
        callBack: () {},
      ),
    );
    netRoleList(widget.appId);
    netBranchList();
  }

  @override
  void dispose() {
    ev.cancel();
    super.dispose();
  }

  @override
  void didUpdateWidget(NormalTenantBody oldWidget) {
    super.didUpdateWidget(oldWidget);
    initData();
  }

  initData() async {
    List<Widget> ls = [];
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    int num = sharedPreferences.getInt(widget.appName) ?? 1;
    for (var i = 0; i < num; i++) {
      ls.add(
        NormalTenantBodyCell(
          data: data,
          callBack: () {},
        ),
      );
    }
    rows = ls;
    setState(() {});
  }

  _addHandle() async {
    var dd = {};
    apps.add(dd);
    rows.add(
      NormalTenantBodyCell(
        data: dd,
        callBack: () {
          print(apps);
        },
      ),
    );
    setState(() {});
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setInt(widget.appName, rows.length);
    widget.addRole!();
  }

  _delHandle() async {
    rows.removeLast();
    apps.removeLast();
    setState(() {});
    SharedPreferences sp = await SharedPreferences.getInstance();
    if (rows.isEmpty) {
      await sp.remove(widget.appName);
      widget.delApp!();
    } else {
      sp.setInt(widget.appName, rows.length);
      widget.delRole!();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ...rows,
          Row(
            children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 12),
                child: InkWell(
                  onTap: () {
                    _addHandle();
                  },
                  child: Row(
                    children: [
                      Icon(
                        Icons.add,
                        size: 20,
                        color: Colors.blue,
                      ),
                      Text(
                        "Add Role",
                        style: TextStyle(fontSize: 15, color: Colors.blue),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: 50,
              ),
              InkWell(
                onTap: () {
                  _delHandle();
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.delete,
                      size: 20,
                      color: Colors.red,
                    ),
                    Text(
                      "Remove Role",
                      style: TextStyle(fontSize: 15, color: Colors.red),
                    ),
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}

class NormalTenantBodyCell extends StatefulWidget {
  dynamic data;
  Function() callBack;
  NormalTenantBodyCell({Key? key, required this.callBack, this.data})
      : super(key: key);

  @override
  _NormalTenantBodyCellState createState() => _NormalTenantBodyCellState();
}

class _NormalTenantBodyCellState extends State<NormalTenantBodyCell> {
  dynamic data;
  @override
  void initState() {
    super.initState();
    data = widget.data ?? {};
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 200,
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: AppSelectField(
              items: nroleList.map((e) => e["name"]).toList(),
              onChanged: (idx) {
                if (idx == -1) {
                  data["roleName"] = "";
                } else {
                  var dict = nroleList[idx];
                  String name = dict["name"];
                  data["roleName"] = name;
                  data["roleId"] = dict["_id"];
                }
                setState(() {});
                widget.callBack();
              },
              must: true,
              placeholder: "Select Role",
              titleStr: "Role",
              value: data["roleName"] ?? "",
            ),
          ),
          Container(
            width: 200,
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: AppSelectField(
              items: nbranchList.map((e) => e["name"]).toList(),
              must: true,
              placeholder: "Select Branch",
              titleStr: "Branch",
              value: data["branchName"] ?? "",
              onChanged: (idx) {
                if (idx == -1) {
                  data["branchName"] = "";
                } else {
                  var dict = nbranchList[idx];
                  String name = dict["name"];
                  data["branchName"] = name;
                  data["branchId"] = dict["_id"];
                }
                setState(() {});
                widget.callBack();
              },
            ),
          ),
          Container(
            width: 200,
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: AppPickField(
              must: true,
              placeholder: "Select Start Date",
              titleStr: "Start Date",
              value: data["startDate"] ?? "",
              onTap: () {
                showCalenderHandle(context, (e) {
                  data["startDate"] = e;
                  setState(() {});
                  widget.callBack();
                });
              },
            ),
          ),
          Container(
            width: 200,
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: AppPickField(
              must: true,
              placeholder: "Select End Date",
              titleStr: "End Date",
              value: data["endDate"] ?? "",
              onTap: () {
                showCalenderHandle(context, (e) {
                  data["endDate"] = e;
                  setState(() {});
                  widget.callBack();
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
