import 'dart:convert';
import 'dart:io';

import 'package:app/pages/tenantm/add.dart';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';

import 'net.dart';

tenantAdd(Map<String, dynamic> data) async {
  var res = await $http("post", "/tenant", data: data);
  return res;
}

tenantDelete(String id) async {
  var res = await $http("delete", "/tenant/" + id);
  return res;
}

tenantUpdate(Map<String, dynamic> data) async {
  var res = await $http("put", "/tenant/" + data["_id"], data: data);
  return res;
}

tenantList(Map<String, dynamic> data) async {
  var res = await $http("get", "/tenant", data: data);
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

tenantDetail(String id) async {
  var res = await $http("get", "/tenant/${id}");
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

// approval

tenantAppAdd() async {}

tenantAppDelete() async {}

tenantAppUpdate() async {}

tenantAppList(Map<String, dynamic> data) async {
  var res = await $http("get", "/tenant/approval/index", data: data);
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

tenantAppPass(String id) async {
  var res = await $http("post", "/tenant/${id}/approval/pass");
  return res;
}

tenantAppReject(String id) async {
  var res = await $http("post", "/tenant/${id}/approval/reject");
  return res;
}

tenantActives() async {
  var res = await $http("get", "/tenant/session/infos");
  return res;
}
