import 'net.dart';

statLogin(String arg) async {
  var res = await $http("get", "/log/stat/login?period=${arg}");
  return res;
}

statException(String arg) async {
  var res = await $http("get", "/log/stat/exception?period=${arg}");
  return res;
}
