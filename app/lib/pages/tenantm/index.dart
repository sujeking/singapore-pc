import 'package:app/base/base_page_router.dart';
import 'package:app/pages/tenantm/add.dart';
import 'package:app/pages/tenantm/approval_add.dart';
import 'package:app/pages/tenantm/approval_edit.dart';
import 'package:app/pages/tenantm/delete.dart';
import 'package:app/pages/tenantm/edit.dart';
import 'package:app/pages/tenantm/list.dart';
import 'package:app/pages/tenantm/read.dart';
import 'package:flutter/material.dart';

class TenantIndex extends StatefulWidget {
  TenantIndex({Key? key}) : super(key: key);

  @override
  _TenantIndexState createState() => _TenantIndexState();
}

class _TenantIndexState extends State<TenantIndex> {
  Map<String, Widget> dict = {
    '/list': TenantListIndexPage(),
    '/read': TenantReadPage(),
    "/add": TenantAddPage(),
    "/edit": TenantEditPage(),
    "/del": TenantDelPage(),
    "/appadd": TenantApprovalAddPage(),
    "/appedit": TenantApprovalEditPage()
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BasePageRouter(
      initPath: '/list',
      dict: dict,
    ));
  }
}
