// ignore_for_file: must_be_immutable

import 'package:app/comps/base_expansion_tile.dart';
import 'package:flutter/material.dart';

class SideMenu extends StatefulWidget {
  Function onTap;
  SideMenu({Key? key, required this.onTap}) : super(key: key);

  @override
  _SideMenuState createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {
  late Function onTap;

  List titles = [];

  @override
  void initState() {
    super.initState();
    onTap = widget.onTap;
    initData();
  }

  initData() {
    var _textColor = Color(0xff333333);

    _Label(str) {
      return Text(
        str,
        style: TextStyle(color: _textColor),
      );
    }

    _ItemView(str, handle) {
      return InkWell(
          onTap: handle,
          child: Container(
              width: double.infinity,
              padding: EdgeInsets.fromLTRB(50, 10, 0, 10),
              color: Color(0xfff5f5f5),
              child: _Label(str)));
    }

    titles = [
      {
        "isOpen": false,
        "label": "Dashboard",
        "icon": Icons.person_outline,
      },
      {
        "isOpen": false,
        "label": "User Management",
        "icon": Icons.sticky_note_2,
      },
      {
        "isOpen": false,
        "label": "Device Management",
        "icon": Icons.person_outline,
      },
      {
        "isOpen": false,
        "label": "Role Management",
        "icon": Icons.list_alt_outlined,
      },
      {
        "isOpen": false,
        "label": "Tenant Management",
        "icon": Icons.person_outline,
      },
      {
        "isOpen": false,
        "label": "IP Management",
        "icon": Icons.sticky_note_2,
      },
      {
        "isOpen": false,
        "label": "Branch Management",
        "icon": Icons.sticky_note_2,
      },
      {
        "isOpen": false,
        "label": "Audit Log Management",
        "icon": Icons.sticky_note_2,
      },
    ];
  }

  clickHandle(idx) {
    onTap(idx);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 60, bottom: 22),
      color: Color(0xfff5f5f5),
      child: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: titles.length,
              itemBuilder: (BuildContext context, int index) {
                var dict = titles[index];
                return Container(
                  constraints: BoxConstraints(minHeight: 30),
                  child: InkWell(
                    onTap: () {
                      clickHandle(index);
                    },
                    child: Container(
                      color: Color(0xfff5f5f5),
                      child: Row(
                        children: [
                          SizedBox(
                            width: 12,
                          ),
                          Icon(
                            dict["icon"],
                            color: Color(0xff999999),
                            size: 20,
                          ),
                          SizedBox(width: 5),
                          Text("${dict["label"]}"),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
          Center(
            child: Text(
              "© 2021 BIBD AT-TAMWIL BERHAD",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 15, color: Color(0x080808).withOpacity(0.65)),
            ),
          )
        ],
      ),
    );
  }
}
