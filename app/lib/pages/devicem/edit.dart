import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_pick_field.dart';
import 'package:app/comps/app_text_field.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/devicem.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class DeviceEditPage extends StatefulWidget {
  DeviceEditPage({Key? key}) : super(key: key);

  @override
  _DeviceEditPageState createState() => _DeviceEditPageState();
}

class _DeviceEditPageState extends State<DeviceEditPage> {
  Map<String, dynamic> body = {
    "deviceHardwareId": "",
    "name": "",
    "assetRegister": "",
    "mdmRegistrationDate": "",
    "startDate": "",
    "endDate": ""
  };

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      body = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
      setState(() {});
    });
  }

  netUpdateDevice() async {
    var res = await deviceUpdate(body);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show(
        "Successful Operation",
        context,
        gravity: Toast.CENTER,
        duration: Toast.LENGTH_LONG,
      );
      Future.delayed(
        Duration(seconds: 2),
        () {
          Navigator.of(context).pop();
        },
      );
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  rowOne() {
    return Container(
      height: 100,
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Row(
        children: [
          Expanded(
            child: Container(
              child: AppTextField(
                titleStr: "Device ID",
                must: true,
                value: body["deviceHardwareId"],
                placeholder: "Enter Device ID",
                onChanged: (v) {
                  body["deviceHardwareId"] = v;
                },
              ),
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: Container(
              child: AppTextField(
                must: true,
                titleStr: "Device Name",
                value: body["name"],
                placeholder: "Enter Device Name",
                onChanged: (v) {
                  body["name"] = v;
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
      height: 100,
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Row(
        children: [
          Expanded(
            child: Container(
              child: AppTextField(
                titleStr: "Assets Register",
                must: true,
                value: body["assetRegister"],
                placeholder: "Enter Assets Register",
                onChanged: (v) {
                  body["assetRegister"] = v;
                },
              ),
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: Container(
              child: AppPickField(
                  titleStr: "MDM Regestion Date",
                  must: true,
                  value: body["mdmRegistrationDate"],
                  placeholder: "Enter MDM Regestion Date",
                  onTap: () {
                    showCalenderHandle(context, (e) {
                      body["mdmRegistrationDate"] = e;
                      setState(() {});
                    });
                  }),
            ),
          ),
        ],
      ),
    );
  }

  rowThree() {
    return Container(
      height: 100,
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Row(
        children: [
          Expanded(
            child: Container(
              child: AppPickField(
                must: true,
                value: body["startDate"],
                titleStr: "Device Registion Date",
                placeholder: "Select Device Registration Date",
                onTap: () {
                  showCalenderHandle(context, (e) {
                    body["startDate"] = e;
                    setState(() {});
                  });
                },
              ),
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: Container(
              child: AppPickField(
                value: body["endDate"],
                onTap: () {
                  showCalenderHandle(context, (e) {
                    body["endDate"] = e;
                    setState(() {});
                  });
                },
                titleStr: "Device De-Registion Date",
                placeholder: "Select Device Registration Date",
              ),
            ),
          ),
        ],
      ),
    );
  }

  ActionView() {
    return Container(
      child: Row(
        children: [
          AppBottomBtn(
            label: 'Submit For Approval',
            color: Colors.blue,
            onTap: () {
              netUpdateDevice();
            },
          ),
          SizedBox(width: 12),
          AppBottomBtn(
            label: 'Cancel',
            color: Color(0xffff0000),
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TopBar(
                  backTitle: "Device Management / View Device / Edit Device",
                  popAction: () {
                    Navigator.pop(context);
                  },
                ),
                PageTitleView(title: "Edit Device"),
                SizedBox(height: 40),
                Expanded(
                  child: ListView(
                    children: [
                      rowOne(),
                      rowTwo(),
                      rowThree(),
                    ],
                  ),
                )
              ],
            ),
            Positioned(
              bottom: 80,
              child: ActionView(),
            )
          ],
        ),
      ),
    );
  }
}
