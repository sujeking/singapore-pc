import 'package:app/comps/app_pick_field.dart';
import 'package:app/comps/app_text_field.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserAddPage extends StatefulWidget {
  UserAddPage({Key? key}) : super(key: key);

  @override
  _UserDetailPageState createState() => _UserDetailPageState();
}

class _UserDetailPageState extends State<UserAddPage>
    with SingleTickerProviderStateMixin {
  rowOne() {
    return Container(
      padding: EdgeInsets.all(12),
      child: Row(
        children: [
          Expanded(
              flex: 1,
              child: Column(
                children: [
                  Text(
                    "Add User",
                    style: TextStyle(fontSize: 43, fontFamily: "sf"),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  ClipOval(
                    child: Image.asset(
                      "assets/img/add_user_default.png",
                      width: 198,
                      height: 198,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 12),
                    alignment: Alignment.center,
                    child: Text(
                      "Add Photo",
                      style: TextStyle(fontSize: 15, color: Color(0xff007aff)),
                    ),
                  )
                ],
              )),
          Expanded(
              flex: 3,
              child: Container(
                padding: EdgeInsets.only(right: 20, top: 30),
                color: Colors.white,
                child: Column(
                  children: [
                    AppTextField(
                      titleStr: "Full Name",
                      placeholder: "Enter Full Name",
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: AppTextField(
                            titleStr: "User SamAccountName Logon",
                            placeholder: "at-tamwil",
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.fromLTRB(12, 30, 12, 0),
                          child: Row(
                            children: [
                              Text("\\"),
                              Text(
                                "\*",
                                style: TextStyle(color: Colors.red),
                              )
                            ],
                          ),
                        ),
                        Expanded(
                          child: AppTextField(
                            placeholder: "azkia",
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: AppTextField(
                            titleStr: "Phone Number",
                            placeholder: "Enter Phone Number",
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.fromLTRB(12, 30, 12, 0),
                        ),
                        Expanded(
                          child: AppTextField(
                            titleStr: "Email",
                            placeholder: "Enter Email Address",
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ))
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 12),
            child: Text(
              "DEVICE ACCESS",
              style: TextStyle(fontSize: 25, fontFamily: "sf"),
            ),
          ),
          Container(
            height: 1,
            margin: EdgeInsets.only(bottom: 12),
            color: Color(0xffebebeb),
          ),
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: AppTextField(
                      titleStr: "Device Name",
                      must: true,
                      placeholder: "Select Device")),
              SizedBox(width: 20),
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                        child: AppPickField(
                      onTap: () {
                        showCalenderHandle(context, (e) {
                          setState(() {});
                        });
                      },
                      titleStr: "Start Date",
                      must: true,
                      placeholder: "Select Start Date",
                    )),
                    SizedBox(width: 10),
                    Expanded(
                        child: AppPickField(
                      onTap: () {
                        showCalenderHandle(context, (e) {
                          setState(() {});
                        });
                      },
                      titleStr: "End Date",
                      placeholder: "Select End Date",
                    )),
                    SizedBox(width: 10),
                    Expanded(
                      child: InkWell(
                        child: Container(
                          padding: EdgeInsets.only(top: 25),
                          child: Text(
                            "Delete Device Access",
                            style: TextStyle(fontSize: 15, color: Colors.red),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
          // add
          Container(
            padding: EdgeInsets.symmetric(vertical: 12),
            child: InkWell(
              child: Row(
                children: [
                  Icon(
                    Icons.add_circle,
                    color: Colors.blue,
                    size: 20,
                  ),
                  SizedBox(
                    width: 12,
                  ),
                  Text(
                    "Add Device Access",
                    style: TextStyle(fontSize: 15, color: Colors.blue),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  rowThree() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 12),
            child: Text(
              "IP ACCESS",
              style: TextStyle(fontSize: 25, fontFamily: "sf"),
            ),
          ),
          Container(
            height: 1,
            margin: EdgeInsets.only(bottom: 12),
            color: Color(0xffebebeb),
          ),
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: AppTextField(
                      titleStr: "IP Name",
                      must: true,
                      placeholder: "Select IP")),
              SizedBox(width: 20),
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                        child: AppPickField(
                      onTap: () {
                        showCalenderHandle(context, (e) {
                          setState(() {});
                        });
                      },
                      titleStr: "Start Date",
                      must: true,
                      placeholder: "Select Start Date",
                    )),
                    SizedBox(width: 10),
                    Expanded(
                        child: AppPickField(
                      onTap: () {
                        showCalenderHandle(context, (e) {
                          setState(() {});
                        });
                      },
                      titleStr: "End Date",
                      placeholder: "Select End Date",
                    )),
                    SizedBox(width: 10),
                    Expanded(
                      child: InkWell(
                        child: Container(
                          padding: EdgeInsets.only(top: 25),
                          child: Text(
                            "Delete IP Access",
                            style: TextStyle(fontSize: 15, color: Colors.red),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
          // add
          Container(
            padding: EdgeInsets.symmetric(vertical: 12),
            child: InkWell(
              child: Row(
                children: [
                  Icon(
                    Icons.add_circle,
                    color: Colors.blue,
                    size: 20,
                  ),
                  SizedBox(
                    width: 12,
                  ),
                  Text(
                    "Add IP",
                    style: TextStyle(fontSize: 15, color: Colors.blue),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  int tab_index = 0;
  late TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(initialIndex: 0, vsync: this, length: 2);
  }

  tabView() {
    return Container(
      padding: EdgeInsets.only(top: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Column(
                children: [
                  InkWell(
                    onTap: () {
                      tab_index = 0;
                      tabController.animateTo(tab_index);
                      setState(() {});
                    },
                    child: Container(
                        constraints: BoxConstraints(minWidth: 120),
                        alignment: Alignment.center,
                        child: Text(
                          "ULTRON +",
                          style: TextStyle(
                              fontSize: 20,
                              color: tab_index == 0
                                  ? Color(0xff080808)
                                  : Color(0xff999999),
                              fontFamily: "sf",
                              fontWeight: tab_index == 0
                                  ? FontWeight.bold
                                  : FontWeight.normal),
                        )),
                  ),
                  Container(
                    height: 2,
                    width: 120,
                    color: tab_index == 0 ? Color(0xff000000) : Colors.white,
                    margin: EdgeInsets.only(top: 12),
                  )
                ],
              ),
              SizedBox(
                width: 12,
              ),
              Column(
                children: [
                  InkWell(
                    onTap: () {
                      tab_index = 1;
                      tabController.animateTo(tab_index);
                      setState(() {});
                    },
                    child: Container(
                        constraints: BoxConstraints(minWidth: 120),
                        alignment: Alignment.center,
                        child: Text(
                          "EXPONENTIAL PRO",
                          style: TextStyle(
                              fontSize: 20,
                              color: tab_index == 1
                                  ? Color(0xff080808)
                                  : Color(0xff999999),
                              fontFamily: "sf",
                              fontWeight: tab_index == 1
                                  ? FontWeight.bold
                                  : FontWeight.normal),
                        )),
                  ),
                  Container(
                    height: 2,
                    width: 120,
                    color: tab_index == 1 ? Color(0xff000000) : Colors.white,
                    margin: EdgeInsets.only(top: 12),
                  )
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  rowFour() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 12),
            child: Row(
              children: [
                Text(
                  "TENANT ACCESS",
                  style: TextStyle(fontSize: 25, fontFamily: "sf"),
                ),
                SizedBox(
                  width: 20,
                ),
                InkWell(
                  child: Row(
                    children: [
                      Icon(
                        Icons.add_circle,
                        color: Colors.blue,
                        size: 20,
                      ),
                      SizedBox(
                        width: 12,
                      ),
                      Text(
                        "Add New Tenant(s) ",
                        style: TextStyle(fontSize: 15, color: Colors.blue),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 1,
            margin: EdgeInsets.only(bottom: 12),
            color: Color(0xffebebeb),
          ),
          tabView(),
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              Expanded(
                  child: AppTextField(
                      titleStr: "Role",
                      must: true,
                      placeholder: "Select Role")),
              SizedBox(width: 20),
              Expanded(
                  child: AppTextField(
                      titleStr: "Role",
                      must: true,
                      placeholder: "Select Role")),
              SizedBox(width: 20),
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                        child: AppPickField(
                      onTap: () {
                        showCalenderHandle(context, (e) {
                          setState(() {});
                        });
                      },
                      titleStr: "Start Date",
                      must: true,
                      placeholder: "Select Start Date",
                    )),
                    SizedBox(width: 10),
                    Expanded(
                        child: AppPickField(
                      onTap: () {
                        showCalenderHandle(context, (e) {
                          setState(() {});
                        });
                      },
                      titleStr: "End Date",
                      placeholder: "Select End Date",
                    )),
                  ],
                ),
              )
            ],
          ),
          // add
          Row(
            children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 12),
                child: InkWell(
                  child: Row(
                    children: [
                      Icon(
                        Icons.add_circle,
                        color: Colors.blue,
                        size: 20,
                      ),
                      SizedBox(
                        width: 12,
                      ),
                      Text(
                        "Add Role",
                        style: TextStyle(fontSize: 15, color: Colors.blue),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: 50,
              ),
              InkWell(
                child: Row(
                  children: [
                    Icon(
                      Icons.remove_circle,
                      color: Colors.red,
                      size: 20,
                    ),
                    SizedBox(
                      width: 12,
                    ),
                    Text(
                      "Remove Role",
                      style: TextStyle(fontSize: 15, color: Colors.red),
                    ),
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          TopBar(
            backTitle: "View IP",
            popAction: () {
              Navigator.pop(context);
            },
          ),
          Expanded(
            child: ListView(
              controller: ScrollController(),
              children: [
                rowOne(),
                rowTwo(),
                rowThree(),
                rowFour(),
                // action
                Container(
                  padding: EdgeInsets.only(left: 12, top: 12, bottom: 12),
                  child: Row(
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 50, vertical: 8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(width: 1, color: Colors.blue)),
                          child: Text(
                            "Submit for Approval",
                            style: TextStyle(color: Colors.blue),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
