import 'dart:ffi';

import 'package:flutter/material.dart';

class AppSearchView extends StatefulWidget {
  Function onTap;
  Function(String v)? onSubmit;
  String btnTitle;
  IconData? icon;
  bool? needTitle;
  AppSearchView(
      {Key? key,
      required this.onTap,
      this.needTitle,
      this.icon,
      required this.btnTitle,
      this.onSubmit})
      : super(key: key);

  @override
  _AppSearchViewState createState() => _AppSearchViewState();
}

class _AppSearchViewState extends State<AppSearchView> {
  late Function onTap;
  late String btnTitle;
  late Function(String v) onSubmit;
  late IconData? icon;
  bool needTitle = true;

  @override
  void initState() {
    super.initState();
    onTap = widget.onTap;
    btnTitle = widget.btnTitle;
    onSubmit = widget.onSubmit ?? (v) {};
    needTitle = widget.needTitle ?? true;
    icon = widget.icon ?? Icons.person_outline;
  }

  addClickHandle() {
    onTap();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 45),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Offstage(
                  offstage: needTitle == false,
                  child: Text("Search",
                      style: TextStyle(fontSize: 20, fontFamily: "sbo")),
                ),
                SizedBox(height: 12),
                Container(
                  width: 370,
                  child: TextField(
                    onSubmitted: (value) => onSubmit(value),
                    decoration: InputDecoration(
                        hintText: "Search",
                        hintStyle: TextStyle(fontSize: 14, fontFamily: "sre"),
                        fillColor: Color(0xff767680).withOpacity(0.12),
                        filled: true,
                        contentPadding: EdgeInsets.fromLTRB(30, 12, 6, 12),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide.none),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide.none),
                        isCollapsed: true),
                  ),
                )
              ],
            ),
            width: 310,
          ),
          Offstage(
            offstage: btnTitle.isEmpty,
            child: InkWell(
              onTap: () => addClickHandle(),
              child: Container(
                alignment: Alignment.center,
                constraints: BoxConstraints(minWidth: 115),
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 25),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(40),
                    border: Border.all(width: 1, color: Color(0xff007aff))),
                child: Row(
                  children: [
                    Icon(icon, color: Color(0xff007aff)),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      btnTitle,
                      style: TextStyle(
                        fontSize: 16,
                        fontFamily: "sli",
                        color: Color(0xff007aff),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
