import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_pick_field.dart';
import 'package:app/comps/app_text_field.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/ipm.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class IPEditPage extends StatefulWidget {
  IPEditPage({Key? key}) : super(key: key);

  @override
  _IPEditPageState createState() => _IPEditPageState();
}

class _IPEditPageState extends State<IPEditPage> {
  Map<String, dynamic> body = {
    "name": "",
    "ipAddress": "",
    "startDate": "",
    "endDate": ""
  };
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      body = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
      setState(() {});
    });
  }

  netUpdateIP() async {
    var res = await ipUpdate(body);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show(
        "Successful Operation",
        context,
        gravity: Toast.CENTER,
        duration: Toast.LENGTH_LONG,
      );
      Future.delayed(
        Duration(seconds: 2),
        () {
          Navigator.of(context).pop();
        },
      );
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  rowOne() {
    return Container(
      height: 100,
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Row(
        children: [
          Expanded(
            child: AppTextField(
              value: body["name"],
              titleStr: "IP Name",
              placeholder: "Enter IP Name",
              must: true,
              onChanged: (e) {
                body["name"] = e;
              },
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: AppTextField(
              value: body["ipAddress"],
              titleStr: "IP Address",
              placeholder: "Enter IP Address",
              must: true,
              onChanged: (e) {
                body["ipAddress"] = e;
              },
            ),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
      height: 100,
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Row(
        children: [
          Expanded(
            child: AppPickField(
              value: body["startDate"],
              titleStr: "IP Start Date",
              must: true,
              placeholder: "Select IP Start Date",
              onTap: () {
                showCalenderHandle(context, (e) {
                  body["startDate"] = e;
                  setState(() {});
                });
              },
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: AppPickField(
              value: body["endDate"],
              onTap: () {
                showCalenderHandle(context, (e) {
                  body["endDate"] = e;
                  setState(() {});
                });
              },
              titleStr: "IP End Date",
              placeholder: "Select IP End Date",
            ),
          ),
        ],
      ),
    );
  }

  ActionView() {
    return Container(
      child: Row(
        children: [
          AppBottomBtn(
            label: 'Submit For Approval',
            color: Colors.blue,
            onTap: () {
              netUpdateIP();
            },
          ),
          SizedBox(width: 12),
          AppBottomBtn(
            label: 'Cancel',
            color: Color(0xffff0000),
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.fromLTRB(40, 12, 40, 80),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TopBar(
              backTitle: "Edit IP",
              popAction: () {
                Navigator.pop(context);
              },
            ),
            PageTitleView(title: "Edit IP"),
            Expanded(
              child: Column(
                children: [
                  rowOne(),
                  rowTwo(),
                ],
              ),
            ),
            ActionView()
          ],
        ),
      ),
    );
  }
}
