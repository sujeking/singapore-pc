import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_pick_field.dart';
import 'package:app/comps/app_read_item.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/devicem.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class DeviceReadPage extends StatefulWidget {
  DeviceReadPage({Key? key}) : super(key: key);

  @override
  _DeviceReadPageState createState() => _DeviceReadPageState();
}

class _DeviceReadPageState extends State<DeviceReadPage> {
  Map<String, dynamic> info = {};
  late String idstr;
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      String id = ModalRoute.of(context)?.settings.arguments as String;
      idstr = id;
      netDeviceDetail(id);
    });
  }

  netDeviceDetail(String id) async {
    if (id.isEmpty) {
      return;
    }
    var res = await deviceDetail(id);
    info = res;

    setState(() {});
  }

  netDel(String id) async {
    var res = await deviceDelete(id);
    int statusCode = res["statusCode"];

    if (statusCode == 200) {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      Future.delayed(Duration(milliseconds: 2), () {
        Navigator.of(context).pop();
      });
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  rowOne() {
    return Container(
      height: 100,
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
                title: "Request By", value: info["creatorName"] ?? ""),
          ),
          Expanded(
            child: AppReadItem(
                title: "Approved By", value: info["approverId"] ?? ""),
          ),
          Expanded(
            child: Container(),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
      height: 100,
      margin: EdgeInsets.only(top: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
                title: "Device Id", value: info["deviceHardwareId"] ?? ""),
          ),
          Expanded(
            child: AppReadItem(title: "Device Name", value: info["name"] ?? ""),
          ),
          Expanded(
            child: AppReadItem(
                title: "Assets Register", value: info["assetRegister"] ?? ""),
          ),
        ],
      ),
    );
  }

  rowThree() {
    return Container(
      height: 100,
      margin: EdgeInsets.only(top: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
                title: "MDM Registration Date",
                value: info["mdmRegistrationDate"] ?? ""),
          ),
          Expanded(
            child: AppReadItem(
                title: "Device Registration Date",
                value: info["createdAt"] ?? ""),
          ),
          Expanded(
            child: AppReadItem(
                title: "Device De-Registration Date",
                value: info["updatedAt"] ?? ""),
          ),
        ],
      ),
    );
  }

  ActionView() {
    return Container(
      child: Row(
        children: info["statusCode"] == 1
            ? [
                AppBottomBtn(
                    label: "Back",
                    color: Colors.blue,
                    onTap: () {
                      Navigator.of(context).pop();
                    }),
              ]
            : [
                AppBottomBtn(
                    label: "Edit Device",
                    color: Colors.blue,
                    onTap: () {
                      Navigator.of(context).pushNamed("/edit", arguments: info);
                    }),
                SizedBox(width: 12),
                AppBottomBtn(
                    label: "Delete Device",
                    color: Colors.red,
                    onTap: () {
                      showDelAlertHandle(context, () {
                        var id = info["_id"];
                        netDel(id);
                      });
                    }),
              ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Stack(
          children: [
            Column(
              children: [
                TopBar(
                  backTitle: "Device Management / View Device",
                  popAction: () {
                    Navigator.pop(context);
                  },
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        PageTitleView(title: "View Device"),
                        rowOne(),
                        rowTwo(),
                        rowThree(),
                      ],
                    ),
                  ),
                )
              ],
            ),
            Positioned(
                bottom: 80,
                left: 0,
                child: Offstage(
                  offstage: info["statusCode"] == null,
                  child: ActionView(),
                ))
          ],
        ),
      ),
    );
  }
}
