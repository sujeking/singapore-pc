import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_read_item.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/branchm.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class BranchApprovalAddPage extends StatefulWidget {
  BranchApprovalAddPage({Key? key}) : super(key: key);

  @override
  _BranchApprovalAddPageState createState() => _BranchApprovalAddPageState();
}

class _BranchApprovalAddPageState extends State<BranchApprovalAddPage> {
  Map<String, dynamic> body = {};

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      body = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
      setState(() {});
    });
  }

  netPassApproval() async {
    var id = body["_id"];
    if (null == id) {
      return;
    }
    var res = await branchAppPass(id);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show("Successful Operation", context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      Future.delayed(Duration(seconds: 2), () {
        Navigator.of(context).pop();
      });
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  netRejectApproval() async {
    var id = body["_id"];
    if (null == id) {
      return;
    }
    var res = await branchAppReject(id);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show("Successful Operation", context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      Future.delayed(Duration(seconds: 2), () {
        Navigator.of(context).pop();
      });
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  rowOne() {
    return Container(
      height: 100,
      padding: EdgeInsets.only(left: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
                title: "Request By", value: body["creatorName"] ?? ""),
          ),
          Expanded(
            child: AppReadItem(
                title: "Requested Date", value: body["updatedAt"] ?? ""),
          ),
          Expanded(
            child: Container(),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
      height: 100,
      padding: EdgeInsets.only(left: 12),
      margin: EdgeInsets.only(top: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
              title: "Branch Name",
              value: body["name"],
            ),
          ),
          Expanded(
            child: AppReadItem(
              title: "Subsidiary",
              value: body["subsidiary"],
            ),
          ),
          Expanded(
            child: AppReadItem(
              title: "Branch Code",
              value: body["code"],
            ),
          ),
        ],
      ),
    );
  }

  rowThree() {
    return Container(
      height: 100,
      padding: EdgeInsets.only(left: 12),
      margin: EdgeInsets.only(top: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
              title: "Branch Location",
              value: body["location"],
            ),
          ),
          Expanded(
            child: AppReadItem(
              title: "Start Date",
              value: body["startDate"],
            ),
          ),
          Expanded(
            child: AppReadItem(
              title: "End Date",
              value: body["endDate"],
            ),
          ),
        ],
      ),
    );
  }

  ActionView() {
    return Container(
      child: Row(
        children: [
          AppBottomBtn(
              label: "Approval Add New Branch",
              color: Colors.blue,
              onTap: () {
                netPassApproval();
              }),
          SizedBox(
            width: 12,
          ),
          AppBottomBtn(
              label: "Reject Add New Branch",
              color: Colors.red,
              onTap: () {
                netRejectApproval();
              }),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.fromLTRB(40, 12, 40, 80),
        child: Stack(
          children: [
            Column(
              children: [
                TopBar(
                  backTitle: "View Branch",
                  popAction: () {
                    Navigator.pop(context);
                  },
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    PageTitleView(title: "View Branch"),
                    rowOne(),
                    rowTwo(),
                    rowThree(),
                  ],
                )
              ],
            ),
            Positioned(bottom: 12, left: 0, child: ActionView())
          ],
        ),
      ),
    );
  }
}
