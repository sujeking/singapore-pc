import 'package:app/comps/active_app_view.dart';
import 'package:app/comps/app_search_view.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/comps/tab_view.dart';
import 'package:app/net/tenantm.dart';
import 'package:app/pages/tenantm/approval_list.dart';
import 'package:app/pages/tenantm/tenant_list.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/material.dart';

class TenantListIndexPage extends StatefulWidget {
  TenantListIndexPage({Key? key}) : super(key: key);

  @override
  _TenantListIndexPageState createState() => _TenantListIndexPageState();
}

class _TenantListIndexPageState extends State<TenantListIndexPage>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  var apps = [];
  String kwd = "";

  @override
  void initState() {
    super.initState();
    netApps();
    tabController = TabController(length: 2, vsync: this);
  }

  netApps() async {
    var res = await tenantActives();
    apps = res["data"];
    setState(() {});
  }

// action
  addTenantHandle() {
    Navigator.pushNamed(context, "/add");
  }

  viewTenantHandle() {
    Navigator.pushNamed(context, "/read");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TopBar(
              backTitle: "Tenant Management",
            ),
            PageTitleView(title: "Tenant Management"),
            ActiveAppView(
              currentIdx: -1,
              apps: apps,
            ),
            AppSearchView(
                onSubmit: (e) {
                  kwd = e;
                  setState(() {});
                },
                btnTitle: "Add Tenant",
                onTap: () => addTenantHandle()),
            AppTabView(
              titles: ["TENANT LIST", "APPROVAL LIST"],
              tabController: tabController,
            ),
            Expanded(
              child: TabBarView(
                controller: tabController,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  TenantListPage(
                    kwd: kwd,
                  ),
                  TenantApprovalPage(
                    kwd: kwd,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
