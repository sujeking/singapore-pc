import 'package:app/comps/active_app_view.dart';
import 'package:app/comps/app_search_view.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/comps/tab_view.dart';
import 'package:app/net/tenantm.dart';
import 'package:app/pages/Userm/list.dart';
import 'package:app/pages/userm/pop.dart';
import 'package:app/pages/userm/user_approval_list.dart';
import 'package:app/pages/userm/user_list.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class UserListIndexPage extends StatefulWidget {
  UserListIndexPage({Key? key}) : super(key: key);

  @override
  _UserListIndexPageState createState() => _UserListIndexPageState();
}

class _UserListIndexPageState extends State<UserListIndexPage>
    with SingleTickerProviderStateMixin {
  int appIdx = 0;
  var table_index = 0;
  var apps = [];
  String clientId = "";
  GlobalKey<ScaffoldState> tabkey = GlobalKey<ScaffoldState>();

  late TabController tabController;
  late OverlayEntry _entry;

  @override
  void initState() {
    super.initState();
    netApps();
    tabController =
        TabController(initialIndex: table_index, length: 2, vsync: this);
  }

  netApps() async {
    var res = await tenantActives();
    apps = res["data"];
    clientId = apps[appIdx]["name"];
    setState(() {});
  }

  // action
  addUserHandle() {
    Navigator.pushNamed(context, "/add");
  }

  approvalUserHandle() {
    Navigator.pushNamed(context, "/editapproval");
  }

  showPopViewHandle() {
    _entry = OverlayEntry(builder: (context) {
      return Material(
        color: Colors.black.withOpacity(0.4),
        child: Container(
          child: Center(
            child: UserPopPage(
              confirmHandle: (info) {
                _entry.remove();
                Navigator.of(context)
                    .pushNamed("/read", arguments: {"info": info});
              },
              cancelHandle: () {
                _entry.remove();
              },
            ),
          ),
        ),
      );
    });
    Overlay.of(context)!.insert(_entry);
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: tabkey,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        color: Colors.white,
        child: Column(
          children: [
            TopBar(
              backTitle: "User Management",
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    PageTitleView(
                      title: "User Management",
                    ),
                    ActiveAppView(
                      currentIdx: appIdx,
                      apps: apps,
                    ),
                    AppSearchView(
                      onTap: showPopViewHandle,
                      btnTitle: "Add User",
                    ),
                    // szw
                    // Row(
                    //   children: [
                    //     InkWell(
                    //       onTap: () {
                    //         approvalUserHandle();
                    //       },
                    //       child: Container(
                    //         padding: EdgeInsets.all(12),
                    //         child: Text(
                    //           "test 查看 Edit User Approval UI",
                    //           style: TextStyle(color: Colors.blue),
                    //         ),
                    //       ),
                    //     ),
                    //   ],
                    // ),
                    AppTabView(
                        titles: ["USER LIST", "APPROVAL LIST"],
                        tabController: tabController),
                    Expanded(
                      child: TabBarView(
                        physics: NeverScrollableScrollPhysics(),
                        controller: tabController,
                        children: [
                          UserListPage(
                            clientId: clientId,
                          ),
                          UserApprovalListPage(),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
