import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';

class AppTextField extends StatefulWidget {
  String? placeholder;
  String? titleStr;
  bool? must;
  String? value;
  Function(String v)? onChanged;
  AppTextField(
      {Key? key,
      this.placeholder,
      this.titleStr,
      this.must,
      this.value,
      this.onChanged})
      : super(key: key);

  @override
  _AppTextFieldState createState() => _AppTextFieldState();
}

class _AppTextFieldState extends State<AppTextField> {
  late String placeholder;
  late String title;
  late bool must;
  late Function onChanged;
  late String value;
  late TextEditingController _editingController;
  @override
  void initState() {
    super.initState();
    placeholder = widget.placeholder ?? "";
    title = widget.titleStr ?? "";
    must = widget.must ?? false;
    onChanged = widget.onChanged ?? (e) {};
    value = widget.value ?? "";
    _editingController = TextEditingController(text: value);
  }

  @override
  void didUpdateWidget(covariant AppTextField oldWidget) {
    super.didUpdateWidget(oldWidget);
    placeholder = widget.placeholder ?? "";
    title = widget.titleStr ?? "";
    must = widget.must ?? false;
    onChanged = widget.onChanged ?? (e) {};
    value = widget.value ?? "";
    _editingController = TextEditingController(text: value);
  }

  var mustTip = Text("*", style: TextStyle(fontSize: 14, color: Colors.red));
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(title,
                style: TextStyle(
                    fontFamily: "sli", fontSize: 15, color: Color(0xff080808))),
            Offstage(
              offstage: !must,
              child: mustTip,
            )
          ],
        ),
        SizedBox(height: 12),
        TextField(
            controller: _editingController,
            onChanged: (e) {
              value = e;
              setState(() {});
              onChanged(e);
            },
            style: TextStyle(fontFamily: "sli", fontSize: 12),
            decoration: InputDecoration(
                hintText: placeholder,
                hintStyle: TextStyle(
                    fontFamily: "sli",
                    fontSize: 12,
                    color: Color(0xff080808).withOpacity(0.5)),
                isCollapsed: true,
                suffixIcon: value.isNotEmpty
                    ? InkWell(
                        onTap: () {
                          value = "";
                          _editingController.text = value;
                          setState(() {});
                          onChanged(value);
                        },
                        hoverColor: Colors.transparent,
                        child: Icon(
                          Icons.cancel,
                          color: Color(0xff8c8c8c),
                        ),
                      )
                    : null,
                contentPadding: EdgeInsets.fromLTRB(23, 15, 0, 15),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: BorderSide(width: 1, color: Color(0x80999999))),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide:
                        BorderSide(width: 1, color: Color(0x80999999))))),
      ],
    );
  }
}
