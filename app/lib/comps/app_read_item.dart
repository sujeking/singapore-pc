import 'package:flutter/material.dart';

class AppReadItem extends StatelessWidget {
  final String title;
  final String value;
  const AppReadItem({Key? key, required this.title, required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(title,
                style: TextStyle(
                  fontSize: 17,
                  fontFamily: "sno",
                  color: Color(0xff3c3c43).withOpacity(0.6),
                )),
            SizedBox(
              height: 14,
            ),
            Text(
              value.isEmpty ? "" : value,
              style: TextStyle(
                  fontFamily: "sli", fontSize: 25, color: Color(0xff080808)),
            )
          ]),
    );
  }
}

class AppReadItemSmall extends StatelessWidget {
  final String title;
  final String value;
  const AppReadItemSmall({Key? key, required this.title, required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(title,
                style: TextStyle(
                  fontSize: 17,
                  fontFamily: "sno",
                  color: Color(0xff3c3c43).withOpacity(0.6),
                )),
            SizedBox(
              height: 14,
            ),
            Text(
              value.isEmpty ? "" : value,
              style: TextStyle(
                  fontFamily: "sli", fontSize: 20, color: Color(0xff080808)),
            )
          ]),
    );
  }
}
