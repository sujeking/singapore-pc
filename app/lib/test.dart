// ignore_for_file: unused_field

import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_normal_form.dart';
import 'package:app/comps/app_pick_field.dart';
import 'package:app/comps/app_select_field.dart';
import 'package:app/comps/app_table.dart';
import 'package:app/comps/app_text_field.dart';
import 'package:app/pages/userm/tenant_pop.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/material.dart';
// import 'http://127.0.0.1:8090/Page.dart';

class Test extends StatefulWidget {
  Test({Key? key}) : super(key: key);

  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<Test> {
  bool isloading = true;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 10), () {
      isloading = false;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: AppTable3(
      isLoading: isloading,
      onRefresh: (c) {
        c.refreshCompleted();
      },
      children: [
        Container(width: 200, height: 80, color: Colors.pink[100]),
        Container(width: 200, height: 80, color: Colors.pink[200]),
        Container(width: 200, height: 80, color: Colors.pink[300]),
        Container(width: 200, height: 80, color: Colors.pink[400]),
      ],
    ));
  }
}
