import 'package:app/comps/active_app_view.dart';
import 'package:app/comps/app_search_view.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/comps/tab_view.dart';
import 'package:app/net/tenantm.dart';
import 'package:app/pages/branchm/branch_approval_list.dart';
import 'package:app/pages/branchm/branch_list.dart';
import 'package:app/pages/tenantm/approval_list.dart';
import 'package:app/pages/tenantm/tenant_list.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';

class BranchListIndexPage extends StatefulWidget {
  BranchListIndexPage({Key? key}) : super(key: key);

  @override
  _TenantListIndexPageState createState() => _TenantListIndexPageState();
}

class _TenantListIndexPageState extends State<BranchListIndexPage>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  String kwd = "";
  var apps = [];
  @override
  void initState() {
    super.initState();
    netApps();
    tabController = TabController(length: 2, vsync: this);
  }

  netApps() async {
    var res = await tenantActives();
    apps = res["data"];
    setState(() {});
  }

// action
  addBranchHandle() {
    Navigator.pushNamed(context, "/add");
  }

  viewDeviceHandle() {
    Navigator.pushNamed(context, "/read");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TopBar(
              backTitle: "Branch Management",
            ),
            PageTitleView(title: "Branch Management"),
            ActiveAppView(
              currentIdx: -1,
              apps: apps,
            ),
            AppSearchView(
                onSubmit: (e) {
                  kwd = e;
                  setState(() {});
                },
                btnTitle: "Add Branch",
                onTap: () => addBranchHandle()),
            AppTabView(
              titles: ["BRANCH LIST", "APPROVAL LIST"],
              tabController: tabController,
            ),
            Expanded(
              child: TabBarView(
                controller: tabController,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  BranchListPage(
                    kwd: kwd,
                  ),
                  BranchApprovalListPage(
                    kwd: kwd,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
