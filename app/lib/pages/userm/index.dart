import 'package:app/base/base_page_router.dart';
import 'package:app/pages/Userm/User_list.dart';
import 'package:app/pages/userm/add.dart';
import 'package:app/pages/userm/approval.dart';
import 'package:app/pages/userm/detail.dart';
import 'package:app/pages/userm/edit.dart';
import 'package:app/pages/userm/edit_approvaldart.dart';
import 'package:app/pages/userm/list.dart';
import 'package:flutter/material.dart';

class UserIndex extends StatefulWidget {
  UserIndex({Key? key}) : super(key: key);

  @override
  _UserIndexState createState() => _UserIndexState();
}

class _UserIndexState extends State<UserIndex> {
  Map<String, Widget> dict = {
    '/list': UserListIndexPage(),
    '/read': UserDetailPage(),
    "/edit": UserEditPage(),
    "/add": UserAddPage(),
    "/editapproval": UserEditApprovalPage(),
    "/approval": UserApprovalPage()
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BasePageRouter(
      initPath: "/list",
      dict: dict,
    ));
  }
}
