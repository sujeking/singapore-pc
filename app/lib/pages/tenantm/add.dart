import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_pick_field.dart';
import 'package:app/comps/app_text_field.dart';
import 'package:app/net/tenantm.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class TenantAddPage extends StatefulWidget {
  TenantAddPage({Key? key}) : super(key: key);

  @override
  _UserDetailPageState createState() => _UserDetailPageState();
}

class _UserDetailPageState extends State<TenantAddPage>
    with SingleTickerProviderStateMixin {
  var body = {
    "name": "",
    "description": "",
    "image": "",
    "startDate": "",
    "endDate": "",
    "addRoleUrl": "",
    "deleteRoleUrl": "",
    "webhookUrl": "",
    "redirectUris": [],
    "webOrigins": []
  };

  @override
  void initState() {
    super.initState();
  }

  netAddTenant() async {
    var res = await tenantAdd(body);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show(
        "Successful Operation",
        context,
        gravity: Toast.CENTER,
        duration: Toast.LENGTH_LONG,
      );
      Future.delayed(
        Duration(seconds: 2),
        () {
          Navigator.of(context).pop();
        },
      );
    } else {
      Toast.show(res["message"], context, gravity: Toast.CENTER);
    }
  }

  rowOne() {
    return Container(
      padding: EdgeInsets.all(12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Add New Tenant",
            style: TextStyle(fontSize: 43, fontFamily: "sf"),
          ),
          SizedBox(
            height: 30,
          ),
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Column(
                    children: [
                      ClipOval(
                        child: Image.asset(
                          "assets/img/add_user_default.png",
                          width: 198,
                          height: 198,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 12),
                        alignment: Alignment.center,
                        child: Text(
                          "Add Photo",
                          style:
                              TextStyle(fontSize: 15, color: Color(0xff007aff)),
                        ),
                      )
                    ],
                  )),
              SizedBox(
                width: 60,
              ),
              Expanded(
                  flex: 3,
                  child: Container(
                    padding: EdgeInsets.only(right: 30),
                    child: Column(
                      children: [
                        AppTextField(
                          titleStr: "Tenant Name",
                          must: true,
                          placeholder: "Enter Full Name",
                          onChanged: (v) => body["name"] = v,
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        AppTextField(
                          titleStr: "Tenant Description (optional)",
                          placeholder: "Enter Full Name",
                          onChanged: (v) => body["description"] = v,
                        ),
                      ],
                    ),
                  ))
            ],
          ),
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: AppPickField(
                    onTap: () {
                      showCalenderHandle(context, (e) {
                        body["startDate"] = e;
                        setState(() {});
                      });
                    },
                    value: body["startDate"] as String,
                    titleStr: "Tenant Start Date",
                    must: true,
                    placeholder: "Select Start Date",
                  )),
              SizedBox(width: 20),
              Expanded(
                child: AppPickField(
                  onTap: () {
                    showCalenderHandle(context, (e) {
                      body["endDate"] = e;
                      setState(() {});
                    });
                  },
                  value: body["endDate"] as String,
                  titleStr: "Tenant End Date (optional)",
                  placeholder: "Select End Date",
                ),
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: AppTextField(
                    titleStr: "Add Role URL (mandatory for ultron)",
                    placeholder: "Enter Userinfo Endpoint",
                    onChanged: (v) => body["addRoleUrl"] = v,
                  )),
              SizedBox(width: 20),
              Expanded(
                child: AppTextField(
                  titleStr: "Delete Role URL (mandatory for ulron)",
                  placeholder: "Enter Client Secret",
                  onChanged: (v) => body["deleteRoleUrl"] = v,
                ),
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: AppTextField(
                    titleStr: "Webhook URL",
                    must: true,
                    placeholder: "Enter Userinfo Endpoint",
                    onChanged: (v) => body["webhookUrl"] = v,
                  )),
              SizedBox(width: 20),
              Expanded(
                child: Container(),
              )
            ],
          )
        ],
      ),
    );
  }

  rowThree() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      color: Colors.white,
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 12),
                child: Text(
                  "Web Origin *",
                  style: TextStyle(fontSize: 25, fontFamily: "sf"),
                ),
              ),
              Container(
                height: 1,
                margin: EdgeInsets.only(bottom: 12),
                color: Color(0xffebebeb),
              ),
              AppTextField(
                titleStr: "Authentication Callback URL_1",
                placeholder: "Enter Authentication Callback URL",
                onChanged: (v) {
                  body["webOrigins"] = [v];
                },
              )
            ],
          )),
          SizedBox(width: 20),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 12),
                child: Text(
                  "Authentication Callback URL *",
                  style: TextStyle(fontSize: 25, fontFamily: "sf"),
                ),
              ),
              Container(
                height: 1,
                margin: EdgeInsets.only(bottom: 12),
                color: Color(0xffebebeb),
              ),
              AppTextField(
                titleStr: "Web Origin_1",
                placeholder: "Enter Web Origin",
                onChanged: (v) {
                  body["redirectUris"] = [v];
                },
              )
            ],
          ))
        ],
      ),
    );
  }

  // int tab_index = 0;
  // late TabController tabController;

  // @override
  // void initState() {
  //   super.initState();
  //   tabController = TabController(initialIndex: 0, vsync: this, length: 2);
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.only(left: 40),
        child: Column(
          children: [
            TopBar(
              backTitle: "Add New Tenant",
              popAction: () {
                Navigator.pop(context);
              },
            ),
            Expanded(
              child: ListView(
                controller: ScrollController(),
                children: [
                  rowOne(),
                  rowTwo(),
                  rowThree(),
                  // action
                  Container(
                    padding: EdgeInsets.only(left: 12, top: 12, bottom: 80),
                    child: Row(
                      children: [
                        AppBottomBtn(
                          label: "Submit for Approval",
                          color: Colors.blue,
                          onTap: netAddTenant,
                        )
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
