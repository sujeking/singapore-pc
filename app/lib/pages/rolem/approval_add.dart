import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_read_item.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/rolem.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class RoleApprovalAddPage extends StatefulWidget {
  RoleApprovalAddPage({Key? key}) : super(key: key);

  @override
  _RoleApprovalAddPageState createState() => _RoleApprovalAddPageState();
}

class _RoleApprovalAddPageState extends State<RoleApprovalAddPage> {
  Map<String, dynamic> body = {};
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      body = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
      setState(() {});
    });
  }

  netPassApproval() async {
    var id = body["_id"];
    if (null == id) {
      return;
    }
    var res = await roleAppPass(id);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show("Successful Operation", context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      Future.delayed(Duration(seconds: 2), () {
        Navigator.of(context).pop();
      });
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  netRejectApproval() async {
    var id = body["_id"];
    if (null == id) {
      return;
    }
    var res = await roleAppPass(id);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show("Successful Operation", context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      Future.delayed(Duration(seconds: 2), () {
        Navigator.of(context).pop();
      });
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  rowOne() {
    return Container(
      height: 100,
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
                title: "Request By", value: body["creatorName"] ?? ""),
          ),
          Expanded(
            child: AppReadItem(
                title: "Requested Date", value: body["createdAt"] ?? ""),
          ),
          Expanded(
            child: Container(),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
      height: 100,
      margin: EdgeInsets.only(top: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(title: "Role Name", value: body["name"] ?? ""),
          ),
          Expanded(
            child: AppReadItem(
                title: "Role Start Date", value: body["startDate"] ?? ""),
          ),
          Expanded(
            child: AppReadItem(
                title: "Role End Date", value: body["endDate"] ?? ""),
          ),
        ],
      ),
    );
  }

  rowThree() {
    return Container(
      height: 100,
      padding: EdgeInsets.only(left: 60),
      margin: EdgeInsets.only(top: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
                title: "MDM Registration Date", value: "10-12-2021"),
          ),
          Expanded(
            child: AppReadItem(
                title: "MDMDevice Registration Date", value: "10-12-2021"),
          ),
          Expanded(
            child:
                AppReadItem(title: "Device De-Registration Date", value: "-"),
          ),
        ],
      ),
    );
  }

  ActionView() {
    return Container(
      child: Row(
        children: [
          AppBottomBtn(
              label: "Approval Add New Role",
              color: Colors.blue,
              onTap: () {
                netPassApproval();
              }),
          SizedBox(
            width: 12,
          ),
          AppBottomBtn(
              label: "Reject Add New Role",
              color: Colors.red,
              onTap: () {
                netRejectApproval();
              }),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TopBar(
                  backTitle: "Approval Role",
                  popAction: () {
                    Navigator.pop(context);
                  },
                ),
                PageTitleView(title: "Add Role Approval"),
                SizedBox(
                  height: 40,
                ),
                Column(
                  children: [
                    rowOne(),
                    rowTwo(),
                  ],
                )
              ],
            ),
            Positioned(
              bottom: 80,
              child: ActionView(),
            )
          ],
        ),
      ),
    );
  }
}
