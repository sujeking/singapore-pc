import 'package:app/comps/app_table.dart';
import 'package:app/comps/app_table_cell.dart';
import 'package:app/comps/base_datatable.dart';
import 'package:app/comps/no_data_view.dart';
import 'package:app/net/userm.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class UserApprovalListPage extends StatefulWidget {
  UserApprovalListPage({Key? key}) : super(key: key);

  @override
  _UserListState createState() => _UserListState();
}

class _UserListState extends State<UserApprovalListPage> {
  List<String> cols = [
    "USER ACCOUNT NAME",
    "ROLE",
    "BRANCH",
    "REQUESTED BY",
    "REQUEST DATE",
    "ACTION "
  ];
  List<String> keys = [
    "name",
    "roleName",
    "branchName",
    "approverId",
    "approvedAt",
    "operationType",
  ];
  bool resort = false;
  String sortKey = "";
  List<Widget> rows = [];

  List<double> widths = [
    240,
    130,
    100,
    216,
    125,
    137,
  ];

  int _page = 1;
  String kwd = "";

  List datasource = [];
  bool isloading = true;
  @override
  void initState() {
    super.initState();
    sortKey = keys.first;
    netUserAppList(null);
  }

  netUserAppList(RefreshController? c) async {
    var fi = {
      "_page": _page,
      "_pageSize": 30,
      "_keyword": kwd,
      "_orderby": resort ? "${sortKey} desc" : "${sortKey} asc",
    };
    var res = await userAppList(fi);
    var list = res["list"];
    datasource.addAll(list);
    updateUI();
    isloading = false;
    setState(() {});
    c!.refreshCompleted();
    c.loadComplete();
  }

  updateUI() {
    rows.clear();
    for (var i = 0; i < datasource.length; i++) {
      var dict = datasource[i];
      var row = InkWell(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(children: [
              AppTableCell(
                  width: widths[0],
                  child: Row(
                    children: [
                      CircleAvatar(
                        radius: 16,
                        backgroundImage:
                            AssetImage("assets/img/user_avatar.jpeg"),
                        backgroundColor: Color(0xfff5f5f5),
                      ),
                      SizedBox(width: 10),
                      Text(
                        dict["name"] ?? "",
                        style: TextStyle(
                            fontSize: 12,
                            fontFamily: "sli",
                            color: Color(0xff080808)),
                      )
                    ],
                  )),
              AppTableCell(width: widths[1], text: dict["roleName"] ?? ""),
              AppTableCell(width: widths[2], text: dict["branchName"] ?? ""),
              AppTableCell(width: widths[3], text: dict["approverId"] ?? ""),
              AppTableCell(width: widths[4], text: dict["approvedAt"] ?? ""),
              AppTableCell(width: widths[5], text: dict["operationType"] ?? ""),
            ]),
            Container(
              height: 0.5,
              width: widths.reduce((a, b) => a + b),
              color: Color(0xffe0e0e0),
            )
          ],
        ),
      );
      rows.add(row);
    }

    if (rows.isEmpty) {
      rows.add(NoDataView(
        width: widths.reduce((a, b) => a + b),
      ));
    }
  }

  _cleanData() {
    // _page = 1;
    // rows.clear();
    // datasource.clear();
  }

  _header() {
    List<AppTable3Header> items = [];
    for (var i = 0; i < cols.length; i++) {
      AppTable3Header ii = AppTable3Header(
        sortkey: keys[i],
        text: cols[i],
        width: widths[i],
        isup: !resort,
        onTap: (key) {
          isloading = true;
          setState(() {});
          sortKey = key;
          _page = 1;
          resort = !resort;
          datasource.clear();
          netUserAppList(null);
        },
      );
      items.add(ii);
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: items),
        Container(
          height: 0.5,
          width: widths.reduce((a, b) => a + b),
          color: Color(0xffe0e0e0),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppTable3(
      isLoading: isloading,
      onRefresh: (RefreshController c) {
        isloading = true;
        setState(() {});
        _cleanData();
        _page = 1;
        netUserAppList(c);
      },
      onLoadMore: (RefreshController c) {
        isloading = true;
        setState(() {});
        _page += 1;
        netUserAppList(c);
      },
      children: [_header(), ...rows],
    );
  }
}
