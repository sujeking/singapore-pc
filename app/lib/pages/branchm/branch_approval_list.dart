import 'package:app/comps/app_table.dart';
import 'package:app/comps/app_table_cell.dart';
import 'package:app/comps/base_datatable.dart';
import 'package:app/net/branchm.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class BranchApprovalListPage extends StatefulWidget {
  String? kwd;
  BranchApprovalListPage({Key? key, this.kwd}) : super(key: key);

  @override
  _BranchApprovalPageSListtate createState() => _BranchApprovalPageSListtate();
}

class _BranchApprovalPageSListtate extends State<BranchApprovalListPage> {
  int _page = 1;
  int _pageSize = 30;
  var datasource = [];
  late var pageInfo;

  String kwd = "";

  List<String> cols = [
    "SUBSIDIARY",
    "BRANCH NAME",
    "BRANCH CODE",
    "LOCATION",
    "REQUESTED BY",
    "REQUESTED DATE",
    "ACTION",
  ];
  List<String> keys = [
    "subsidiary",
    "name",
    "code",
    "location",
    "creatorName",
    "updatedAt",
    "operationType"
  ];
  List<double> widths = [
    122.0,
    140.0,
    105.0,
    260.0,
    140.0,
    140.0,
    120.0,
  ];

  List<Widget> rows = [];

  bool isloading = true;

  bool resort = false;
  String sortKey = "";

  @override
  void initState() {
    super.initState();
    sortKey = keys.first;
    netBranchAppList(null);
  }

  @override
  void didUpdateWidget(BranchApprovalListPage oldWidget) {
    super.didUpdateWidget(oldWidget);
    kwd = widget.kwd ?? "";
    _cleanData();
    netBranchAppList(null);
  }

  netBranchAppList(RefreshController? c) async {
    var fi = {
      "_page": _page,
      "_pageSize": _pageSize,
      "_keyword": kwd,
      "_orderby": resort ? "${sortKey} desc" : "${sortKey} asc",
    };
    var res = await branchAppList(fi);
    var list = res["list"];
    datasource.addAll(list);
    updateUI();
    isloading = false;
    setState(() {});
    c!.refreshCompleted();
    c.loadComplete();
  }

  rowSelectHandle(dict) {
    print(dict);
    var type = dict["operationType"];
    if (type == "create") {
      Navigator.pushNamed(context, "/appadd", arguments: dict["newData"]);
    } else {
      Navigator.pushNamed(context, "/appedit", arguments: dict);
    }
  }

  updateUI() {
    rows.clear();
    for (var i = 0; i < datasource.length; i++) {
      var dict = datasource[i]["approval"];
      var newData = dict["newData"];
      var row = InkWell(
          onTap: () {
            rowSelectHandle(dict);
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(children: [
                AppTableCell(
                    width: widths[0], text: newData["subsidiary"] ?? ""),
                AppTableCell(width: widths[1], text: newData["name"] ?? ""),
                AppTableCell(width: widths[2], text: newData["code"] ?? ""),
                AppTableCell(width: widths[3], text: newData["location"] ?? ""),
                AppTableCell(
                    width: widths[4], text: newData["creatorName"] ?? ""),
                AppTableCell(
                    width: widths[5], text: newData["updatedAt"] ?? ""),
                AppTableCell(
                    width: widths[6], text: dict["operationType"] ?? ""),
              ]),
              Container(
                height: 0.5,
                width: widths.reduce((a, b) => a + b),
                color: Color(0xffe0e0e0),
              )
            ],
          ));
      rows.add(row);
    }
  }

  _cleanData() {
    _page = 1;
    rows.clear();
    datasource.clear();
  }

  _header() {
    List<AppTable3Header> items = [];
    for (var i = 0; i < cols.length; i++) {
      AppTable3Header ii = AppTable3Header(
        sortkey: keys[i],
        text: cols[i],
        isup: !resort,
        width: widths[i],
        onTap: (key) {
          isloading = true;
          setState(() {});
          sortKey = key;
          _page = 1;
          resort = !resort;
          datasource.clear();
          netBranchAppList(null);
        },
      );
      items.add(ii);
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: items),
        Container(
          height: 0.5,
          width: widths.reduce((a, b) => a + b),
          color: Color(0xffe0e0e0),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppTable3(
      isLoading: isloading,
      onRefresh: (RefreshController c) {
        isloading = true;
        setState(() {});
        _cleanData();
        netBranchAppList(c);
      },
      onLoadMore: (RefreshController c) {
        isloading = true;
        setState(() {});
        _page += 1;
        netBranchAppList(c);
      },
      children: [_header(), ...rows],
    );
  }
}
