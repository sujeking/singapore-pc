import 'package:app/comps/app_search_view.dart';
import 'package:app/comps/appitem.dart';
import 'package:app/comps/active_app_view.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/comps/tab_view.dart';
import 'package:app/net/tenantm.dart';
import 'package:app/pages/devicem/device_approval_list.dart';
import 'package:app/pages/devicem/device_list.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/material.dart';

class DeviceListPage extends StatefulWidget {
  DeviceListPage({Key? key}) : super(key: key);

  @override
  _DeviceListPageState createState() => _DeviceListPageState();
}

class _DeviceListPageState extends State<DeviceListPage>
    with SingleTickerProviderStateMixin {
  var table_index = 0;
  String kwd = "";
  var apps = [];
  GlobalKey<ScaffoldState> tabkey = GlobalKey<ScaffoldState>();

// action
  addDeviceHandle() {
    Navigator.pushNamed(context, "/add");
  }

  viewDeviceHandle() {
    Navigator.pushNamed(context, "/read");
  }

  approvalDeviceHandle() {
    Navigator.pushNamed(context, "/approval");
  }

  late TabController tabController;
  @override
  void initState() {
    super.initState();
    netApps();
    tabController =
        TabController(initialIndex: table_index, length: 2, vsync: this);
  }

  netApps() async {
    var res = await tenantActives();
    apps = res["data"];
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: tabkey,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        color: Colors.white,
        child: Column(
          children: [
            TopBar(
              backTitle: "Device Management",
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    PageTitleView(
                      title: "Device Management",
                    ),
                    ActiveAppView(
                      currentIdx: -1,
                      apps: apps,
                    ),
                    AppSearchView(
                      onSubmit: (e) {
                        kwd = e;
                        setState(() {});
                      },
                      btnTitle: 'Add Device',
                      onTap: addDeviceHandle,
                    ),
                    AppTabView(
                      titles: ["Device List", "Approval List"],
                      tabController: tabController,
                    ),
                    Expanded(
                      child: TabBarView(
                        physics: NeverScrollableScrollPhysics(),
                        controller: tabController,
                        children: [
                          DeviceList(kwd: kwd),
                          DeviceApprovalList(kwd: kwd),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
