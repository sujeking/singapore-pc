import 'package:app/comps/active_app_view.dart';
import 'package:app/comps/app_search_view.dart';
import 'package:app/comps/appitem.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/comps/tab_view.dart';
import 'package:app/net/tenantm.dart';
import 'package:app/pages/rolem/approval_list.dart';
import 'package:app/pages/rolem/role_list.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';

class RoleListIndexPage extends StatefulWidget {
  RoleListIndexPage({Key? key}) : super(key: key);

  @override
  _RoleListPageState createState() => _RoleListPageState();
}

class _RoleListPageState extends State<RoleListIndexPage>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  int appIdx = 0;
  var apps = [];
  String kwd = "";
  String clientId = "";
  @override
  void initState() {
    super.initState();
    netApps();
    tabController = TabController(length: 2, vsync: this);
  }

  netApps() async {
    var res = await tenantActives();
    apps = res["data"];
    clientId = apps[appIdx]["name"];
    setState(() {});
  }

// action
  addRoleHandle() {
    Navigator.pushNamed(context, "/add");
  }

  viewRoleHandle() {
    Navigator.pushNamed(context, "/read");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TopBar(
              backTitle: "Role Management",
            ),
            PageTitleView(title: "Role Management"),
            ActiveAppView(
              currentIdx: appIdx,
              apps: apps,
            ),
            AppSearchView(
                onSubmit: (e) {
                  kwd = e;
                  setState(() {});
                },
                onTap: addRoleHandle,
                btnTitle: "Add Role"),
            AppTabView(titles: [
              "Role List",
              "Approval List",
            ], tabController: tabController),
            Expanded(
                child: TabBarView(
              physics: NeverScrollableScrollPhysics(),
              controller: tabController,
              children: [
                RoleListPage(
                  kwd: kwd,
                  clientId: clientId,
                ),
                RoleApprovalListPage(
                  kwd: kwd,
                )
              ],
            ))
          ],
        ),
      ),
    );
  }
}
