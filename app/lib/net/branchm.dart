import 'net.dart';

branchAdd(Map<String, dynamic> data) async {
  var res = await $http("post", "/branch", data: data);
  return res;
}

branchDelete(String id) async {
  var res = await $http("delete", "/branch/" + id);
  return res;
}

branchUpdate(Map<String, dynamic> data) async {
  var res = await $http("put", "/branch/" + data["_id"], data: data);
  return res;
}

branchList(Map<String, dynamic> data) async {
  var res = await $http("get", "/branch", data: data);
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

branchDetail(String id) async {
  var res = await $http("get", "/branch/${id}");
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

// approval

branchAppList(Map<String, dynamic> data) async {
  var res = await $http("get", "/branch/approval/index", data: data);
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

branchAppPass(String id) async {
  var res = await $http("post", "/branch/${id}/approval/pass");
  return res;
}

branchAppReject(String id) async {
  var res = await $http("post", "/branch/${id}/approval/reject");
  return res;
}
