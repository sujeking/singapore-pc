import 'package:app/comps/active_app_view.dart';
import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_pick_field.dart';
import 'package:app/comps/app_table_cell.dart';
import 'package:app/comps/app_text_field.dart';
import 'package:app/comps/no_data_view.dart';
import 'package:app/net/logm.dart';
import 'package:app/net/userm.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:toast/toast.dart';

class TabItemView extends StatelessWidget {
  Function() onTap;
  bool selected;
  String text;

  TabItemView(
      {Key? key,
      required this.onTap,
      required this.text,
      required this.selected})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 12),
      child: Column(
        children: [
          InkWell(
            onTap: onTap,
            child: Container(
                constraints: BoxConstraints(minWidth: 120),
                alignment: Alignment.center,
                child: Text(
                  text,
                  style: TextStyle(
                      fontSize: 20,
                      color: selected ? Color(0xff080808) : Color(0xff999999),
                      fontFamily: "sbo",
                      fontWeight:
                          selected ? FontWeight.bold : FontWeight.normal),
                )),
          ),
          Container(
            height: 2,
            width: 120,
            color: selected ? Color(0xff000000) : Colors.white,
            margin: EdgeInsets.only(top: 12),
          )
        ],
      ),
    );
  }
}

class UserDetailPage extends StatefulWidget {
  UserDetailPage({Key? key}) : super(key: key);

  @override
  _UserDetailPageState createState() => _UserDetailPageState();
}

class _UserDetailPageState extends State<UserDetailPage>
    with SingleTickerProviderStateMixin {
  var info = {};
  List tenantList = [];
  var uid = "";
  Map<String, dynamic> currentTenant = {};
  List logs = [];

  @override
  void initState() {
    super.initState();
    tabController = TabController(initialIndex: 0, vsync: this, length: 2);

    Future.delayed(Duration.zero, () {
      var dict =
          ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
      ;
      uid = dict["uid"] ?? "";
      if (uid.isEmpty) {
        info = dict["info"];
        print("***************************");
        print(info);
        print("***************************");

        setState(() {});
      } else {
        netUserDetail();
      }
    });
  }

  netUserDetail() async {
    if (uid.isEmpty) {
      print("无效uid");
      return;
    }
    var res = await userDetail(uid);
    info = res;
    tenantList = info["tenantList"];
    currentTenant = tenantList[0] ?? {};
    setState(() {});
    netFetchLog();
  }

  netFetchLog() async {
    var fi = {
      "tenantId": currentTenant["tenantId"],
      "_page": 1,
      "_pageSize": 30,
    };
    var res = await logList(fi);
    logs = res["list"] ?? [];
    setState(() {});
  }

  netDelSessionHandle() async {
    String id = info["_id"];
    var res = await userDeleteSession(id);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      Future.delayed(Duration(milliseconds: 2), () {
        Navigator.of(context).pop();
      });
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  netDelUserHandle() async {
    String id = info["_id"];
    var res = await userDelete(id);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      Future.delayed(Duration(milliseconds: 2), () {
        Navigator.of(context).pop();
      });
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  _rowTabview() {
    List<Widget> tags = [];
    for (var i = 0; i < tenantList.length; i++) {
      var dict = tenantList[i];
      var tag = TabItemView(
        text: dict["tenantName"],
        onTap: () {
          tab_index = i;
          currentTenant = tenantList[i];
          netFetchLog();
        },
        selected: i == tab_index,
      );
      tags.add(tag);
    }

    return Container(
      padding: EdgeInsets.only(top: 12),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Column(
          children: [
            Row(children: tags),
          ],
        ),
      ),
    );
  }

  _listHeader(List cols, List<double> widths) {
    return Container(
      width: widths.reduce((a, b) => a + b) + widths.length * 12,
      child: Column(
        children: [
          Row(
            children: cols.map((e) {
              int idx = cols.indexOf(e);
              return Container(
                  height: 50,
                  width: widths[idx] + 12,
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(right: 12),
                  child: Text(
                    e,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ));
            }).toList(),
          ),
          Container(height: 1, color: Color(0xffebebeb))
        ],
      ),
    );
  }

  rowOne() {
    _tipview(title, value) {
      return Container(
        constraints: BoxConstraints(
          minWidth: 118,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: TextStyle(
                fontSize: 13,
                fontFamily: "sno",
                color: Color(0xff3c3c43).withOpacity(0.6),
              ),
            ),
            Text(
              value,
              style: TextStyle(
                fontSize: 15,
                fontFamily: "sli",
                color: Color(0xff080808),
              ),
            )
          ],
        ),
      );
    }

    return Container(
      padding: EdgeInsets.only(bottom: 50),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "View User",
            style: TextStyle(fontSize: 43, fontFamily: "sf"),
          ),
          SizedBox(
            height: 50,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipOval(
                child: Image.asset(
                  "assets/img/user_avatar.jpeg",
                  width: 233,
                  height: 233,
                ),
              ),
              SizedBox(
                width: 80,
              ),
              Expanded(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.only(right: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          info["name"] ?? "",
                          maxLines: 2,
                          style: TextStyle(fontSize: 51, fontFamily: "sf"),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                _tipview(
                                    "OTP Phone Number", info["phone"] ?? ""),
                                SizedBox(
                                  width: 50,
                                ),
                                _tipview("Email Address", info["email"] ?? ""),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                _tipview("User Name", info["name"] ?? ""),
                                SizedBox(
                                  width: 50,
                                ),
                                _tipview("User SamAccountName Logon",
                                    info["description"] ?? ""),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ))
            ],
          ),
        ],
      ),
    );
  }

  _rowDevice() {
    List<Widget> rows = [];

    List cols = [
      "DEVICE NAME",
      "DEVICE ID",
      "ASSET REGISTER",
      "CREATED BY",
      "APPROVED BY",
      "START DATE",
      "END DATE",
      "STATUS"
    ];
    List<double> widths = [
      130,
      230,
      180,
      130,
      130,
      130,
      130,
      130,
    ];

    List list = info["deviceList"] ?? [];

    for (var i = 0; i < list.length; i++) {
      var dict = list[i];
      var row = Row(
        children: [
          AppTableCell(width: widths[0], text: dict["deviceName"] ?? "---"),
          AppTableCell(width: widths[1], text: dict["deviceId"] ?? "---"),
          AppTableCell(width: widths[2], text: dict["register"] ?? "---"),
          AppTableCell(width: widths[3], text: dict["creatorName"] ?? "---"),
          AppTableCell(width: widths[4], text: dict["approval"] ?? "---"),
          AppTableCell(width: widths[5], text: dict["startDate"] ?? "---"),
          AppTableCell(width: widths[6], text: dict["endDate"] ?? "---"),
          AppTableCell(width: widths[7], text: dict["statusName"] ?? "---"),
        ],
      );
      rows.add(row);
    }

    if (rows.isEmpty) {
      rows.add(NoDataView());
    }

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 12),
            child: Text(
              "DEVICE ACCESS HISTORY",
              style: TextStyle(fontSize: 40, fontFamily: "sf"),
            ),
          ),
          Container(
            child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Column(
                  children: [_listHeader(cols, widths), ...rows],
                )),
          ),
        ],
      ),
    );
  }

  _rowIP() {
    List<Widget> rows = [];

    List cols = [
      "IP NAME",
      "IP ADDRESS",
      "CREATED BY",
      "APPROVED BY",
      "START DATE",
      "END DATE",
      "STATUS"
    ];
    List<double> widths = [
      130,
      130,
      130,
      130,
      130,
      130,
      130,
    ];

    List list = info["ipList"] ?? [];

    for (var i = 0; i < list.length; i++) {
      var dict = list[i];
      var row = Row(
        children: [
          AppTableCell(width: widths[0], text: dict["ipName"] ?? "---"),
          AppTableCell(width: widths[1], text: dict["ipAddress"] ?? "---"),
          AppTableCell(width: widths[2], text: dict["createAt"] ?? "---"),
          AppTableCell(width: widths[3], text: dict["approval"] ?? "---"),
          AppTableCell(width: widths[4], text: dict["startDate"] ?? "---"),
          AppTableCell(width: widths[5], text: dict["endDate"] ?? "---"),
          AppTableCell(width: widths[6], text: dict["statusName"] ?? "---"),
        ],
      );
      rows.add(row);
    }

    if (rows.isEmpty) {
      rows.add(NoDataView());
    }
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 12),
            child: Text(
              "IP ACCESS HISTORY",
              style: TextStyle(fontSize: 40, fontFamily: "sf"),
            ),
          ),
          Container(
            child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [_listHeader(cols, widths), ...rows],
                )),
          ),
        ],
      ),
    );
  }

  _rowTenant() {
    List<Widget> rows = [];

    List cols = [
      "ROLE",
      "BRANCH",
      "CREATED BY",
      "APPROVED BY",
      "START DATE",
      "END DATE",
      "STATUS"
    ];
    List<double> widths = [
      130,
      130,
      130,
      130,
      130,
      130,
      130,
    ];
    List roleList = currentTenant["roleList"] ?? [];
    for (var i = 0; i < roleList.length; i++) {
      var dict = roleList[i];
      var row = Row(
        children: [
          AppTableCell(width: widths[0], text: dict["roleName"] ?? "---"),
          AppTableCell(width: widths[1], text: dict["branchName"] ?? "---"),
          AppTableCell(width: widths[2], text: dict["creatorName"] ?? "---"),
          AppTableCell(width: widths[3], text: dict["approvedAt"] ?? "---"),
          AppTableCell(width: widths[4], text: dict["startDate"] ?? "---"),
          AppTableCell(width: widths[5], text: dict["endDate"] ?? "---"),
          AppTableCell(width: widths[6], text: dict["statusName"] ?? "---"),
        ],
      );
      rows.add(row);
    }

    if (rows.isEmpty) {
      rows.add(NoDataView(
        width: widths.reduce((a, b) => a + b),
      ));
    }

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _rowTabview(),
          SizedBox(height: 20),
          Container(
            padding: EdgeInsets.only(bottom: 12),
            child: Text(
              "TENANT ROLE HISTORY",
              style: TextStyle(fontSize: 40, fontFamily: "sf"),
            ),
          ),
          Container(
            child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [_listHeader(cols, widths), ...rows],
                )),
          ),
        ],
      ),
    );
  }

  _rowDetail() {
    List<Widget> rows = [];

    List cols = [
      "ROLE",
      "ACTIVITY DATE & TIME",
      "IP NAME",
      "DEVICE NAME",
      "ASSET REGISTER",
      "BRANCH",
      "ACTIVITY",
      "EXCEPTION",
    ];
    List<double> widths = [
      130,
      180,
      130,
      130,
      130,
      130,
      130,
      130,
    ];

    for (var i = 0; i < logs.length; i++) {
      var dict = logs[i];
      var row = Row(
        children: [
          AppTableCell(width: widths[0], text: dict["role"] ?? "---"),
          AppTableCell(width: widths[1], text: dict["activityTime"] ?? "---"),
          AppTableCell(width: widths[2], text: dict["ipAddress"] ?? "---"),
          AppTableCell(width: widths[3], text: dict["deviceId"] ?? "---"),
          AppTableCell(width: widths[4], text: dict["assetRegister"] ?? "---"),
          AppTableCell(width: widths[5], text: dict["branch"] ?? "---"),
          AppTableCell(width: widths[6], text: dict["activityType"] ?? "---"),
          AppTableCell(width: widths[7], text: dict["exception"] ?? "---"),
        ],
      );
      rows.add(row);
    }

    if (rows.isEmpty) {
      rows.add(NoDataView(
        width: widths.reduce((a, b) => a + b),
      ));
    }

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 12),
            child: Text(
              "DETAILED ACTIVITIES",
              style: TextStyle(fontSize: 40, fontFamily: "sf"),
            ),
          ),
          Container(
            child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Column(
                  children: [_listHeader(cols, widths), ...rows],
                )),
          ),
        ],
      ),
    );
  }

  int tab_index = 0;
  late TabController tabController;
  actionView() {
    return Container(
      padding: EdgeInsets.only(top: 12, bottom: 12),
      child: Row(
        children: [
          AppBottomBtn(
              label: "Edit User",
              color: Colors.blue,
              onTap: () {
                info["authUserId"] = info["authUserId"] ?? info["id"];
                Navigator.of(context).pushNamed("/edit", arguments: info);
              }),
          SizedBox(
            width: 12,
          ),
          Offstage(
            offstage: uid.isEmpty,
            child: AppBottomBtn(
              label: "Terminate Session",
              color: Colors.red,
              onTap: netDelSessionHandle,
            ),
          ),
          SizedBox(
            width: 12,
          ),
          Offstage(
            offstage: uid.isEmpty,
            child: AppBottomBtn(
              label: "Terminate User",
              color: Colors.red,
              onTap: netDelUserHandle,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Column(
          children: [
            TopBar(
              backTitle: "View User",
              popAction: () {
                Navigator.pop(context);
              },
            ),
            Expanded(
              child: ListView(
                controller: ScrollController(),
                children: [
                  rowOne(),
                  _rowDevice(),
                  _rowIP(),
                  _rowTenant(),
                  _rowDetail(),
                  // action
                  actionView()
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
