import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_pick_field.dart';
import 'package:app/comps/app_read_item.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/ipm.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class IPReadPage extends StatefulWidget {
  IPReadPage({Key? key}) : super(key: key);

  @override
  _IPReadPageState createState() => _IPReadPageState();
}

class _IPReadPageState extends State<IPReadPage> {
  Map<String, dynamic> info = {};
  late String idstr;
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      String id = ModalRoute.of(context)?.settings.arguments as String;
      idstr = id;
      netIPDetail(id);
    });
  }

  netIPDetail(String id) async {
    if (id.isEmpty) {
      return;
    }
    var res = await ipDetail(id);
    info = res;
    setState(() {});
  }

  netDelIP(String id) async {
    var res = await ipDelete(id);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show(
        "Successful Operation",
        context,
        gravity: Toast.CENTER,
        duration: Toast.LENGTH_LONG,
      );
      Future.delayed(
        Duration(seconds: 2),
        () {
          Navigator.of(context).pop();
        },
      );
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  rowOne() {
    return Container(
      height: 100,
      padding: EdgeInsets.only(left: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
              title: "Created By",
              value: info["creatorName"] ?? "",
            ),
          ),
          Expanded(
            child: AppReadItem(
              title: "Approved By",
              value: info["approverName"] ?? "",
            ),
          ),
          Expanded(
            child: Container(
              child: Column(),
            ),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
      height: 100,
      padding: EdgeInsets.only(left: 12),
      margin: EdgeInsets.only(top: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
              title: "IP Name",
              value: info["name"] ?? "",
            ),
          ),
          Expanded(
            child: AppReadItem(
              title: "IP Address",
              value: info["ipAddress"] ?? "",
            ),
          ),
          Expanded(
            child: AppReadItem(
              title: "IP Created Date",
              value: info["createdAt"] ?? "",
            ),
          ),
        ],
      ),
    );
  }

  ActionView() {
    return Container(
      child: Row(
        children: info["statusCode"] == 1
            ? [
                AppBottomBtn(
                    label: "Back",
                    color: Colors.blue,
                    onTap: () {
                      Navigator.of(context).pop();
                    }),
              ]
            : [
                AppBottomBtn(
                    label: "Edit IP",
                    color: Colors.blue,
                    onTap: () {
                      Navigator.of(context)
                          .pushNamed("/edit", arguments: info)
                          .then(
                        (value) {
                          netIPDetail(idstr);
                        },
                      );
                    }),
                SizedBox(width: 12),
                AppBottomBtn(
                    label: "Delete IP",
                    color: Colors.red,
                    onTap: () {
                      showDelAlertHandle(context, () {
                        netDelIP(idstr);
                      });
                    }),
              ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.fromLTRB(40, 12, 40, 80),
        child: Column(
          children: [
            TopBar(
              backTitle: "View IP",
              popAction: () {
                Navigator.pop(context);
              },
            ),
            Expanded(
              child: ListView(
                children: [
                  PageTitleView(title: "View IP"),
                  rowOne(),
                  rowTwo(),
                ],
              ),
            ),
            Offstage(
              offstage: info["statusCode"] == null,
              child: ActionView(),
            ),
          ],
        ),
      ),
    );
  }
}
