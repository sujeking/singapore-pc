import 'package:app/comps/app_table.dart';
import 'package:app/comps/app_table_cell.dart';
import 'package:app/comps/status_enum.dart';
import 'package:app/net/tenantm.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class TenantListPage extends StatefulWidget {
  String? kwd;
  TenantListPage({Key? key, this.kwd}) : super(key: key);

  @override
  _TenantListPageState createState() => _TenantListPageState();
}

class _TenantListPageState extends State<TenantListPage> {
  int _page = 1;
  int _pageSize = 30;
  var datasource = [];
  late var pageInfo;
  String kwd = "";

  List<String> cols = [
    "TENANT NAME",
    "CREATED BY",
    "APPROVED BY",
    "START DATE",
    "END DATE",
    "APPROVAL STATUS",
  ];
  List<String> keys = [
    "name",
    "creatorName",
    "approvedAt",
    "startDate",
    "endDate",
    "statusCode",
  ];
  bool resort = false;
  String sortKey = "";

  List<double> widths = [
    230.0,
    155.0,
    155.0,
    200.0,
    200.0,
    190.0,
  ];

  List<Widget> rows = [];
  bool isloading = true;
  @override
  void initState() {
    super.initState();
    sortKey = keys.first;
    netTenantList(null);
  }

  @override
  void didUpdateWidget(TenantListPage oldWidget) {
    super.didUpdateWidget(oldWidget);
    kwd = widget.kwd ?? "";
    rows.clear();
    datasource.clear();
    _page = 1;
    netTenantList(null);
  }

  netTenantList(RefreshController? c) async {
    var fi = {
      "_page": _page,
      "_pageSize": _pageSize,
      "_keyword": kwd,
      "_orderby": resort ? "${sortKey} desc" : "${sortKey} asc",
    };
    var res = await tenantList(fi);
    pageInfo = res["pageInfo"];
    var list = res["list"];
    datasource.addAll(list);
    print("okok");
    updateUI();
    isloading = false;
    setState(() {});
    c!.refreshCompleted();
    c.loadComplete();
  }

  rowSelectHandle(dict) {
    Navigator.pushNamed(context, "/read", arguments: dict["_id"]);
  }

  updateUI() {
    rows.clear();
    for (var i = 0; i < datasource.length; i++) {
      var dict = datasource[i];
      var row = InkWell(
        onTap: () {
          rowSelectHandle(dict);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(children: [
              AppTableCell(
                  width: widths[0],
                  child: Row(
                    children: [
                      ClipOval(
                        child: Container(
                          color: Color(0xfff5f5f5),
                          width: 32,
                          height: 32,
                          child: CachedNetworkImage(
                            imageUrl: dict["image"],
                            errorWidget: (
                              BuildContext context,
                              String url,
                              dynamic error,
                            ) =>
                                Icon(Icons.person, color: Color(0xff999999)),
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Text(
                        dict["name"] ?? "",
                        style: TextStyle(
                            fontSize: 12,
                            fontFamily: "sli",
                            color: Color(0xff080808)),
                      )
                    ],
                  )),
              AppTableCell(width: widths[1], text: dict["creatorName"] ?? ""),
              AppTableCell(width: widths[2], text: dict["approvedAt"] ?? "---"),
              AppTableCell(width: widths[3], text: dict["startDate"] ?? ""),
              AppTableCell(width: widths[4], text: dict["endDate"] ?? ""),
              AppTableCell(
                  width: widths[5],
                  child: Row(
                    children: [
                      ClipOval(
                        child: Container(
                          width: 8,
                          height: 8,
                          color: StatusColors[dict["statusCode"] as int],
                        ),
                      ),
                      SizedBox(width: 10),
                      Text(
                        dict["statusName"] ?? "",
                        style: TextStyle(
                            fontSize: 12,
                            fontFamily: "sli",
                            color: Color(0xff080808)),
                      )
                    ],
                  )),
            ]),
            Container(
              height: 0.5,
              width: widths.reduce((a, b) => a + b),
              color: Color(0xffe0e0e0),
            )
          ],
        ),
      );
      rows.add(row);
    }
  }

  _cleanData() {
    _page = 1;
    rows.clear();
    datasource.clear();
  }

  _header() {
    List<AppTable3Header> items = [];
    for (var i = 0; i < cols.length; i++) {
      AppTable3Header ii = AppTable3Header(
        sortkey: keys[i],
        isup: !resort,
        text: cols[i],
        width: widths[i],
        onTap: (key) {
          isloading = true;
          setState(() {});
          sortKey = key;
          _page = 1;
          resort = !resort;
          datasource.clear();
          netTenantList(null);
        },
      );
      items.add(ii);
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: items),
        Container(
          height: 0.5,
          width: widths.reduce((a, b) => a + b),
          color: Color(0xffe0e0e0),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppTable3(
      isLoading: isloading,
      onRefresh: (RefreshController c) {
        isloading = true;
        setState(() {});
        _cleanData();
        netTenantList(c);
      },
      onLoadMore: (RefreshController c) {
        isloading = true;
        setState(() {});
        _page += 1;
        netTenantList(c);
      },
      children: [_header(), ...rows],
    );
  }
}
