import 'dart:typed_data';

import 'package:dio/dio.dart';

import 'net.dart';

userAdd(Map<String, dynamic> data) async {
  var res = await $http("post", "/user", data: data);
  return res;
}

userDelete(String id) async {
  var res = await $http("delete", "/user/" + id);
  return res;
}

userDeleteSession(String id) async {
  var res = await $http("delete", "/user/${id}/session");
  return res;
}

userUploadImg(authUserId, String path) async {
  FormData formData =
      FormData.fromMap({"avatar": await MultipartFile.fromFile(path)});
  var res =
      await $http("post", "/user/upload/avatar/${authUserId}", data: formData);
  return res;
}

userUpdate(Map<String, dynamic> data) async {
  var res = await $http("put", "/user/" + data["_id"], data: data);
  return res;
}

userList(Map<String, dynamic> data) async {
  var res = await $http("get", "/user", data: data);
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

userListPop(Map<String, dynamic> data) async {
  var res = await $http("get", "/user/keycloak/index", data: data);
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

userDetail(String id) async {
  var res = await $http("get", "/user/${id}");
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

// approval

userAppList(Map<String, dynamic> data) async {
  var res = await $http("get", "/user/approval/index", data: data);
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

userAppPass(String id) async {
  var res = $http("post", "/user/approval/pass/${id}");
  return res;
}

userAppReject(String id) async {
  var res = $http("post", " /user/approval/reject/${id}");
  return res;
}
