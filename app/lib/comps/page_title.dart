import 'package:flutter/material.dart';

class PageTitleView extends StatelessWidget {
  final String title;
  const PageTitleView({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: TextStyle(fontSize: 32, fontFamily: "sse"),
    );
  }
}
