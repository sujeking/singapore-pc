import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_search_view.dart';
import 'package:app/comps/app_table.dart';
import 'package:app/comps/app_table_cell.dart';
import 'package:app/net/userm.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class UserPopPage extends StatefulWidget {
  Function(dynamic info) confirmHandle;
  Function() cancelHandle;
  UserPopPage({
    Key? key,
    required this.confirmHandle,
    required this.cancelHandle,
  }) : super(key: key);

  @override
  _UserPopPageState createState() => _UserPopPageState();
}

class _UserPopPageState extends State<UserPopPage> {
  late Function(dynamic info) confirmHandle;
  late Function() cancelHandle;

  var cols = ["USERNAME", "FULL NAME"];
  List<Widget> rows = [];
  int selectedidx = -1;
  var pageInfo = {};
  int _page = 1;
  int _pageSize = 60;
  String kwd = "";
  bool isLoading = true;
  var datasource = [];

  @override
  void initState() {
    super.initState();
    confirmHandle = widget.confirmHandle;
    cancelHandle = widget.cancelHandle;
    netUserList(null);
  }

  netUserList(RefreshController? c) async {
    var fi = {"_page": _page, "_pageSize": _pageSize, "search": kwd};
    var res = await userListPop(fi);
    var list = res;
    datasource.addAll(list);
    updateUI();
    isLoading = false;
    setState(() {});
    c!.refreshCompleted();
    c.loadComplete();
  }

  updateUI() {
    rows.clear();
    for (var i = 0; i < datasource.length; i++) {
      var dict = datasource[i];

      String firstName = dict["firstName"] ?? "";
      String lastName = dict["lastName"] ?? "";

      var row = InkWell(
        onTap: () {
          selectedidx = i;
          rows.clear();
          updateUI();
          setState(() {});
        },
        child: Container(
          color: selectedidx == i ? Colors.blue[100] : Colors.white,
          height: 40,
          padding: EdgeInsets.symmetric(horizontal: 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: AppTableCell(
                          width: double.infinity, text: dict["username"] ?? ""),
                    ),
                    Expanded(
                      child: AppTableCell(
                          width: double.infinity,
                          text: "${firstName}${lastName}"),
                    ),
                  ],
                ),
              ),
              Container(
                height: 1,
                color: Color(0xffe5e5e5),
              )
            ],
          ),
        ),
      );
      rows.add(row);
    }
  }

  _header() {
    List<Widget> items = [];
    for (var i = 0; i < cols.length; i++) {
      AppTable3Header ii = AppTable3Header(
        sortkey: "",
        isup: false,
        text: cols[i],
        hidenIcon: true,
        width: double.infinity,
      );
      Widget item = Expanded(child: ii);
      items.add(item);
    }
    return Row(children: items);
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.6,
      height: size.height * 0.9,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          )),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              padding: EdgeInsets.symmetric(horizontal: 80),
              child: AppSearchView(
                  onSubmit: (e) {
                    kwd = e;
                    _page = 1;
                    rows.clear();
                    datasource.clear();
                    netUserList(null);
                  },
                  needTitle: false,
                  onTap: () {},
                  btnTitle: "")),
          SizedBox(
            height: 12,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 80),
              child: ListView.builder(
                itemCount: datasource.length + 1,
                itemBuilder: (BuildContext context, int index) {
                  if (index == 0) {
                    return _header();
                  } else {
                    Widget item = rows[index - 1];
                    return item;
                  }
                },
              ),
              // child: AppTable3(
              //   onRefresh: (_refreshController) {
              //     _page = 1;
              //     rows.clear();
              //     datasource.clear();
              //     netUserList(_refreshController);
              //   },
              //   onLoadMore: (_refreshController) {
              //     _page += 1;
              //     netUserList(_refreshController);
              //   },
              //   children: [
              //     _header(),
              //     ...rows,
              //   ],
              // ),
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(12, 12, 12, 50),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Center(
                  child: Row(
                    children: [
                      AppBottomBtn(
                        label: "CONFIRM",
                        color: Color(0xff007aff),
                        onTap: () {
                          if (selectedidx == -1) {
                            return;
                          }
                          var dict = datasource[selectedidx];
                          dict["name"] = dict["username"];
                          dict["image"] = "";
                          dict["description"] = "";
                          dict["phone"] = "";
                          confirmHandle(dict);
                        },
                      ),
                      SizedBox(
                        width: 12,
                      ),
                      AppBottomBtn(
                        label: "CANCEL",
                        color: Colors.red,
                        onTap: () {
                          cancelHandle();
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
