import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_pick_field.dart';
import 'package:app/comps/app_table.dart';
import 'package:app/comps/app_text_field.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserEditApprovalPage extends StatefulWidget {
  UserEditApprovalPage({Key? key}) : super(key: key);

  @override
  _UserEditApprovalPageState createState() => _UserEditApprovalPageState();
}

class _UserEditApprovalPageState extends State<UserEditApprovalPage>
    with SingleTickerProviderStateMixin {
  _rowProfile() {
    _tipview(title, value) {
      return Container(
        constraints: BoxConstraints(
          minWidth: 118,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: TextStyle(
                  fontSize: 13, color: Color(0xff3c3c43).withOpacity(0.6)),
            ),
            Text(
              value,
              style: TextStyle(fontSize: 15, color: Color(0xff080808)),
            )
          ],
        ),
      );
    }

    return Container(
      margin: EdgeInsets.only(bottom: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Edit User Approval",
            style: TextStyle(fontSize: 43, fontFamily: "sf"),
          ),
          SizedBox(
            height: 40,
          ),
          Container(
            height: 233,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipOval(
                  child: Image.asset(
                    "assets/img/user_avatar.jpeg",
                    width: 233,
                    height: 233,
                  ),
                ),
                SizedBox(
                  width: 70,
                ),
                Expanded(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.only(
                        right: 20,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "HAIFAA AFIQAH",
                            maxLines: 2,
                            style: TextStyle(fontSize: 51, fontFamily: "sf"),
                          ),
                          SizedBox(
                            height: 12,
                          ),
                          Column(
                            children: [
                              Row(
                                children: [
                                  _tipview("OTP Phone Number", "+6738282205"),
                                  SizedBox(
                                    width: 50,
                                  ),
                                  _tipview("Email Address",
                                      "bazla.suhaili@at-tamwil.com"),
                                ],
                              ),
                              SizedBox(
                                height: 24,
                              ),
                              Row(
                                children: [
                                  _tipview("User Name", "bazla suhaili"),
                                  SizedBox(
                                    width: 50,
                                  ),
                                  _tipview("User SamAccountName Logon",
                                      "at-tamwil \* bazla"),
                                ],
                              )
                            ],
                          ),
                        ],
                      ),
                    ))
              ],
            ),
          ),
        ],
      ),
    );
  }

  _rowDevice() {
    List cols = [
      "RECORD TYPE",
      "DEVICE NAME",
      "DEVICE ID",
      "START DATE",
      "END DATE",
      "REQUESTED BY",
      "REQUESTED DATE",
      ""
    ];
    List<double> widths = [130, 130, 130, 130, 130, 130, 130, 200];

    List<Widget> rows = [];
    for (var i = 0; i < 5; i++) {
      BtnCellModel model = BtnCellModel(Colors.blue, "APPROVE");
      BtnCellModel model2 = BtnCellModel(Colors.red, "REJECT");

      var row = Row(
        children: [
          TextCell(oldValue: "record type", newValue: "002", width: widths[0]),
          TextCell(oldValue: "device name", newValue: "002", width: widths[1]),
          TextCell(oldValue: "device id", newValue: "002", width: widths[2]),
          TextCell(oldValue: "start date", newValue: "002", width: widths[3]),
          TextCell(oldValue: "end date", newValue: "002", width: widths[4]),
          TextCell(oldValue: "requested by", newValue: "002", width: widths[5]),
          TextCell(
              oldValue: "requested date", newValue: "002", width: widths[6]),
          BtnCell(models: [model, model2], width: widths[7])
        ],
      );
      rows.add(row);
    }

    return Container(
      margin: EdgeInsets.only(bottom: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 12),
                  child: Text(
                    "Device ACCESS REVIEW",
                    style: TextStyle(fontSize: 30, fontFamily: "sf"),
                  ),
                ),
                SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _listHeader(cols, widths),
                        ...rows,
                      ],
                    )),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _rowIP() {
    List cols = [
      "RECORD TYPE",
      "IP NAME",
      "IP ADDRESS",
      "START DATE",
      "END DATE",
      "REQUESTED BY",
      "REQUESTED DATE",
      "123",
    ];

    List<double> widths = [
      130,
      130,
      130,
      130,
      130,
      130,
      130,
      200,
    ];

    List<Widget> rows = [];
    for (var i = 0; i < 5; i++) {
      BtnCellModel model = BtnCellModel(Colors.blue, "APPROVE");
      BtnCellModel model2 = BtnCellModel(Colors.red, "REJECT");

      var row = Row(
        children: [
          TextCell(oldValue: "record type", newValue: "002", width: widths[0]),
          TextCell(oldValue: "ip name", newValue: "002", width: widths[1]),
          TextCell(oldValue: "ip address", newValue: "002", width: widths[2]),
          TextCell(oldValue: "start date", newValue: "002", width: widths[3]),
          TextCell(oldValue: "end date", newValue: "002", width: widths[4]),
          TextCell(oldValue: "requested by", newValue: "002", width: widths[5]),
          TextCell(
              oldValue: "requested date", newValue: "002", width: widths[6]),
          BtnCell(models: [model, model2], width: widths[7])
        ],
      );
      rows.add(row);
    }

    return Container(
      margin: EdgeInsets.only(bottom: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 12),
                  child: Text(
                    "IP ACCESS REVIEW",
                    style: TextStyle(fontSize: 30, fontFamily: "sf"),
                  ),
                ),
                SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _listHeader(cols, widths),
                        ...rows,
                      ],
                    )),
              ],
            ),
          ),
        ],
      ),
    );
  }

  int tab_index = 0;
  late TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(initialIndex: 0, vsync: this, length: 2);
  }

  tabView() {
    return Container(
      padding: EdgeInsets.only(top: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Column(
                children: [
                  InkWell(
                    onTap: () {
                      tab_index = 0;
                      tabController.animateTo(tab_index);
                      setState(() {});
                    },
                    child: Container(
                        constraints: BoxConstraints(minWidth: 120),
                        alignment: Alignment.center,
                        child: Text(
                          "ULTRON +",
                          style: TextStyle(
                              fontSize: 20,
                              color: tab_index == 0
                                  ? Color(0xff080808)
                                  : Color(0xff999999),
                              fontFamily: "sf",
                              fontWeight: tab_index == 0
                                  ? FontWeight.bold
                                  : FontWeight.normal),
                        )),
                  ),
                  Container(
                    height: 2,
                    width: 120,
                    color: tab_index == 0 ? Color(0xff000000) : Colors.white,
                    margin: EdgeInsets.only(top: 12),
                  )
                ],
              ),
              SizedBox(
                width: 12,
              ),
              Column(
                children: [
                  InkWell(
                    onTap: () {
                      tab_index = 1;
                      tabController.animateTo(tab_index);
                      setState(() {});
                    },
                    child: Container(
                        constraints: BoxConstraints(minWidth: 120),
                        alignment: Alignment.center,
                        child: Text(
                          "EXPONENTIAL PRO",
                          style: TextStyle(
                              fontSize: 20,
                              color: tab_index == 1
                                  ? Color(0xff080808)
                                  : Color(0xff999999),
                              fontFamily: "sf",
                              fontWeight: tab_index == 1
                                  ? FontWeight.bold
                                  : FontWeight.normal),
                        )),
                  ),
                  Container(
                    height: 2,
                    width: 120,
                    color: tab_index == 1 ? Color(0xff000000) : Colors.white,
                    margin: EdgeInsets.only(top: 12),
                  )
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  _rowTenant() {
    var size = MediaQuery.of(context).size;
    var width = size.width;

    List cols = [
      "RECORD TYPE",
      "ROLE",
      "BRANCH",
      "START DATE",
      "END DATE",
      "REQUESTED BY",
      "REQUESTED DATE",
      ""
    ];
    List<double> widths = [
      130.0,
      130.0,
      130.0,
      130.0,
      130.0,
      130.0,
      130.0,
      200.0,
    ];

    List<Widget> rows = [];
    for (var i = 0; i < 5; i++) {
      BtnCellModel model = BtnCellModel(Colors.blue, "APPROVE");
      BtnCellModel model2 = BtnCellModel(Colors.red, "REJECT");

      var row = Row(
        children: [
          TextCell(oldValue: "record type", newValue: "002", width: widths[0]),
          TextCell(oldValue: "role", newValue: "002", width: widths[1]),
          TextCell(oldValue: "branch", newValue: "002", width: widths[2]),
          TextCell(oldValue: "start date", newValue: "002", width: widths[3]),
          TextCell(oldValue: "end date", newValue: "002", width: widths[4]),
          TextCell(oldValue: "requested by", newValue: "002", width: widths[5]),
          TextCell(
              oldValue: "requested date", newValue: "002", width: widths[6]),
          BtnCell(models: [model, model2], width: widths[7])
        ],
      );
      rows.add(row);
    }

    return Container(
      margin: EdgeInsets.only(bottom: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 12),
                  child: Text(
                    "TENANT ROLE HISTORY",
                    style: TextStyle(fontSize: 30, fontFamily: "sf"),
                  ),
                ),
                _rowTabview(),
                SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _listHeader(cols, widths),
                        ...rows,
                      ],
                    )),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _actionView() {
    return Container(
        padding: EdgeInsets.only(bottom: 40),
        child: Row(
          children: [
            AppBottomBtn(
              label: "Back",
              color: Colors.blue,
              onTap: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ));
  }

  _listHeader(List cols, List<double> widths) {
    return Container(
      width: widths.reduce((a, b) => a + b) + widths.length * 12,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: cols.map((e) {
              int idx = cols.indexOf(e);
              return Container(
                  height: 50,
                  width: widths[idx] + 12,
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(right: 12),
                  child: Text(
                    e,
                    style: TextStyle(
                      fontSize: 11,
                      fontWeight: FontWeight.bold,
                    ),
                  ));
            }).toList(),
          ),
          Container(height: 1, color: Color(0xffebebeb))
        ],
      ),
    );
  }

  _rowTabview() {
    List<Widget> tags = [];
    for (var i = 0; i < 2; i++) {
      var tag = _TabItemView(
        text: "App${i}",
        onTap: () {
          tab_index = i;
        },
        selected: i == tab_index,
      );
      tags.add(tag);
    }

    return Container(
      padding: EdgeInsets.only(top: 12),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(children: tags),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Column(
          children: [
            TopBar(
              backTitle: "User Management / Edit User Approval",
              popAction: () {
                Navigator.pop(context);
              },
            ),
            Expanded(
              child: ListView(
                controller: ScrollController(),
                children: [
                  _rowProfile(),
                  _rowDevice(),
                  _rowIP(),
                  _rowTenant(),
                  _actionView()
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class TextCell extends StatelessWidget {
  double width;
  String oldValue;
  String newValue;
  TextCell({
    Key? key,
    required this.oldValue,
    required this.newValue,
    required this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width + 12,
      height: 70,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 12),
                Text(
                  oldValue,
                  style: TextStyle(
                    fontFamily: "sli",
                    fontSize: 11,
                    color: Color(0xff646464),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  newValue,
                  style: TextStyle(
                    fontFamily: "sli",
                    fontSize: 11,
                    color: Color(0xff007aff),
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 1,
            color: Color(0xffe5e5e5),
          )
        ],
      ),
    );
  }
}

class BtnCellModel {
  String btnText;
  Color btnColor;
  BtnCellModel(this.btnColor, this.btnText);
}

class BtnCell extends StatelessWidget {
  List<BtnCellModel> models;
  Function(int idx)? onTap;
  double width;
  BtnCell({
    Key? key,
    required this.models,
    required this.width,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      constraints: BoxConstraints(minWidth: width + 20),
      child: Column(
        children: [
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 12),
                Row(
                  children: models.map((e) {
                    return Container(
                      margin: EdgeInsets.only(right: 20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(width: 1, color: e.btnColor),
                      ),
                      padding: EdgeInsets.symmetric(
                        horizontal: 20,
                        vertical: 10,
                      ),
                      child: Text(
                        e.btnText,
                        style: TextStyle(
                          fontSize: 11,
                          color: e.btnColor,
                          fontFamily: "sli",
                        ),
                      ),
                    );
                  }).toList(),
                ),
              ],
            ),
          ),
          Container(
            height: 1,
            width: width + 12,
            color: Color(0xffe5e5e5),
          )
        ],
      ),
    );
  }
}

class _TabItemView extends StatelessWidget {
  Function() onTap;
  bool selected;
  String text;

  _TabItemView(
      {Key? key,
      required this.onTap,
      required this.text,
      required this.selected})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: onTap,
            child: Container(
                constraints: BoxConstraints(minWidth: 120),
                // alignment: Alignment.center,
                child: Text(
                  text,
                  style: TextStyle(
                      fontSize: 15,
                      color: selected ? Color(0xff080808) : Color(0xff999999),
                      fontFamily: "sbo",
                      fontWeight:
                          selected ? FontWeight.bold : FontWeight.normal),
                )),
          ),
          Container(
            height: 2,
            width: 120,
            color: selected ? Color(0xff000000) : Colors.white,
            margin: EdgeInsets.only(top: 12),
          )
        ],
      ),
    );
  }
}
