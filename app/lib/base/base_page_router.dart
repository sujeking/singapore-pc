import 'package:flutter/material.dart';

class BasePageRouter extends StatefulWidget {
  final Map dict;
  final String initPath;
  BasePageRouter({Key? key, required this.dict, required this.initPath})
      : super(key: key);

  @override
  _BasePageRouterState createState() => _BasePageRouterState();
}

class _BasePageRouterState extends State<BasePageRouter> {
  Map dict = {};
  String initPath = "";
  @override
  void initState() {
    super.initState();
    dict = widget.dict;
    initPath = widget.initPath;
  }

  RoutePageBuilder createPages(RouteSettings settings) {
    return (BuildContext nContext, Animation<double> animation,
        Animation<double> secondaryAnimation) {
      String? path = settings.name;
      return dict[path] as Widget;
    };
  }

  Widget build(BuildContext context) {
    return Navigator(
        initialRoute: initPath,
        onGenerateRoute: (val) {
          RoutePageBuilder builder = createPages(val);
          return PageRouteBuilder(
              settings: val,
              pageBuilder: builder,
              transitionsBuilder: (BuildContext context,
                  Animation<double> animation,
                  Animation secanimation,
                  Widget child) {
                return SlideTransition(
                  position: Tween<Offset>(
                    begin: Offset(1, 0),
                    end: Offset(0, 0),
                  ).animate(animation),
                  child: child,
                );
              });
        });
  }
}
