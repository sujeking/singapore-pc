import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

String BASEURI = "https://ssoapi.fornewtech.com/api";
// String BASEURI = "http://localhost:8099";

$http(String method, String path, {dynamic data}) async {
  Response response;
  try {
    final Dio dio = Dio(
      BaseOptions(
        method: method,
        baseUrl: BASEURI,
      ),
    );
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    dio.options.headers['content-Type'] = 'application/json';
    print("==========准备请求${method}=============");
    print(path);
    print(data);
    if (data == null) {
      response = await dio.request(path);
    } else {
      if (method == "get") {
        response = await dio.request(path, queryParameters: data);
      } else {
        response = await dio.request(path, data: data);
      }
    }
    print("=========结果=============");
    print(response);
    return response.data;
  } catch (e) {
    return {"message": e.toString(), "statusCode": -1};
  }
}

//loading

late OverlayEntry loadentry;
showLoadingHandle(BuildContext context) {
  loadentry = OverlayEntry(builder: (BuildContext context) {
    return Container(
      color: Colors.black.withOpacity(0.1),
      child: Center(
        child: Container(
          child: CupertinoActivityIndicator(radius: 20),
        ),
      ),
    );
  });
//往Overlay中插入插入OverlayEntry
  Overlay.of(context)?.insert(loadentry);
  return dismissLoading;
}

void dismissLoading() {
  loadentry.remove();
}
