import 'package:flutter/material.dart';

class AppBottomBtn extends StatelessWidget {
  final String label;
  final Color color;
  final Function onTap;
  const AppBottomBtn({
    Key? key,
    required this.label,
    required this.color,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap();
      },
      child: Container(
        alignment: Alignment.center,
        constraints: BoxConstraints(minWidth: 251),
        padding: EdgeInsets.symmetric(horizontal: 50, vertical: 8),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            border: Border.all(width: 1, color: color)),
        child: Text(
          label,
          style: TextStyle(color: color, fontFamily: "sli", fontSize: 16),
        ),
      ),
    );
  }
}
