import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_read_item.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/ipm.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class IPApprovalAddPage extends StatefulWidget {
  IPApprovalAddPage({Key? key}) : super(key: key);

  @override
  _IPApprovalAddPageState createState() => _IPApprovalAddPageState();
}

class _IPApprovalAddPageState extends State<IPApprovalAddPage> {
  Map<String, dynamic> body = {};

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      body = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
      setState(() {});
    });
  }

  netPassApproval() async {
    var id = body["_id"];
    if (null == id) {
      return;
    }
    var res = await ipAppPass(id);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show("Successful Operation", context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      Future.delayed(Duration(seconds: 2), () {
        Navigator.of(context).pop();
      });
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  netRejectApproval() async {
    var id = body["_id"];
    if (null == id) {
      return;
    }
    var res = await ipAppReject(id);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show("Successful Operation", context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      Future.delayed(Duration(seconds: 2), () {
        Navigator.of(context).pop();
      });
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  rowOne() {
    return Container(
      height: 100,
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
                title: "Request By", value: body["creatorName"] ?? ""),
          ),
          Expanded(
            child: AppReadItem(
                title: "Requested Date", value: body["updatedAt"] ?? ""),
          ),
          Expanded(
            child: Container(),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
      height: 100,
      margin: EdgeInsets.only(top: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(title: "IP Name", value: body["name"] ?? ""),
          ),
          Expanded(
            child: AppReadItem(
                title: "IP Address", value: body["ipAddress"] ?? ""),
          ),
          Expanded(
            child: AppReadItem(
                title: "IP Created Date", value: body["createdAt"] ?? ""),
          ),
        ],
      ),
    );
  }

  backHandle() {
    Navigator.pop(context);
  }

  ActionView() {
    return Container(
      child: Row(
        children: [
          AppBottomBtn(
              label: "Approval Add New IP",
              color: Colors.blue,
              onTap: () {
                netPassApproval();
              }),
          SizedBox(
            width: 12,
          ),
          AppBottomBtn(
              label: "Reject Add New IP",
              color: Colors.red,
              onTap: () {
                netRejectApproval();
              }),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TopBar(
                  backTitle: "Approval IP",
                  popAction: () {
                    Navigator.pop(context);
                  },
                ),
                PageTitleView(title: "Add IP Approval"),
                SizedBox(
                  height: 40,
                ),
                Column(
                  children: [
                    rowOne(),
                    rowTwo(),
                  ],
                )
              ],
            ),
            Positioned(
              bottom: 80,
              child: ActionView(),
            )
          ],
        ),
      ),
    );
  }
}
