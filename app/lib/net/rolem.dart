import 'dart:convert';
import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';

import 'net.dart';

roleAdd(Map<String, dynamic> data) async {
  var res = await $http("post", "/role", data: data);
  return res;
}

roleDelete(String id) async {
  var res = await $http("delete", "/role/" + id);
  return res;
}

roleUpdate(Map<String, dynamic> data) async {
  var res = await $http("put", "/role/" + data["_id"], data: data);
  return res;
}

roleList(Map<String, dynamic> data) async {
  var res = await $http("get", "/role", data: data);
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

roleDetail(String id) async {
  var res = await $http("get", "/role/${id}");
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

// approval

roleAppAdd() async {}

roleAppDelete() async {}

roleAppUpdate() async {}

roleAppList(Map<String, dynamic> data) async {
  var res = await $http("get", "/role/approval/index", data: data);
  int statusCode = res["statusCode"];
  if (statusCode == 200) {
    return res["data"];
  } else {
    return {};
  }
}

roleAppPass(String id) async {
  var res = await $http("post", "/role/${id}/approval/pass");
  return res;
}

roleAppReject(String id) async {
  var res = await $http("post", "/role/${id}/approval/reject");
  return res;
}
