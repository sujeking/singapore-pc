import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_search_view.dart';
import 'package:app/comps/app_table.dart';
import 'package:app/comps/app_table_cell.dart';
import 'package:app/net/tenantm.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class UserTenantPopPage extends StatefulWidget {
  Function(dynamic) confirmHandle;
  Function() cancelHandle;
  UserTenantPopPage({
    Key? key,
    required this.confirmHandle,
    required this.cancelHandle,
  }) : super(key: key);

  @override
  _UserTenantPopPageState createState() => _UserTenantPopPageState();
}

class _UserTenantPopPageState extends State<UserTenantPopPage> {
  late Function(dynamic) confirmHandle;
  late Function() cancelHandle;

  List<double> widths = [400];
  List<Widget> rows = [];
  int selectedidx = -1;
  var pageInfo = {};
  int _page = 1;
  int _pageSize = 30;
  String kwd = "";
  var datasource = [];

  @override
  void initState() {
    super.initState();
    confirmHandle = widget.confirmHandle;
    cancelHandle = widget.cancelHandle;
    netUserList(null);
  }

  netUserList(RefreshController? c) async {
    var fi = {"_page": _page, "_pageSize": _pageSize, "search": kwd};
    var res = await tenantList(fi);
    datasource.addAll(res["list"]);
    updateUI();
    setState(() {});
    c!.refreshCompleted();
    c.loadComplete();
  }

  updateUI() {
    for (var i = 0; i < datasource.length; i++) {
      var dict = datasource[i];

      var row = InkWell(
        onTap: () {
          selectedidx = i;
          rows.clear();
          updateUI();
          setState(() {});
        },
        child: Container(
          color: selectedidx == i ? Colors.blue[100] : Colors.white,
          height: 40,
          padding: EdgeInsets.symmetric(horizontal: 12),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AppTableCell(width: widths[0], text: dict["name"] ?? ""),
            ],
          ),
        ),
      );
      rows.add(row);
    }
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.6,
      height: size.height * 0.9,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          )),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              padding: EdgeInsets.symmetric(horizontal: 80),
              child: AppSearchView(
                  onSubmit: (e) {
                    kwd = e;
                    _page = 1;
                    rows.clear();
                    datasource.clear();
                    netUserList(null);
                  },
                  needTitle: false,
                  onTap: () {},
                  btnTitle: "")),
          SizedBox(
            height: 12,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 80),
              child: AppTable3(
                onRefresh: (_refreshController) {
                  _page = 1;
                  rows.clear();
                  datasource.clear();
                  netUserList(_refreshController);
                },
                onLoadMore: (_refreshController) {
                  _page += 1;
                  netUserList(_refreshController);
                },
                children: [
                  ...rows,
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(12, 12, 12, 50),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Center(
                  child: Row(
                    children: [
                      AppBottomBtn(
                        label: "CONFIRM",
                        color: Color(0xff007aff),
                        onTap: () {
                          if (selectedidx == -1) {
                            return;
                          }
                          var dict = datasource[selectedidx];
                          confirmHandle(dict);
                        },
                      ),
                      SizedBox(
                        width: 12,
                      ),
                      AppBottomBtn(
                        label: "CANCEL",
                        color: Colors.red,
                        onTap: () {
                          cancelHandle();
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
