import 'dart:math';

import 'package:app/comps/app_filtter_view.dart';
import 'package:app/comps/active_app_view.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/tenantm.dart';
import 'package:app/pages/dashboard/bashboard_listview.dart';
import 'package:app/pages/dashboard/right_chart.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/material.dart';

import 'package:charts_flutter/flutter.dart' as charts;

import 'left_chart.dart';

class DashBoard extends StatefulWidget {
  DashBoard({Key? key}) : super(key: key);

  @override
  _DashBoardState createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  static Color bordeColor = Color(0xffe6e6e6);
  var apps = [];
  int leftIdx = 0;
  int rightIdx = 0;

  @override
  void initState() {
    super.initState();
    netApps();
  }

  netApps() async {
    var res = await tenantActives();
    apps = res["data"];
    setState(() {});
  }

  Charts() {
    return Row(
      children: [
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                padding: EdgeInsets.only(bottom: 12),
                child: Text("Active Sessions Log",
                    style: TextStyle(fontSize: 18))),
            Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(width: 1, color: bordeColor)),
                child: Column(
                  children: [
                    //buttons
                    Row(
                      children: [
                        Expanded(child: AppFiltterView(
                          onClick: (idx) {
                            leftIdx = idx;
                            setState(() {});
                          },
                        )),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 12),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  ClipOval(
                                    child: Container(
                                      width: 11,
                                      height: 11,
                                      color: Color(0xffc2dbf2),
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Text(
                                    "No. of Currently Active User",
                                    style: TextStyle(
                                        fontSize: 10, color: Color(0xff080808)),
                                  )
                                ],
                              ),
                              SizedBox(height: 10),
                              Row(
                                children: [
                                  ClipOval(
                                    child: Container(
                                      width: 11,
                                      height: 11,
                                      color: Color(0xfffad5d4),
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Text(
                                    "No. of Failed Log-Ins",
                                    style: TextStyle(
                                        fontSize: 10, color: Color(0xff080808)),
                                  )
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: SizedBox(
                        width: 1200,
                        height: 280,
                        child: LeftChart(
                          idx: leftIdx,
                        ),
                      ),
                    ),
                  ],
                )),
          ],
        )),
        SizedBox(
          width: 12,
        ),
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 12),
              child: Text("Exceptions", style: TextStyle(fontSize: 18)),
            ),
            Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(width: 1, color: bordeColor)),
                child: Column(
                  children: [
                    //buttons
                    Row(
                      children: [
                        Expanded(child: AppFiltterView(
                          onClick: (idx) {
                            rightIdx = idx;
                            setState(() {});
                          },
                        )),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 12),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  ClipOval(
                                    child: Container(
                                      width: 11,
                                      height: 11,
                                      color: Color(0xffd8e7e2),
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Text(
                                    "No. of Unregistered IPs",
                                    style: TextStyle(
                                        fontSize: 10, color: Color(0xff080808)),
                                  )
                                ],
                              ),
                              SizedBox(height: 10),
                              Row(
                                children: [
                                  ClipOval(
                                    child: Container(
                                      width: 11,
                                      height: 11,
                                      color: Color(0xfff9eed8),
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Text(
                                    "No. of Unregistered Device",
                                    style: TextStyle(
                                        fontSize: 10, color: Color(0xff080808)),
                                  )
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: SizedBox(
                          width: 1200,
                          height: 280,
                          child: RightChart(
                            idx: rightIdx,
                          )),
                    ),
                  ],
                )),
          ],
        )),
      ],
    );
  }

  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.fromLTRB(40, 12, 35, 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TopBar(
            backTitle: "Dashboard",
          ),
          PageTitleView(title: "Dashboard"),
          ActiveAppView(
            currentIdx: -1,
            apps: apps,
          ),
          Container(
            height: 12,
            color: Colors.white,
          ),
          Expanded(
            child: SingleChildScrollView(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Charts(),
                Container(
                    padding: EdgeInsets.symmetric(vertical: 12),
                    child: Text("Recent Activities",
                        style: TextStyle(fontSize: 18))),
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(width: 1, color: bordeColor)),
                  constraints: BoxConstraints(maxHeight: 900),
                  child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 12),
                      child: BashBoardListView()),
                )
              ],
            )),
          )
        ],
      ),
    );
  }
}
