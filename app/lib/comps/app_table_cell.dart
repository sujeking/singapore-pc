import 'package:app/comps/base_datatable.dart';
import 'package:flutter/material.dart';

// BaseDataCell AppTableCell({text, child}) {
//   if (null != child) {
//     return BaseDataCell(child);
//   } else {
//     return BaseDataCell(Text(text,
//         style: TextStyle(
//             fontSize: 12, fontFamily: "sli", color: Color(0xff080808))));
//   }
// }

AppTableCell({text, child, width}) {
  if (null != child) {
    return Container(
      child: child,
      width: width + 12,
    );
  } else {
    return Container(
        width: width + 12,
        height: 55,
        alignment: Alignment.centerLeft,
        child: Text(text,
            style: TextStyle(
                fontSize: 12, fontFamily: "sli", color: Color(0xff080808))));
  }
}
