import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';

late OverlayEntry entry;
void showCalenderHandle(BuildContext context, Function(String v) callback) {
  entry = OverlayEntry(builder: (BuildContext context) {
    return Container(
      color: Colors.black.withOpacity(0.4),
      child: Center(
        child: ClipRRect(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
          child: Container(
            width: 550,
            height: 420,
            color: Colors.white,
            child: Column(
              children: [
                Expanded(
                  child: TableCalendar(
                    rowHeight: 40,
                    focusedDay: DateTime.now(),
                    firstDay: DateTime.utc(2000, 1, 1),
                    lastDay: DateTime.utc(2100, 12, 31),
                    onDaySelected: (selectedDay, focusedDay) {
                      String dateStr =
                          formatDate(selectedDay, [yyyy, '-', mm, '-', dd]);
                      callback(dateStr);
                      entry.remove();
                    },
                  ),
                ),
                Container(
                  height: 60,
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        onTap: () {
                          callback("");
                          entry.remove();
                        },
                        child: Container(
                          alignment: Alignment.center,
                          height: 30,
                          width: 100,
                          decoration: BoxDecoration(
                            border: Border.all(width: 1, color: Colors.blue),
                            borderRadius: BorderRadius.all(
                              Radius.circular(8),
                            ),
                          ),
                          child: Text(
                            "Clean",
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      InkWell(
                        onTap: () {
                          entry.remove();
                        },
                        child: Container(
                          alignment: Alignment.center,
                          height: 30,
                          width: 100,
                          decoration: BoxDecoration(
                            border: Border.all(width: 1, color: Colors.pink),
                            borderRadius: BorderRadius.all(
                              Radius.circular(8),
                            ),
                          ),
                          child: Text(
                            "Cancel",
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.red,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  });
//往Overlay中插入插入OverlayEntry
  Overlay.of(context)?.insert(entry);
}

showDelAlertHandle(BuildContext context, Function confirmHandle) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Confirm"),
          content: Text("Are You Sure Want Delete?"),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text("Cancel")),
            TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  confirmHandle();
                },
                child: Text("Delete",
                    style: TextStyle(
                      color: Colors.red,
                    )))
          ],
        );
      });
}

class AppPickField extends StatefulWidget {
  String? placeholder;
  String? titleStr;
  bool? must;
  Function? onTap;
  IconData? iconData;
  String? value;
  AppPickField(
      {Key? key,
      this.placeholder,
      this.titleStr,
      this.must,
      this.onTap,
      this.value,
      this.iconData})
      : super(key: key);

  @override
  _AppPickFieldState createState() => _AppPickFieldState();
}

class _AppPickFieldState extends State<AppPickField> {
  late String placeholder;
  late String title;
  late bool must;
  late Function onTap;
  late IconData iconData;
  late String value;

  @override
  void initState() {
    super.initState();
    placeholder = widget.placeholder ?? "";
    title = widget.titleStr ?? "";
    must = widget.must ?? false;
    onTap = widget.onTap ?? () {};
    iconData = widget.iconData ?? Icons.insert_invitation;
    value = widget.value ?? "";
  }

  @override
  void didUpdateWidget(AppPickField oldWidget) {
    super.didUpdateWidget(oldWidget);
    placeholder = widget.placeholder ?? "";
    title = widget.titleStr ?? "";
    must = widget.must ?? false;
    onTap = widget.onTap ?? () {};
    iconData = widget.iconData ?? Icons.insert_invitation;
    value = widget.value ?? "";
  }

  var mustTip = Text("*", style: TextStyle(fontSize: 14, color: Colors.red));
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(title,
                style: TextStyle(fontSize: 14, color: Color(0xff080808))),
            Offstage(
              offstage: !must,
              child: mustTip,
            )
          ],
        ),
        SizedBox(height: 12),
        Stack(
          children: [
            TextField(
                controller: TextEditingController(text: value),
                style: TextStyle(fontSize: 14),
                readOnly: true,
                decoration: InputDecoration(
                    suffixIcon: Icon(iconData),
                    hintText: placeholder,
                    hintStyle: TextStyle(
                        fontSize: 12,
                        color: Color(0xff080808).withOpacity(0.5)),
                    isCollapsed: true,
                    contentPadding: EdgeInsets.fromLTRB(23, 15, 0, 15),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide:
                            BorderSide(width: 1, color: Color(0x80999999))),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide:
                            BorderSide(width: 1, color: Color(0x80999999))))),
            InkWell(
              hoverColor: Colors.transparent,
              onTap: () {
                onTap();
              },
              child: Container(
                height: 42,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
