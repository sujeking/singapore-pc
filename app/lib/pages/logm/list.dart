// ignore_for_file: import_of_legacy_library_into_null_safe

import 'dart:io';

import 'package:app/comps/active_app_view.dart';
import 'package:app/comps/app_filtter_view.dart';
import 'package:app/comps/app_search_view.dart';
import 'package:app/comps/app_table.dart';
import 'package:app/comps/app_table_cell.dart';
import 'package:app/comps/no_data_view.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/logm.dart';
import 'package:app/net/tenantm.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import "package:path_provider/path_provider.dart";
import 'package:toast/toast.dart';

class LogListPage extends StatefulWidget {
  LogListPage({Key? key}) : super(key: key);

  @override
  _LogListPageState createState() => _LogListPageState();
}

class _LogListPageState extends State<LogListPage> {
  List<String> cols = [
    'USER ACCOUNT NAME',
    'MODULE',
    'ACTIVE DATE & TIME',
    'ACTIVTY TYPE',
    'IP ADDRESS',
    'DEVICE ID',
    'BRANCH NAME',
    'EXCEPTION',
  ];
  String clientId = "";
  List<String> keys = [
    "userName",
    "module",
    "activityTime",
    "activityType",
    "ipAddress",
    "deviceId",
    "branch",
    "exception",
  ];
  List periods = [
    "Today",
    "LastWeek",
    "LastMonth",
    "Last3Month",
    "Last6Month",
  ];
  int fillterIdx = 0;
  bool isloading = true;

  List<double> widths = [
    180,
    120,
    180,
    180,
    180,
    200,
    190,
    200,
    100,
  ];

  List<Widget> rows = [];

  int appIdx = 0;

  int _page = 1;
  int _pageSize = 30;
  String kwd = "";
  var datasource = [];
  var apps = [];
  bool resort = false;
  String sortKey = "";

// action
  addDeviceHandle() {
    Navigator.pushNamed(context, "/add");
  }

  viewDeviceHandle() {
    Navigator.pushNamed(context, "/read");
  }

  @override
  void initState() {
    super.initState();
    sortKey = keys.first;
    netApps();
  }

  netApps() async {
    var res = await tenantActives();
    apps = res["data"];
    if (apps.isNotEmpty) {
      var dict = apps.first;
      clientId = dict["name"];
      netLogList(null);
    }
  }

  netLogList(RefreshController? c) async {
    var fi = {
      "_page": _page,
      "_pageSize": _pageSize,
      "_keyword": kwd,
      "clientId": clientId,
      "period": periods[fillterIdx],
      "_orderby": resort ? "${sortKey} desc" : "${sortKey} asc",
    };
    var res = await logList(fi);
    var list = res["list"];
    datasource.addAll(list);
    updateUI();
    isloading = false;
    setState(() {});
    c!.refreshCompleted();
    c.loadComplete();
  }

  netLogDownload() async {
    try {
      var res = await logDownload();
      Directory? dir = await getDownloadsDirectory();
      String path = dir!.path;
      List arr = path.split("/");
      String dpath = "/${arr[1]}/${arr[2]}/${arr.last}";
      var file = await File("${dpath}/sso_log.csv");
      file.writeAsString(res);
      Toast.show("Export success", context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    } catch (e) {
      Toast.show("Export failed,try again", context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  updateUI() {
    rows.clear();
    for (var i = 0; i < datasource.length; i++) {
      var dict = datasource[i];
      var row = InkWell(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                AppTableCell(width: widths[0], text: dict["userName"] ?? ""),
                AppTableCell(width: widths[1], text: dict["module"] ?? ""),
                AppTableCell(
                    width: widths[2], text: dict["activityTime"] ?? ""),
                AppTableCell(
                    width: widths[3], text: dict["activityType"] ?? ""),
                AppTableCell(width: widths[4], text: dict["ipAddress"] ?? ""),
                AppTableCell(width: widths[5], text: dict["deviceId"] ?? ""),
                AppTableCell(width: widths[6], text: dict["branch"] ?? ""),
                AppTableCell(width: widths[7], text: dict["exception"] ?? ""),
                AppTableCell(
                  width: widths[8],
                  child: Container(
                    child: Row(
                      children: [
                        Icon(
                          Icons.more_horiz,
                          color: Colors.blue,
                          size: 14,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          "More Details",
                          style: TextStyle(
                            color: Colors.blue,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Container(
              height: 0.5,
              width: widths.reduce((a, b) => a + b) + 100,
              color: Color(0xffe0e0e0),
            )
          ],
        ),
      );
      rows.add(row);
    }

    if (rows.isEmpty) {
      rows.add(
        NoDataView(),
      );
    }
  }

  _cleanData() {
    _page = 1;
    rows.clear();
    datasource.clear();
  }

  _header() {
    List<AppTable3Header> items = [];
    for (var i = 0; i < cols.length; i++) {
      AppTable3Header ii = AppTable3Header(
        sortkey: keys[i],
        text: cols[i],
        isup: !resort,
        width: widths[i],
        onTap: (key) {
          isloading = true;
          setState(() {});
          sortKey = key;
          _page = 1;
          resort = !resort;
          datasource.clear();
          netLogList(null);
        },
      );
      items.add(ii);
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: items),
        Container(
          height: 0.5,
          width: widths.reduce((a, b) => a + b),
          color: Color(0xffe0e0e0),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TopBar(
              backTitle: "View Audit Log",
            ),
            PageTitleView(title: "View Audit Log"),
            SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Expanded(
                  child: AppFiltterView(
                    idx: fillterIdx,
                    onClick: (idx) {
                      fillterIdx = idx;
                      _page = 1;
                      datasource.clear();
                      netLogList(null);
                    },
                  ),
                ),
                // export btn
                InkWell(
                  onTap: netLogDownload,
                  child: Container(
                    alignment: Alignment.center,
                    constraints: BoxConstraints(minWidth: 115),
                    padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(40),
                        border: Border.all(width: 1, color: Color(0xff007aff))),
                    child: Row(
                      children: [
                        Icon(Icons.arrow_downward, color: Color(0xff007aff)),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          "Export",
                          style: TextStyle(
                            fontSize: 16,
                            fontFamily: "sli",
                            color: Color(0xff007aff),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
            ActiveAppView(
              currentIdx: appIdx,
              apps: apps,
              selectHandle: (name) {
                _page = 1;
                datasource.clear();
                clientId = name;
                netLogList(null);
              },
            ),
            AppSearchView(
              onTap: () {
                netLogDownload();
              },
              btnTitle: "",
              onSubmit: (e) {},
            ),
            Expanded(
              child: AppTable3(
                isLoading: isloading,
                onRefresh: (RefreshController c) {
                  isloading = true;
                  setState(() {});
                  _cleanData();
                  netLogList(c);
                },
                onLoadMore: (RefreshController c) {
                  isloading = true;
                  setState(() {});
                  _page += 1;
                  netLogList(c);
                },
                children: [_header(), ...rows],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
