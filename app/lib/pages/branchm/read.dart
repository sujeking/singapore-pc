// ignore_for_file: import_of_legacy_library_into_null_safe

import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_pick_field.dart';
import 'package:app/comps/app_read_item.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/branchm.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class BranchReadPage extends StatefulWidget {
  BranchReadPage({Key? key}) : super(key: key);

  @override
  _BranchReadPageState createState() => _BranchReadPageState();
}

class _BranchReadPageState extends State<BranchReadPage> {
  Map<String, dynamic> info = {};
  late String idstr;
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      String id = ModalRoute.of(context)?.settings.arguments as String;
      idstr = id;
      netBranchDetail(id);
    });
  }

  netBranchDetail(String id) async {
    if (null == id) {
      return;
    }
    var res = await branchDetail(id);
    info = res;
    setState(() {});
  }

  netDelBranch(String id) async {
    var res = await branchDelete(id);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show(
        "Successful Operation",
        context,
        gravity: Toast.CENTER,
        duration: Toast.LENGTH_LONG,
      );
      Future.delayed(
        Duration(seconds: 2),
        () {
          Navigator.of(context).pop();
        },
      );
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  rowOne() {
    return Container(
      height: 100,
      padding: EdgeInsets.only(left: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
              title: "Created By",
              value: info["creatorName"] ?? "",
            ),
          ),
          Expanded(
            child: AppReadItem(
              title: "Approved By",
              value: info["approverName"] ?? "",
            ),
          ),
          Expanded(
            child: Container(
              child: Column(),
            ),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
      height: 100,
      padding: EdgeInsets.only(left: 12),
      margin: EdgeInsets.only(top: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
              title: "Branch Name",
              value: info["name"] ?? "",
            ),
          ),
          Expanded(
            child: AppReadItem(
              title: "Subsidiary",
              value: info["subsidiary"] ?? "",
            ),
          ),
          Expanded(
            child: AppReadItem(
              title: "Branch Code",
              value: info["code"] ?? "",
            ),
          ),
        ],
      ),
    );
  }

  rowThree() {
    return Container(
      height: 100,
      padding: EdgeInsets.only(left: 12),
      margin: EdgeInsets.only(top: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
              title: "Branch Location",
              value: info["location"] ?? "",
            ),
          ),
          Expanded(
            child: AppReadItem(
              title: "Start Date",
              value: info["startDate"] ?? "",
            ),
          ),
          Expanded(
            child: AppReadItem(
              title: "End Date",
              value: info["endDate"] ?? "",
            ),
          ),
        ],
      ),
    );
  }

  ActionView() {
    return Container(
      child: Row(
        children: info["statusCode"] == 1
            ? [
                AppBottomBtn(
                    label: "Back",
                    color: Colors.blue,
                    onTap: () {
                      Navigator.of(context).pop();
                    }),
              ]
            : [
                AppBottomBtn(
                    label: "Edit Branch",
                    color: Colors.blue,
                    onTap: () {
                      Navigator.of(context)
                          .pushNamed("/edit", arguments: info)
                          .then(
                        (value) {
                          netBranchDetail(idstr);
                        },
                      );
                    }),
                SizedBox(width: 12),
                AppBottomBtn(
                    label: "Delete Branch",
                    color: Colors.red,
                    onTap: () {
                      showDelAlertHandle(context, () {
                        netDelBranch(idstr);
                      });
                    }),
              ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.fromLTRB(40, 12, 40, 80),
        child: Column(
          children: [
            TopBar(
              backTitle: "View Branch",
              popAction: () {
                Navigator.pop(context);
              },
            ),
            Expanded(
              child: ListView(
                children: [
                  PageTitleView(title: "View Branch"),
                  rowOne(),
                  rowTwo(),
                  rowThree(),
                ],
              ),
            ),
            ActionView()
          ],
        ),
      ),
    );
  }
}
