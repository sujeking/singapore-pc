import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_pick_field.dart';
import 'package:app/comps/app_read_item.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/rolem.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class RoleReadPage extends StatefulWidget {
  RoleReadPage({Key? key}) : super(key: key);

  @override
  _RoleReadPageState createState() => _RoleReadPageState();
}

class _RoleReadPageState extends State<RoleReadPage> {
  Map<String, dynamic> info = {};
  late String idstr;
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      String id = ModalRoute.of(context)?.settings.arguments as String;
      idstr = id;
      netRoleDetail(id);
    });
  }

  netRoleDetail(String id) async {
    if (null == id) {
      return;
    }
    var res = await roleDetail(id);
    info = res;
    setState(() {});
  }

  netRoleDel(String id) async {
    var res = await roleDelete(id);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show(
        "Successful Operation",
        context,
        gravity: Toast.CENTER,
        duration: Toast.LENGTH_LONG,
      );
      Future.delayed(
        Duration(seconds: 2),
        () {
          Navigator.of(context).pop();
        },
      );
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  rowOne() {
    return Container(
      height: 100,
      padding: EdgeInsets.only(left: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
              title: "Created By",
              value: info["creatorName"] ?? "",
            ),
          ),
          Expanded(
            child: AppReadItem(
              title: "Approved By",
              value: info["approverName"] ?? "",
            ),
          ),
          Expanded(
            child: Container(
              child: Column(),
            ),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
      height: 100,
      padding: EdgeInsets.only(left: 12),
      margin: EdgeInsets.only(top: 12),
      child: Row(
        children: [
          Expanded(
            child: AppReadItem(
              title: "Role Name",
              value: info["name"] ?? "---",
            ),
          ),
          Expanded(
            child: AppReadItem(
              title: "Role Start Date",
              value: info["startDate"] ?? "---",
            ),
          ),
          Expanded(
            child: AppReadItem(
              title: "Role End Date",
              value: info["endDate"] ?? "---",
            ),
          ),
        ],
      ),
    );
  }

  ActionView() {
    return Container(
      child: Row(
        children: info["statusCode"] == 1
            ? [
                AppBottomBtn(
                    label: "Back",
                    color: Colors.blue,
                    onTap: () {
                      Navigator.of(context).pop();
                    }),
              ]
            : [
                AppBottomBtn(
                    label: "Edit Role",
                    color: Colors.blue,
                    onTap: () {
                      Navigator.of(context)
                          .pushNamed("/edit", arguments: info)
                          .then(netRoleDetail(idstr));
                    }),
                SizedBox(width: 12),
                AppBottomBtn(
                    label: "Delete Role",
                    color: Colors.red,
                    onTap: () {
                      showDelAlertHandle(context, () {
                        netRoleDel(idstr);
                      });
                    }),
              ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.fromLTRB(40, 12, 40, 80),
        child: Column(
          children: [
            TopBar(
              backTitle: "View Role",
              popAction: () {
                Navigator.pop(context);
              },
            ),
            Expanded(
              child: ListView(
                children: [
                  PageTitleView(title: "View Role"),
                  rowOne(),
                  rowTwo(),
                ],
              ),
            ),
            Offstage(
              offstage: info["statusCode"] == null,
              child: ActionView(),
            ),
          ],
        ),
      ),
    );
  }
}
