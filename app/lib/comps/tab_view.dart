import 'package:flutter/material.dart';

class AppTabView extends StatefulWidget {
  final List<String> titles;
  final TabController tabController;
  AppTabView({Key? key, required this.titles, required this.tabController})
      : super(key: key);

  @override
  _AppTabViewState createState() => _AppTabViewState();
}

class _AppTabViewState extends State<AppTabView>
    with SingleTickerProviderStateMixin {
  int table_index = 0;
  late TabController tabController;
  List<String> titles = [];
  @override
  void initState() {
    super.initState();
    titles = widget.titles;
    tabController = widget.tabController;
  }

  Widget _ItemView(title, idx) {
    return Container(
      margin: EdgeInsets.only(right: 12),
      child: Column(
        children: [
          InkWell(
            onTap: () {
              table_index = idx;
              tabController.animateTo(table_index);
              setState(() {});
            },
            highlightColor: Colors.transparent,
            hoverColor: Colors.transparent,
            radius: 0,
            child: Container(
                constraints: BoxConstraints(minWidth: 120),
                alignment: Alignment.center,
                child: Text(
                  title,
                  style: TextStyle(
                      fontSize: 15,
                      fontFamily: table_index == idx ? "sbo" : "sli"),
                )),
          ),
          Container(
            height: 1.5,
            width: 120,
            color: table_index == idx ? Color(0xff080808) : Colors.transparent,
            margin: EdgeInsets.only(top: 12),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
              children: titles.map((e) {
            int idx = titles.indexOf(e);
            return _ItemView(e, idx);
          }).toList()),
        ],
      ),
    );
  }
}
