// ignore_for_file: invalid_use_of_visible_for_testing_member

import 'package:app/layout.dart';
import 'package:app/test.dart';
import 'package:app/pages/login/login.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

var routes = {
  "/": (context) => LoginPage(),
  "/ts": (context) => Test(),
  "/layout": (context) => Layout(),
};

const Set<PointerDeviceKind> _kTouchLikeDeviceTypes = <PointerDeviceKind>{
  PointerDeviceKind.touch,
  PointerDeviceKind.mouse,
  PointerDeviceKind.stylus,
  PointerDeviceKind.invertedStylus,
  PointerDeviceKind.unknown
};

void main() {
  SharedPreferences.setMockInitialValues({});
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: "/",
      routes: routes,
      debugShowCheckedModeBanner: false,
      scrollBehavior: const MaterialScrollBehavior()
          .copyWith(scrollbars: true, dragDevices: _kTouchLikeDeviceTypes),
    );
  }
}



// xcode setting
// 
// <key>com.apple.security.network.client</key>
// <true/>
// 
// <key>com.apple.security.files.user-selected.read-write</key>
// <true/>
// <key>com.apple.security.files.bookmarks.app-scope</key>
// <true/>