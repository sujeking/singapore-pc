import 'package:app/comps/app_approval_items.dart';
import 'package:app/comps/app_bottom_btn.dart';
import 'package:app/comps/app_read_item.dart';
import 'package:app/comps/page_title.dart';
import 'package:app/net/branchm.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:toast/toast.dart';

class BranchApprovalEditPage extends StatefulWidget {
  BranchApprovalEditPage({Key? key}) : super(key: key);

  @override
  _BranchApprovalEditPageSatate createState() =>
      _BranchApprovalEditPageSatate();
}

class _BranchApprovalEditPageSatate extends State<BranchApprovalEditPage> {
  Map<String, dynamic> body = {};

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      body = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
      setState(() {});
    });
  }

  netPassApproval() async {
    var id = body["newData"]["_id"];
    if (null == id) {
      return;
    }
    var res = await branchAppPass(id);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show("Successful Operation", context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      Future.delayed(Duration(seconds: 2), () {
        Navigator.of(context).pop();
      });
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  netRejectApproval() async {
    var id = body["newData"]["_id"];
    if (null == id) {
      return;
    }
    var res = await branchAppReject(id);
    int statusCode = res["statusCode"];
    if (statusCode == 200) {
      Toast.show("Successful Operation", context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      Future.delayed(Duration(seconds: 2), () {
        Navigator.of(context).pop();
      });
    } else {
      Toast.show(res["message"], context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  rowOne() {
    return Container(
      height: 100,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: AppReadItem(
                title: "Request By", value: body["newData"]["creatorName"]),
          ),
          SizedBox(width: 50),
          Expanded(
            child: AppReadItem(
                title: "Requested Date", value: body["newData"]["updatedAt"]),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 12),
        child: Row(
          children: [
            Expanded(
                child: AppApprovalItems(
              title: "Branch Name",
              oldValue: body["existingData"]["name"],
              newValue: body["newData"]["name"],
            )),
            SizedBox(
              width: 50,
            ),
            Expanded(
                child: AppApprovalItems(
              title: "Tenant",
              oldValue: body["existingData"]["tenantId"],
              newValue: body["newData"]["tenantId"],
            )),
          ],
        ));
  }

  rowThree() {
    return Container(
        margin: EdgeInsets.only(top: 48),
        child: Row(
          children: [
            Expanded(
                child: AppApprovalItems(
              title: "Branch End Date",
              oldValue: body["existingData"]["endDate"],
              newValue: body["newData"]["endDate"],
            )),
            SizedBox(
              width: 50,
            ),
            Expanded(
                child: AppApprovalItems(
              title: "Branch Start Date",
              oldValue: body["existingData"]["startDate"],
              newValue: body["newData"]["startDate"],
            )),
          ],
        ));
  }

  backHandle() {
    Navigator.pop(context);
  }

  ActionView() {
    return Container(
      child: Row(
        children: [
          AppBottomBtn(
              label: "Approval Edit Branch",
              color: Colors.blue,
              onTap: () {
                netPassApproval();
              }),
          SizedBox(
            width: 12,
          ),
          AppBottomBtn(
              label: "Reject Edit Branch",
              color: Colors.red,
              onTap: () {
                netRejectApproval();
              }),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Container(
          padding: EdgeInsets.only(bottom: 80),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TopBar(
                backTitle: "Approval Branch",
                popAction: () {
                  Navigator.pop(context);
                },
              ),
              PageTitleView(title: "Edit Branch Approval"),
              SizedBox(
                height: 40,
              ),
              Expanded(
                child: ListView(
                  children: [
                    rowOne(),
                    rowTwo(),
                    rowThree(),
                  ],
                ),
              ),
              ActionView(),
            ],
          ),
        ),
      ),
    );
  }
}
