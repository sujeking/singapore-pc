import 'package:app/comps/appitem.dart';
import 'package:flutter/material.dart';

class ActiveAppView extends StatefulWidget {
  int currentIdx;
  List? apps;
  Function(String name)? selectHandle;
  ActiveAppView({
    Key? key,
    this.apps,
    this.selectHandle,
    required this.currentIdx,
  }) : super(key: key);

  @override
  _ActiveAppViewState createState() => _ActiveAppViewState();
}

class _ActiveAppViewState extends State<ActiveAppView> {
  List apps = [];
  @override
  void initState() {
    super.initState();
    apps = widget.apps ?? [];
  }

  @override
  void didUpdateWidget(covariant ActiveAppView oldWidget) {
    super.didUpdateWidget(oldWidget);
    apps = widget.apps ?? [];
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Offstage(
        offstage: apps.isEmpty,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 12),
            Text("Active Users",
                style: TextStyle(fontSize: 18, fontFamily: "sse")),
            SizedBox(height: 12),
            Container(
              height: 120,
              color: Colors.white,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: apps.length,
                controller: ScrollController(),
                itemBuilder: (BuildContext context, int index) {
                  var dict = apps[index] as Map<String, dynamic>;
                  return AppItem(
                    onTap: widget.selectHandle,
                    isSeleced: widget.currentIdx == index,
                    data: dict,
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
