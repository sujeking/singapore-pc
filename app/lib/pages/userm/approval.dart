import 'package:app/views/topbar/topbar.dart';
import 'package:flutter/material.dart';

class UserApprovalPage extends StatefulWidget {
  UserApprovalPage({Key? key}) : super(key: key);

  @override
  _UserApprovalPageState createState() => _UserApprovalPageState();
}

class _UserApprovalPageState extends State<UserApprovalPage> {
  rowOne() {
    return Container(
      height: 100,
      padding: EdgeInsets.only(left: 12),
      child: Row(
        children: [
          Expanded(
            child: Container(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Requested By",
                        style:
                            TextStyle(fontSize: 14, color: Color(0xff999999))),
                    Text(
                      "Blasz",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Approved By",
                        style:
                            TextStyle(fontSize: 14, color: Color(0xff999999))),
                    Text(
                      "Blasz",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              child: Column(),
            ),
          ),
        ],
      ),
    );
  }

  rowTwo() {
    return Container(
      height: 100,
      padding: EdgeInsets.only(left: 12),
      margin: EdgeInsets.only(top: 12),
      child: Row(
        children: [
          Expanded(
            child: Container(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("IP Name",
                        style:
                            TextStyle(fontSize: 14, color: Color(0xff999999))),
                    Text(
                      "Dev ENV",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("IP Address",
                        style:
                            TextStyle(fontSize: 14, color: Color(0xff999999))),
                    Text(
                      "127.0.0.1",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("IP Created Date",
                        style:
                            TextStyle(fontSize: 14, color: Color(0xff999999))),
                    Text(
                      "12-04-2020",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                    )
                  ]),
            ),
          ),
        ],
      ),
    );
  }

  rowThree() {
    return Container(
      height: 100,
      padding: EdgeInsets.only(left: 12),
      margin: EdgeInsets.only(top: 12),
      child: Row(
        children: [
          Expanded(
            child: Container(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("MDM Date",
                        style:
                            TextStyle(fontSize: 14, color: Color(0xff999999))),
                    Text(
                      "10-12-2021",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("MDM Register Date",
                        style:
                            TextStyle(fontSize: 14, color: Color(0xff999999))),
                    Text(
                      "10-12-2021",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("MDM Date",
                        style:
                            TextStyle(fontSize: 14, color: Color(0xff999999))),
                    Text(
                      "",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                    )
                  ]),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Column(
            children: [
              TopBar(
                backTitle: "View IP",
                popAction: () {
                  Navigator.pop(context);
                },
              ),
              Column(
                children: [
                  rowOne(),
                  rowTwo(),
                ],
              )
            ],
          ),
          Positioned(
            bottom: 12,
            left: 0,
            child: Container(
              child: Row(
                children: [
                  InkWell(
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 50, vertical: 8),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          border: Border.all(width: 1, color: Colors.blue)),
                      child: Text(
                        "edit device",
                        style: TextStyle(color: Colors.blue),
                      ),
                    ),
                  ),
                  SizedBox(width: 12),
                  InkWell(
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 50, vertical: 8),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          border: Border.all(width: 1, color: Colors.red)),
                      child: Text(
                        "delete device",
                        style: TextStyle(color: Colors.red),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
