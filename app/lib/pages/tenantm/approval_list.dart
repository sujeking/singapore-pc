import 'package:app/comps/app_table.dart';
import 'package:app/comps/app_table_cell.dart';
import 'package:app/comps/base_datatable.dart';
import 'package:app/comps/no_data_view.dart';
import 'package:app/net/tenantm.dart';
import 'package:app/views/topbar/topbar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class TenantApprovalPage extends StatefulWidget {
  String? kwd;
  TenantApprovalPage({Key? key, this.kwd}) : super(key: key);

  @override
  _TenantApprovalPageState createState() => _TenantApprovalPageState();
}

class _TenantApprovalPageState extends State<TenantApprovalPage> {
  int _page = 1;
  int _pageSize = 30;
  var datasource = [];
  late var pageInfo;
  String kwd = "";
  List<String> cols = [
    "TENANT NAME",
    "REQUESTED BY",
    "REQUESTED DATE",
    "ACTION",
  ];
  List<String> keys = [
    "name",
    "requested BY",
    "requested DATE",
    "action",
  ];
  bool resort = false;
  String sortKey = "";

  List<double> widths = [
    367.0,
    155.0,
    210.0,
    300.0,
  ];

  List<Widget> rows = [];
  bool isloading = true;
  @override
  void initState() {
    super.initState();
    sortKey = keys.first;
    netTenantAppList(null);
  }

  @override
  void didUpdateWidget(TenantApprovalPage oldWidget) {
    super.didUpdateWidget(oldWidget);
    kwd = widget.kwd ?? "";
    _cleanData();
    netTenantAppList(null);
  }

  netTenantAppList(RefreshController? c) async {
    var fi = {
      "_page": _page,
      "_pageSize": _pageSize,
      "_keyword": kwd,
      "_orderby": resort ? "${sortKey} desc" : "${sortKey} asc",
    };
    var res = await tenantAppList(fi);
    pageInfo = res["pageInfo"];
    var list = res["list"];
    datasource.addAll(list);
    print(datasource);
    updateUI();
    isloading = false;
    setState(() {});
    c!.refreshCompleted();
    c.loadComplete();
  }

  rowSelectHandle(data, index) {
    if (index == 0) {
      Navigator.pushNamed(context, "/appadd");
    } else {
      Navigator.pushNamed(context, "/appedit");
    }
  }

  updateUI() {
    rows.clear();
    for (var i = 0; i < datasource.length; i++) {
      var dict = datasource[i];
      var row = InkWell(
        onTap: () {
          rowSelectHandle({}, 0);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(children: [
              AppTableCell(
                  width: widths[0],
                  child: Row(
                    children: [
                      ClipOval(
                        child: Container(
                          color: Color(0xfff5f5f5),
                          width: 32,
                          height: 32,
                          child: CachedNetworkImage(
                            imageUrl: dict["image"],
                            errorWidget: (
                              BuildContext context,
                              String url,
                              dynamic error,
                            ) =>
                                Icon(Icons.person, color: Color(0xff999999)),
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Text(
                        dict["name"] ?? "",
                        style: TextStyle(
                            fontSize: 12,
                            fontFamily: "sli",
                            color: Color(0xff080808)),
                      )
                    ],
                  )),
              AppTableCell(width: widths[1], text: dict["szw"] ?? ""),
              AppTableCell(width: widths[2], text: dict["szw"] ?? ""),
              AppTableCell(width: widths[3], text: dict["szw"] ?? ""),
            ]),
            Container(
              height: 0.5,
              width: widths.reduce((a, b) => a + b),
              color: Color(0xffe0e0e0),
            )
          ],
        ),
      );
      rows.add(row);
    }
    if (rows.isEmpty) {
      rows.add(NoDataView(
        width: widths.reduce((a, b) => a + b),
      ));
    }
  }

  _cleanData() {
    _page = 1;
    rows.clear();
    datasource.clear();
  }

  _header() {
    List<AppTable3Header> items = [];
    for (var i = 0; i < cols.length; i++) {
      AppTable3Header ii = AppTable3Header(
        sortkey: keys[i],
        text: cols[i],
        isup: !resort,
        width: widths[i],
        onTap: (key) {
          isloading = true;
          setState(() {});
          sortKey = key;
          _page = 1;
          resort = !resort;
          datasource.clear();
          netTenantAppList(null);
        },
      );
      items.add(ii);
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: items),
        Container(
          height: 0.5,
          width: widths.reduce((a, b) => a + b),
          color: Color(0xffe0e0e0),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppTable3(
      isLoading: isloading,
      onRefresh: (RefreshController c) {
        isloading = true;
        setState(() {});
        _cleanData();
        netTenantAppList(c);
      },
      onLoadMore: (RefreshController c) {
        isloading = true;
        setState(() {});
        _page += 1;
        netTenantAppList(c);
      },
      children: [_header(), ...rows],
    );
  }
}
