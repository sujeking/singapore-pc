import 'package:badges/badges.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class TopBar extends StatefulWidget {
  String? backTitle;
  Function? popAction;
  TopBar({Key? key, this.backTitle, this.popAction}) : super(key: key);

  @override
  _TopBarState createState() => _TopBarState();
}

class _TopBarState extends State<TopBar> {
  String? backTitle;
  Function? popAction;

  @override
  void initState() {
    super.initState();
    backTitle = widget.backTitle;
    popAction = widget.popAction;
  }

  backBtn(title) {
    return InkWell(
      onTap: popHandle,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Offstage(
            offstage: popAction == null,
            child: Row(
              children: [
                Icon(
                  IconData(
                    int.parse("0xe675"),
                    fontFamily: 'iconfont',
                    matchTextDirection: true,
                  ),
                  size: 22,
                ),
                SizedBox(
                  width: 5,
                ),
              ],
            ),
          ),
          Text(
            title,
            style: TextStyle(
                fontSize: 18, fontFamily: "sf", color: Color(0xff71706f)),
          )
        ],
      ),
    );
  }

  popHandle() {
    popAction!();
  }

  logoutHandle() {
    Navigator.of(context).pop();
  }

  rightView() {
    return Row(
      children: [
        InkWell(
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                width: 40,
                height: 40,
                child: Icon(
                  Icons.notifications_none,
                  color: Colors.black.withOpacity(0.65),
                  size: 30,
                ),
              ),
              Positioned(
                right: 0,
                top: 0,
                child: ClipOval(
                  child: Container(
                    color: Color(0xff2885FF),
                    width: 22,
                    height: 22,
                    alignment: Alignment.center,
                    child: Text(
                      "2",
                      style: TextStyle(fontSize: 12, color: Colors.white),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        SizedBox(width: 15),
        InkWell(
          onTap: () {
            showMenu(
                context: context,
                position: RelativeRect.fromLTRB(20, 60, 0, 0),
                items: [
                  PopupMenuItem(
                      onTap: logoutHandle,
                      child: Container(width: 90, child: Text("Logout"))),
                  PopupMenuItem(child: Text("V1.0.0")),
                ]);
          },
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(27),
                border: Border.all(
                  width: 1,
                  color: Color(0xff999999).withOpacity(0.6),
                )),
            child: ClipOval(
              child: Image.asset(
                "assets/img/user_avatar.jpeg",
                width: 54,
                height: 54,
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Offstage(
            offstage: false,
            child: backBtn(backTitle ?? ""),
          ),
          rightView()
        ],
      ),
    );
  }
}
