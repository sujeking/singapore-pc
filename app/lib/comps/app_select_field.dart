import 'package:app/comps/app_popup_route.dart';
import 'package:flutter/material.dart';

class AppSelectField extends StatefulWidget {
  List items;
  String? placeholder;
  String? titleStr;
  bool? must;
  String? value;
  bool? multiSelected;
  Function(dynamic v)? onChanged;
  AppSelectField(
      {Key? key,
      this.placeholder,
      this.titleStr,
      required this.items,
      this.must,
      this.value,
      this.multiSelected,
      this.onChanged})
      : super(key: key);

  @override
  _AppSelectFieldState createState() => _AppSelectFieldState();
}

class _AppSelectFieldState extends State<AppSelectField> {
  late String placeholder;
  late String title;
  late bool must;
  late Function onChanged;
  late String value;
  late TextEditingController _editingController;
  bool multiSelected = false;

  double height = 0;
  late OverlayEntry entry;
  GlobalKey key = GlobalKey();
  bool hasshow = false;
  FocusNode focusNode = FocusNode();
  List selected = [];

  @override
  void initState() {
    super.initState();
    placeholder = widget.placeholder ?? "";
    title = widget.titleStr ?? "";
    must = widget.must ?? false;
    onChanged = widget.onChanged ?? (e) {};
    value = widget.value ?? "";
    _editingController = TextEditingController(text: value);
    multiSelected = widget.multiSelected ?? false;
    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        showList();
      }
    });
  }

  @override
  void didUpdateWidget(covariant AppSelectField oldWidget) {
    super.didUpdateWidget(oldWidget);
    placeholder = widget.placeholder ?? "";
    title = widget.titleStr ?? "";
    must = widget.must ?? false;
    onChanged = widget.onChanged ?? (e) {};
    value = widget.value ?? "";
    _editingController = TextEditingController(text: value);
    multiSelected = widget.multiSelected ?? false;
  }

  showList() {
    hasshow = true;
    RenderBox renderBox = key.currentContext!.findRenderObject() as RenderBox;
    var x = renderBox.localToGlobal(Offset.zero).dx - 260;
    var y = renderBox.localToGlobal(Offset.zero).dy + 35;
    var width = renderBox.size.width;
    focusNode.unfocus();
    Navigator.push(
        context,
        AppPopupRoute(
            child: GestureDetector(
          onTap: () {
            Navigator.pop(context);
            setState(() {});
          },
          child: Material(
            color: Colors.transparent,
            child: Stack(
              children: [
                Positioned(
                  left: x,
                  top: y,
                  width: width,
                  height: 181,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          width: 1,
                          color: Color(0xffe5e5e5),
                        ),
                      ),
                      child: _MenuList(
                        data: widget.items,
                        selected: selected,
                        multiSelected: multiSelected,
                        onSelect: (list) {
                          if (multiSelected) {
                            onChanged(list);
                            setState(() {});
                          } else {
                            onChanged(list.first);
                            setState(() {});
                          }
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ))).then((value) {
      print(value);
      setState(() {});
    });
  }

  var mustTip = Text("*", style: TextStyle(fontSize: 14, color: Colors.red));
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(title,
                style: TextStyle(
                    fontFamily: "sli", fontSize: 15, color: Color(0xff080808))),
            Offstage(
              offstage: !must,
              child: mustTip,
            )
          ],
        ),
        SizedBox(height: 12),
        TextField(
          key: key,
          controller: _editingController,
          focusNode: focusNode,
          style: TextStyle(
            fontFamily: "sli",
            fontSize: 14,
            // color: Color(0xff080808).withOpacity(0.5),
          ),
          decoration: InputDecoration(
              hintText: placeholder,
              hintStyle: TextStyle(
                  fontFamily: "sli",
                  fontSize: 12,
                  color: Color(0xff080808).withOpacity(0.5)),
              isCollapsed: true,
              suffixIcon: _editingController.text.isNotEmpty
                  ? InkWell(
                      onTap: () {
                        value = "";
                        selected.clear();
                        _editingController.text = value;
                        setState(() {});
                        onChanged(-1);
                      },
                      hoverColor: Colors.transparent,
                      child: Icon(
                        Icons.cancel,
                        color: Color(0xff8c8c8c),
                      ),
                    )
                  : null,
              contentPadding: EdgeInsets.fromLTRB(23, 15, 0, 15),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: BorderSide(width: 1, color: Color(0x80999999))),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: BorderSide(width: 1, color: Color(0x80999999)))),
        )
      ],
    );
  }
}

///
///

class RadioCell extends StatefulWidget {
  Function(bool) onTap;
  bool? has_checked;
  String data;
  RadioCell({
    Key? key,
    required this.data,
    this.has_checked,
    required this.onTap,
  }) : super(key: key);

  @override
  _RadioCellState createState() => _RadioCellState();
}

class _RadioCellState extends State<RadioCell> {
  bool has_checked = false;

  @override
  void initState() {
    super.initState();
    has_checked = widget.has_checked ?? false;
  }

  @override
  void didUpdateWidget(RadioCell oldWidget) {
    super.didUpdateWidget(oldWidget);
    has_checked = widget.has_checked ?? false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        has_checked = true;
        setState(() {});
        widget.onTap(has_checked);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(12),
            height: 44,
            child: Row(
              children: [
                Icon(
                  has_checked
                      ? Icons.radio_button_checked_outlined
                      : Icons.radio_button_unchecked_outlined,
                  color: Colors.blue,
                  size: 20,
                ),
                SizedBox(
                  width: 12,
                ),
                Text(
                  widget.data,
                  style: TextStyle(
                    fontSize: 14,
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 1,
            color: Color(0xffe5e5e5),
          )
        ],
      ),
    );
  }
}

/// List
class _MenuList extends StatefulWidget {
  Function(List)? onSelect;
  List data;
  bool multiSelected;
  List selected;
  _MenuList({
    Key? key,
    required this.data,
    required this.multiSelected,
    required this.selected,
    this.onSelect,
  }) : super(key: key);

  @override
  __MenuListState createState() => __MenuListState();
}

class __MenuListState extends State<_MenuList> {
  var multiSelected = false;
  var selected = [];
  @override
  void initState() {
    super.initState();
    selected = widget.selected;
    multiSelected = widget.multiSelected;
  }

  @override
  void didUpdateWidget(_MenuList oldWidget) {
    super.didUpdateWidget(oldWidget);
    multiSelected = widget.multiSelected;
    selected = widget.selected;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        padding: EdgeInsets.fromLTRB(0, 0, 0, 35),
        itemCount: widget.data.length,
        itemBuilder: (BuildContext context, int index) {
          var data = widget.data[index];
          return multiSelected
              ? CheckBoxCell(
                  data: data,
                  has_checked: selected.contains(index),
                  onTap: (checked) {
                    if (checked) {
                      selected.add(index);
                    } else {
                      selected.remove(index);
                    }
                    widget.onSelect!(selected);
                    setState(() {});
                  },
                )
              : RadioCell(
                  data: data,
                  has_checked: selected.contains(index),
                  onTap: (checked) {
                    if (checked) {
                      selected.clear();
                      selected.add(index);
                      widget.onSelect!(selected);
                      setState(() {});
                    }
                  },
                );
        },
      ),
    );
  }
}

///
///
///
class CheckBoxCell extends StatefulWidget {
  Function(bool) onTap;
  bool? has_checked;
  String data;

  CheckBoxCell({
    Key? key,
    required this.data,
    this.has_checked,
    required this.onTap,
  }) : super(key: key);

  @override
  _CheckBoxCellState createState() => _CheckBoxCellState();
}

class _CheckBoxCellState extends State<CheckBoxCell> {
  bool has_checked = false;

  @override
  void initState() {
    super.initState();
    has_checked = widget.has_checked ?? false;
  }

  @override
  void didUpdateWidget(CheckBoxCell oldWidget) {
    super.didUpdateWidget(oldWidget);
    has_checked = widget.has_checked ?? false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        has_checked = !has_checked;
        widget.onTap(has_checked);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(12),
            height: 44,
            child: Row(
              children: [
                Icon(
                  has_checked
                      ? Icons.check_box
                      : Icons.check_box_outline_blank_outlined,
                  color: Colors.blue,
                  size: 20,
                ),
                SizedBox(
                  width: 12,
                ),
                Text(
                  widget.data,
                ),
              ],
            ),
          ),
          Container(
            height: 1,
            color: Color(0xffe5e5e5),
          )
        ],
      ),
    );
  }
}
