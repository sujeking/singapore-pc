import 'package:flutter/material.dart';

class NoDataView extends StatelessWidget {
  double? width;
  NoDataView({Key? key, this.width = 900}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: 180,
      color: Colors.white,
      child: Center(
          child: Text(
        "Has No Data",
        style: TextStyle(
          fontSize: 18,
          color: Color(0xff999999),
        ),
      )),
    );
  }
}
