import 'package:flutter/material.dart';

class AppFiltterView extends StatefulWidget {
  Function(int idx)? onClick;
  int idx;
  AppFiltterView({
    Key? key,
    this.idx = 0,
    this.onClick,
  }) : super(key: key);

  @override
  State<AppFiltterView> createState() => _AppFiltterViewState();
}

class _AppFiltterViewState extends State<AppFiltterView> {
  static Color bordeColor = Color(0xffe6e6e6);

  @override
  Widget build(BuildContext context) {
    var arr = [
      "Today",
      "Last Week",
      "Last Month",
      "Last 3 Month",
      "Last 6 Month",
    ];

    return Container(
      height: 30,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: arr.length,
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
            onTap: () {
              widget.onClick!(index);
            },
            child: Container(
              margin: EdgeInsets.only(right: 6),
              padding: EdgeInsets.symmetric(horizontal: 6),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: widget.idx == index ? Color(0xfff5f5f5) : Colors.white,
                  border: Border.all(width: 1, color: bordeColor)),
              child: Center(
                  child: Text(
                arr[index],
                style: TextStyle(fontSize: 14, fontFamily: "sli"),
              )),
            ),
          );
        },
      ),
    );
  }
}
